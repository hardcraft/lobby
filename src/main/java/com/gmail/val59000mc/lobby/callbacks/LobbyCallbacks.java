package com.gmail.val59000mc.lobby.callbacks;

import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.flowdispatcher.FlowDispatcher;
import com.gmail.val59000mc.lobby.killskilldispatcher.KillSkillDispatcher;
import com.gmail.val59000mc.lobby.perks.PerksManager;
import com.gmail.val59000mc.lobby.perks.PerksStrings;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.ScoreboardStats;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Parser;
import com.google.common.collect.Lists;

public class LobbyCallbacks extends DefaultPluginCallbacks{

	private ScoreboardStats scoreboardStats;
	private PerksManager perksManager;
	private KillSkillDispatcher killSkillDispatcher;
	private FlowDispatcher flowDispatcher;

	public PerksManager getPerksManager() {
		return perksManager;
	}

	public KillSkillDispatcher getKillSkillDispatcher() {
		return killSkillDispatcher;
	}

	public FlowDispatcher getFlowDispatcher() {
		return flowDispatcher;
	}

	@Override
	public HCPlayer newHCPlayer(Player player){
		return new LobbyPlayer(player);
	}
	
	@Override
	public HCPlayer newHCPlayer(HCPlayer hcPlayer){
		return new LobbyPlayer(hcPlayer);
	}
	
	@Override
	public List<HCTeam> createTeams() {
		return Lists.newArrayList(
				newHCTeam("Défaut", ChatColor.GRAY, null)
		);
	}

	@Override
	public void beforeLoad(){
		this.killSkillDispatcher = new KillSkillDispatcher(getApi());
		killSkillDispatcher.load();
		
		this.flowDispatcher = new FlowDispatcher(getApi());
		flowDispatcher.load();
	}

	@Override
	public WorldConfig configureWorld(WorldManager worldManager) {

		World lobby = Bukkit.getWorlds().get(0);
		
		Location spawn = Parser.parseLocation(lobby, getConfig().getString("world.lobby"));
		Location center = Parser.parseLocation(lobby, getConfig().getString("world.center"));
		
		this.scoreboardStats = new ScoreboardStats(getApi());
		
		PerksStrings strings = new PerksStrings(getApi());
		strings.setup();
		this.perksManager = new PerksManager(getApi(), strings);
		
		lobby.setGameRuleValue("doDaylightCycle", "true");
		lobby.setGameRuleValue("commandBlockOutput", "false");
		lobby.setGameRuleValue("logAdminCommands", "false");
		lobby.setGameRuleValue("sendCommandFeedback", "false");
		lobby.setGameRuleValue("doMobSpawning", "false");
		lobby.setGameRuleValue("randomTickSpeed", "0");
		lobby.setTime(6000);
		lobby.setStorm(false);
		lobby.setWeatherDuration(999999999);
		lobby.setDifficulty(Difficulty.HARD);
		lobby.setSpawnLocation(spawn.getBlockX(), spawn.getBlockY(), spawn.getBlockZ());
		lobby.setKeepSpawnInMemory(true);
		lobby.save();
		
		
		int scoreboardRotation = 20 * getConfig().getInt("scoreboard-rotation",Constants.SCOREBOARD_ROTATION_SECONDS);
		
		getApi().buildTask("cycle scoreboard pages", new HCTask() {
			
			@Override
			public void run() {
				
				Bukkit.getScheduler().runTaskAsynchronously(getApi().getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						scoreboardStats.nextPage();

						if(scoreboardStats.getPage().isLastPage()){
							scoreboardStats.cleanOldScoreboardCache();
							scoreboardStats.refreshPlayersStats();
						}
						
						scoreboardStats.buildPages();
						
						
						Bukkit.getScheduler().runTask(getApi().getPlugin(), new Runnable() {
							@Override
							public void run() {
								getPmApi().updatePlayersScoreboards();
							}
						});
						
					}
				});

			}
		})
		.addListener(new HCTaskListener(){
			
			@EventHandler
			public void onPlayerJoin(HCPlayerDBInsertedEvent e){
				LobbyPlayer lobbyPlayer = (LobbyPlayer) e.getHcPlayer();
				
				Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						if(!scoreboardStats.isPlayerCached(lobbyPlayer)){
							scoreboardStats.refreshPlayerStats(lobbyPlayer);
							scoreboardStats.addPlayer(lobbyPlayer);
							scoreboardStats.buildPage(lobbyPlayer);
						}
						
						Bukkit.getScheduler().runTask(getPlugin(), new Runnable() {
							@Override
							public void run() {
								if(lobbyPlayer.getScoreboard() != null){
									lobbyPlayer.getScoreboard().update();
								}
							}
						});
						
					}
				});
				
			}
		})
		.withDelay(scoreboardRotation)
		.withInterval(scoreboardRotation)
		.build()
		.start();

		return new WorldConfig
			.Builder(lobby)
			.withLobby(spawn)
			.withCenter(center)
			.build();
	}
	
	@Override
	public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {
		return scoreboardStats.getScoreboardContent((LobbyPlayer) hcPlayer);
	}
	
	@Override
	public void vanishedPlayer(HCPlayer hcPlayer) {
		// is called when relogging to lobby after being vanished
		
		if(hcPlayer.isOnline()){
			Player player = hcPlayer.getPlayer();

			// removing vanish perm
			if(player.hasPermission("hardcraftpvp.be-vanished")){
				getApi().getDependencies().removePerm(player.getName(), "hardcraftpvp.be-vanished");
			}
			
			// restart logging process
			getPmApi().playerJoinsTheGame(player);
			
		}
	}
	
	@Override
	public void waitPlayerAtLobby(HCPlayer hcPlayer) {
		// should not be called in a continuous game
		if(hcPlayer.isOnline()){
			Player player = hcPlayer.getPlayer();
			player.teleport(getApi().getWorldConfig().getLobby());
		}
	}
	
	@Override
	public void relogPlayingPlayer(HCPlayer hcPlayer) {
		if(hcPlayer.isOnline()){
			
			Player player = hcPlayer.getPlayer();
			
			player.setGameMode(GameMode.ADVENTURE);
			player.setExp(0);
			player.setHealth(20);
			player.setFoodLevel(20);
			Inventories.clear(player);
			clearJoiningPlayerPotionEffects(player);
			
			if(player.hasPermission("hardcraftpvp.announce")){
				getStringsApi()
					.get("lobby.join")
					.replace("%player%", ChatColor.translateAlternateColorCodes('&', getDependencies().getPrefix(player)) + hcPlayer.getName())
					.sendChat();
				
			}
			
			// say welsome if player wasn't cached into memory
			if(hcPlayer.getTimePlayed() == 0){
				getStringsApi()
					.get("lobby.welcome")
					.replace("%player%", hcPlayer.getName())
					.sendChat(hcPlayer);
			}
			
			if(getApi().getDependencies().isSimpleInventoryGUIEnabled()){
				Bukkit.getScheduler().runTaskLater(getApi().getPlugin(), new Runnable() {
					public void run() {
						SIG.getPlugin().getAPI().giveInventoryToPlayer(player, "join");
						player.updateInventory();
					}
				}, 1);
			}
			
			player.teleport(getApi().getWorldConfig().getLobby());
						
		}
	}
	
	private void clearJoiningPlayerPotionEffects(Player p){
		
		if(!p.hasPermission("hardcraftpvp.fly")){
			p.setFlying(false);
			p.setAllowFlight(false);
		}
		
		if(!p.hasPermission("hardcraftpvp.speed")){
			Effects.remove(p, PotionEffectType.SPEED);
		}
		
		if(!p.hasPermission("hardcraftpvp.jump")){
			Effects.remove(p, PotionEffectType.JUMP);
		}
		
		
		if((p.hasPermission("hardcraftpvp.speed") && !p.hasPermission("hardcraftpvp.speedplus"))
			|| (p.hasPermission("hardcraftpvp.jump") && !p.hasPermission("hardcraftpvp.jumpplus"))){
			
			int speed = -1;
			int jump = -1;
			
			Collection<PotionEffect> effects = p.getActivePotionEffects();
			for(PotionEffect effect : effects){
				if(effect.getType().equals(PotionEffectType.SPEED)){
					speed = effect.getAmplifier();
				}else if(effect.getType().equals(PotionEffectType.JUMP)){
					jump = effect.getAmplifier();
				}
			}
			
			if(speed >= 3){
				Effects.remove(p, PotionEffectType.SPEED);
				Effects.addPermanent(p, PotionEffectType.SPEED, 2, false);
			}
			
			if(jump >= 3){
				Effects.remove(p, PotionEffectType.JUMP);
				Effects.addPermanent(p, PotionEffectType.JUMP, 2, false);
			}
			
		}
	}
	
	@Override
	public void spectatePlayer(HCPlayer hcPlayer) {
		// should not be called in lobby
	}
	
	@Override
	public void startPlayer(HCPlayer hcPlayer) {
		// should not be called in a continuous game
	}
	
	@Override
	public void assignStuffToPlayer(HCPlayer hcPlayer){
		hcPlayer.setStuff(null);
	}
	
	@Override
	public void respawnPlayer(HCPlayer hcPlayer) {
		relogPlayingPlayer(hcPlayer);
	}
	
	@Override
	public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {		
		event.getDrops().clear();
		event.setKeepLevel(false);
		event.setDroppedExp(0);
		getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());
		
	}
	/**
	 * Modify the death event if needed
	 * Default: respawn player after 20 ticks
	 * @param event
	 * @param hcPlayer
	 */
	@Override
	public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {		
		event.getDrops().clear();
		event.setKeepLevel(false);
		event.setDroppedExp(0);
		getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());
		
	}

	@Override
	public void formatDeathMessage(HCPlayer hcKilled, HCPlayer hcKiller, PlayerDeathEvent event) {
		event.setDeathMessage(null);
	}
	
	@Override
	public void formatDeathMessage(HCPlayer hcKilled, PlayerDeathEvent event){
		event.setDeathMessage(null);
	}
	
	
	@Override
	public Location getNextRespawnLocation(HCPlayer hcPlayer) {
		return getApi().getWorldConfig().getLobby();
	}
	
}
