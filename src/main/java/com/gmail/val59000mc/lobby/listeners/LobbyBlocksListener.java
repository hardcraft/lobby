package com.gmail.val59000mc.lobby.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class LobbyBlocksListener extends HCListener{

	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}

	
	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityExplodeBlockst(EntityExplodeEvent e){
		e.blockList().clear();
	}
	
	
}
