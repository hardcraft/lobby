package com.gmail.val59000mc.lobby.listeners;

import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.lobby.callbacks.LobbyCallbacks;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.flowdispatcher.FlowDispatcherActionParser;
import com.gmail.val59000mc.lobby.killskilldispatcher.KillSkillDispatcherActionParser;
import com.gmail.val59000mc.lobby.perks.PerksManager;
import com.gmail.val59000mc.lobby.sig.FriendActionParser;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.lobby.stats.inventories.*;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterActionParserEvent;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterInventoryEvent;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterPlaceholderEvent;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.placeholders.SimplePlaceholder;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.google.common.collect.Lists;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LobbySigListener extends HCListener{

	private InventoryStats inventoryStats;
	
	@EventHandler
	public void onRegisterPlaceholders(SIGRegisterPlaceholderEvent e){

		e.getPm().registerNewPlaceholder(new SimplePlaceholder("{current-month-and-year}") {
			
			@Override
			public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
				if(original.contains("{current-month-and-year}")){
					Date now = new Date();
					String month = new SimpleDateFormat("MMMM", new Locale("fr")).format(now);
					String year = new SimpleDateFormat("yyyy", new Locale("fr")).format(now);
					return original.replaceAll(getPattern(), month+" "+year);
				}
				return original;
			}
		});
	}
	
	@EventHandler
	public void onRegisterActionParser(SIGRegisterActionParserEvent e){
		e.getAp().registerActionParser(new FriendActionParser(getPlugin()));
		e.getAp().registerActionParser(new KillSkillDispatcherActionParser(((LobbyCallbacks) getCallbacksApi()).getKillSkillDispatcher()));
		e.getAp().registerActionParser(new FlowDispatcherActionParser(((LobbyCallbacks) getCallbacksApi()).getFlowDispatcher()));
	}
	
	@EventHandler
	public void onRegisterStatisticsInventory(SIGRegisterInventoryEvent e){
		InventoryManager im = e.getIm();
		
		inventoryStats = new InventoryStats(
				getApi(), 
				e.getIm(), 
				getApi().getConfig().getLong("statistics.cache-expiration", Constants.STATISTICS_CACHE_EXPIRATION_MILLIS), 
				getApi().getConfig().getLong("statistics.access-expiration", Constants.STATISTICS_ACCESS_EXPIRATION_MILLIS)
		);	
		
		//////////////////////////////////////
		// STATISTICS_MAIN_GLOBAL_INVENTORY
		/////////////////////////////////////
		
		im.addInventory(buildStatisticsMainInventory());
		
		
	}
	
	@EventHandler
	public void onRegisterPerksInventory(SIGRegisterInventoryEvent e){
		InventoryManager im = e.getIm();
		PerksManager perksManager = ((LobbyCallbacks) getCallbacksApi()).getPerksManager();
		
		List<Inventory> inventories = new ArrayList<Inventory>();
		
		inventories.addAll(perksManager.buildKillSkillInventories());
		inventories.addAll(perksManager.buildEvolutionInventories());
		inventories.addAll(perksManager.buildCtfInventories());
		inventories.addAll(perksManager.buildSurvivalRushInventories());
		inventories.addAll(perksManager.buildGladiatorInventories());
		inventories.addAll(perksManager.buildRageInventories());

		
		for(Inventory inv : inventories){
			im.addInventory(inv);
		}
		
	}
	
	private Inventory buildStatisticsMainInventory() {
		HCStringsAPI s = getStringsApi();
		
		Inventory inventory = new Inventory(Constants.STATISTICS_MAIN_INVENTORY, s.get("lobby.statistics.main-inventory.title").toString(), 1);
		
		Item own = new Item.ItemBuilder(Material.SKULL_ITEM)
			.withDamage((short) 3)
			.withPlayerSkullName("{player}")
			.withName(s.get("lobby.statistics.main-inventory.own.name").toString())
			.withLore(s.getList("lobby.statistics.main-inventory.own.lore").toStringList())
			.withPosition(3)
			.build();
		own.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(inventoryStats, InventoryType.GLOBAL, "%player%", 0)));
		inventory.addItem(own);
		
		Item search = new Item.ItemBuilder(Material.BOOK_AND_QUILL)
			.withName(s.get("lobby.statistics.main-inventory.search.name").toString())
			.withLore(s.getList("lobby.statistics.main-inventory.search.lore").toStringList())
			.withPosition(4)
			.build();
		search.setActions(Lists.newArrayList(new OpenAnvilGUIAction(inventoryStats, InventoryType.GLOBAL)));
		inventory.addItem(search);
		
		Item ranking = new Item.ItemBuilder(Material.LADDER)
			.withName(s.get("lobby.statistics.main-inventory.ranking.name").toString())
			.withLore(s.getList("lobby.statistics.main-inventory.ranking.lore").toStringList())
			.withPosition(5)
			.build();
		ranking.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(inventoryStats, InventoryType.RANKING_GLOBAL, Game.GLOBAL, 0, SortBy.KILLS)));
		inventory.addItem(ranking);
		
		Item previous = new Item.ItemBuilder(Material.RAW_FISH)
			.withDamage((short) 3)
			.withName(s.get("lobby.statistics.back").toString())
			.withPosition(8)
			.build();
		previous.setActions(Lists.newArrayList(new OpenInventoryAction("games")));	
		inventory.addItem(previous);
		
		return inventory;
	}
}
