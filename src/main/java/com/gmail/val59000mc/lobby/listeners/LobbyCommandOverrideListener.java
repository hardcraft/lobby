package com.gmail.val59000mc.lobby.listeners;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class LobbyCommandOverrideListener extends HCListener{

	private HCCommand vanishExecutor;

	public LobbyCommandOverrideListener(HCCommand vanishExecutor) {
		this.vanishExecutor = vanishExecutor;
	}

	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		PluginCommand vanishCmd = Bukkit.getServer().getPluginCommand("vanish");
		if(vanishCmd.getPlugin().equals(HCGamesLib.getPlugin())){
			vanishExecutor.setApi(getApi());
			vanishCmd.setExecutor(vanishExecutor);
		}
	}
	
	
}
