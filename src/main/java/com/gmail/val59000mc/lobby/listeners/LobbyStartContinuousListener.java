package com.gmail.val59000mc.lobby.listeners;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCAfterContinuousEvent;
import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;

public class LobbyStartContinuousListener extends HCListener{

	@EventHandler
	public void afterStartContinous(HCAfterContinuousEvent e){

		HCGameAPI game = getApi();
		
		// DISTANCE
		String type = game.getConfig().getString("config.prevent-far-away.playing.type", com.gmail.val59000mc.hcgameslib.common.Constants.DEFAULT_PREVENT_FAR_AWAY_TYPE);
		final Double squared = game.getConfig().getDouble("config.prevent-far-away.playing.squared-distance",com.gmail.val59000mc.hcgameslib.common.Constants.DEFAULT_PREVENT_FAR_AWAY_PLAYING);
		
		// BOUNDS
		String min = game.getConfig().getString("config.prevent-far-away.playing.min",com.gmail.val59000mc.hcgameslib.common.Constants.DEFAULT_PREVENT_FAR_AWAY_MIN);
		String max = game.getConfig().getString("config.prevent-far-away.playing.max",com.gmail.val59000mc.hcgameslib.common.Constants.DEFAULT_PREVENT_FAR_AWAY_MAX);
		final World world = game.getWorldConfig().getWorld();
		final LocationBounds bounds = new LocationBounds(Parser.parseLocation(world, min), Parser.parseLocation(world, max));
		
		// prenvet below 0
		final boolean preventBelowZero = game.getConfig().getBoolean("config.prevent-far-away.playing.prevent-below-zero",com.gmail.val59000mc.hcgameslib.common.Constants.DEFAULT_PREVENT_BELOW_ZERO);
		
		// only launching if lib scheduler is disabling
		if((game.is(GameState.PLAYING) || game.is(GameState.CONTINUOUS)) && !game.getConfig().getBoolean("config.prevent-far-away.playing.enabled", false)){
			Location refLocation = game.getWorldConfig().getCenter();
			game.buildTask("prevent far away playing", new HCTask() {
				
				@Override
				public void run() {
					for(HCPlayer hcPlayer : getPmApi().getPlayers(true, null)){
						Player player = hcPlayer.getPlayer();
						
						if(player.getWorld().equals(world)){
							
							// below zero
							if((preventBelowZero && player.getLocation().getY() < 0)
								
							    // far from bounds		
							    || (type.equals("BOUNDS") && !bounds.contains(player.getLocation()))
							
							    // far from ref location
							    || (type.equals("DISTANCE") && player.getLocation().distanceSquared(refLocation) > squared)
							){
								player.teleport(refLocation);
								getStringsApi().get("messages.dont-leave-map").sendChatP(hcPlayer);
								getSoundApi().play(hcPlayer, Sound.VILLAGER_NO, 1, 1.3f);
							}
						}
						 
					}
				}
			})
			.withInterval(35)
			.addListener(new HCTaskListener(){
				@EventHandler
				public void onGameStateChange(HCBeforeEndEvent e){
					getScheduler().stop();
				}
			}).build().start();
			
		}
	}
}
