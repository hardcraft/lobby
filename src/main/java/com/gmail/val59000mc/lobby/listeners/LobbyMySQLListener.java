package com.gmail.val59000mc.lobby.listeners;

import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.lobby.callbacks.LobbyCallbacks;
import com.gmail.val59000mc.lobby.perks.PerksManager;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.spigotutils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import java.sql.SQLException;

public class LobbyMySQLListener extends HCListener{

	// Queries
	private String createLobbyPlayerSQL;
	private String createLobbyPlayerStatsSQL;
	private String createPlayerSponsorshipSQL;
	
	private String insertLobbyPlayerSQL;
	private String insertLobbyPlayerStatsSQL;
	private String updateLobbyPlayerGlobalStatsSQL;
	private String updateLobbyPlayerStatsSQL;

	private String insertKillSkillPlayerPerks;
	private String insertEvolutionPlayerPerks;
	private String insertCtfPlayerPerks;
	private String insertGladiatorPlayerPerks;
	private String insertSurvivalRushPlayerPerks;
	private String insertRagePlayerPerks;

	private PerksManager perksManager;
	
	/**
	 * Load sql queries from resources files
	 */
	public void readQueries() {
		HCMySQLAPI sql = getMySQLAPI();
		if(sql.isEnabled()){
			createLobbyPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_lobby_player.sql");
			createLobbyPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_lobby_player_stats.sql");
			createPlayerSponsorshipSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/sponsor/create_player_sponsorship.sql");
						
			insertLobbyPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_lobby_player.sql");
			insertLobbyPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_lobby_player_stats.sql");
			updateLobbyPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/update_lobby_player_global_stats.sql");
			updateLobbyPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/update_lobby_player_stats.sql");
		
			insertKillSkillPlayerPerks = sql.readQueryFromResource(getApi().getPlugin(), "sql/perks/insert_killskill_player_perks.sql");
			insertEvolutionPlayerPerks = sql.readQueryFromResource(getApi().getPlugin(), "sql/perks/insert_evo_player_perks.sql");
			insertCtfPlayerPerks = sql.readQueryFromResource(getApi().getPlugin(), "sql/perks/insert_ctf_player_perks.sql");
			insertGladiatorPlayerPerks = sql.readQueryFromResource(getApi().getPlugin(), "sql/perks/insert_gladiator_player_perks.sql");
			insertSurvivalRushPlayerPerks = sql.readQueryFromResource(getApi().getPlugin(), "sql/perks/insert_survivalrush_player_perks.sql");
			insertRagePlayerPerks = sql.readQueryFromResource(getApi().getPlugin(), "sql/perks/insert_rage_player_perks.sql");
		}
	}
	
	/**
	 * Insert game if not exists
	 * @param e
	 */
	@EventHandler
	public void onGameFinishedLoading(HCAfterLoadEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		readQueries();
		
		perksManager = ((LobbyCallbacks) getCallbacksApi()).getPerksManager();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					try{
						sql.execute(sql.prepareStatement(createLobbyPlayerSQL));
						
						sql.execute(sql.prepareStatement(createLobbyPlayerStatsSQL));
						
						sql.execute(sql.prepareStatement(createPlayerSponsorshipSQL));
						
					}catch(SQLException e){
						Logger.severe("Couldnt create tables for Lobby stats or perks");
						e.printStackTrace();
					}
					
				}
			});
		}
		
	}
	
	/**
	 * Add new player if not exists when joining
	 * Update name (because it may change)
	 * Update current played game 
	 * @param e
	 */
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(HCPlayerDBInsertedEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					String id =  String.valueOf(e.getHcPlayer().getId());

					try{ 
						// Insert ctf player if not exists
						sql.execute(sql.prepareStatement(insertLobbyPlayerSQL, id));
						
						// Insert ctf player stats if not exists
						sql.execute(sql.prepareStatement(insertLobbyPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));
						
						// Insert all player perks if not exists
						sql.execute(sql.prepareStatement(insertKillSkillPlayerPerks, id));
						sql.execute(sql.prepareStatement(insertEvolutionPlayerPerks, id));
						sql.execute(sql.prepareStatement(insertCtfPlayerPerks, id));
						sql.execute(sql.prepareStatement(insertGladiatorPlayerPerks, id));
						sql.execute(sql.prepareStatement(insertSurvivalRushPlayerPerks, id));
						sql.execute(sql.prepareStatement(insertRagePlayerPerks, id));

						// Load perks
						perksManager.reloadPlayerPerks((LobbyPlayer) e.getHcPlayer());
					
					
					} catch (SQLException ex) {
						Log.severe("Couldn't find perks for player "+e.getHcPlayer().getName());
						ex.printStackTrace();
					}
				}
			});
		}
	}
	
	
	/**
	 * Save all players global data when game ends
	 * @param e
	 */
	@EventHandler
	public void onGameEnds(HCPlayerDBPersistedSessionEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			LobbyPlayer lobbyPlayer = (LobbyPlayer) e.getHcPlayer();
			
			// Launch one async task to save all data
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{
							
						// Update ctf player global stats
						sql.execute(sql.prepareStatement(updateLobbyPlayerGlobalStatsSQL,
								String.valueOf(lobbyPlayer.getTimePlayed()),
								String.valueOf(lobbyPlayer.getId())
						));
						
	
						// Update ctf player stats
						sql.execute(sql.prepareStatement(updateLobbyPlayerStatsSQL,
								String.valueOf(lobbyPlayer.getTimePlayed()),
								String.valueOf(lobbyPlayer.getId()),
								sql.getMonth(),
								sql.getYear()
						));
					
					}catch(SQLException e){
						Logger.severe("Couldnt update Lobby player stats for player="+lobbyPlayer.getName()+" uuid="+lobbyPlayer.getUuid());
						e.printStackTrace();
					}
					
				}
			});
		}

	}
}
