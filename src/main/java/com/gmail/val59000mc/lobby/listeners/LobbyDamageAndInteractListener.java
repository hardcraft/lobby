package com.gmail.val59000mc.lobby.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.spigotmc.event.entity.EntityMountEvent;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class LobbyDamageAndInteractListener extends HCListener{
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onPotionSplash(PotionSplashEvent e){
		e.setCancelled(true);
	}
	
	// do not rotate item frame or painting
	@EventHandler
	public void onRotateItemFrame(PlayerInteractEntityEvent e){
		Entity entity = e.getRightClicked();
		if(entity.getType().equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}

	// do not damage item frame
	@EventHandler
	public void onDamageItemFrame(EntityDamageEvent e){
		EntityType type = e.getEntityType();
		if(type.equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}
	
	// prevent breaking hanging entity
	@EventHandler
	public void onBreakPainting(HangingBreakEvent e){
		e.setCancelled(true);
	}
	
	//prevent placing hanging entity
	@EventHandler
	public void onPlacePainting(HangingPlaceEvent e){
		e.setCancelled(true);
	}
	
	// do not damage minecart
	@EventHandler
	public void onDamageMinecart(VehicleDamageEvent e){
		EntityType type = e.getVehicle().getType();
		if(type.equals(EntityType.MINECART) 
				|| type.equals(EntityType.MINECART_CHEST)
				|| type.equals(EntityType.MINECART_HOPPER) 
				|| type.equals(EntityType.MINECART_FURNACE) 
				|| type.equals(EntityType.MINECART_TNT)){
			e.setCancelled(true);
		}
	}
	
	// do not empty buckets
	@EventHandler
	public void onUseBucket(PlayerBucketEmptyEvent e){
		e.setCancelled(true);
	}

	// do not fill buckets
	@EventHandler
	public void onUseBucket(PlayerBucketFillEvent e){
		e.setCancelled(true);
	}

	// do not enter bed
	@EventHandler
	public void onEnterBed(PlayerBedEnterEvent e){
		e.setCancelled(true);
	}
	
	// do not enter vehicle
	@EventHandler
	public void onEnterVehicle(EntityMountEvent e){
		if(e.getEntity().getType().equals(EntityType.PLAYER) && e.getMount().getType().equals(EntityType.MINECART)){
			e.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
    public void onInteractAtBlock(PlayerInteractEvent event){
    	Player player = event.getPlayer();
        
    	// do not light off fire
        if(event.getAction() == Action.LEFT_CLICK_BLOCK){
        	if(event.hasBlock() && event.getClickedBlock().getRelative(event.getBlockFace()).getType().equals(Material.FIRE)){
        		event.setCancelled(true);
        	}
        }
        
        // do not jump of soil
        if(event.getAction() == Action.PHYSICAL){
        	if(event.hasBlock() && event.getClickedBlock().getType().equals(Material.SOIL)){
        		event.setCancelled(true);
        	}
        }
	
    }

	// do not lose food
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
}
