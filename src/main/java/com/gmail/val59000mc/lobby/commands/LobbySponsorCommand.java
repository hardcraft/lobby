package com.gmail.val59000mc.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.sponsors.SponsorManager;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.spigotutils.Time;

public class LobbySponsorCommand extends HCCommand{

	private SponsorManager manager;
	
	public LobbySponsorCommand(JavaPlugin plugin) {
		super(plugin, "sponsor");
	}
	
	@Override
	public void onRegister(HCGameAPI api){
		this.manager = new SponsorManager(api);
		api.registerListener(manager);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		if(sender instanceof Player){

			HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) sender);
			if(hcPlayer == null)
				return true;
			
			if(args.length == 1){
				String name = args[0];
				
				if(name.equals("list")){
					manager.printWeekSponsoredByPlayer(hcPlayer);
				}else if(name.equals("help")){
					displayHelp(hcPlayer);
				}else if(name.equals("debug-confirm")){
					if(sender.isOp()){
						manager.schedulerhandleConfirmedAndInvalidSponsorships();
					}
				}else if(name.equals("debug-clean")){
					if(sender.isOp()){
						manager.schedulerCleanExpiredSponsorships();
					}
				}else if(name.length() <= 16){
					manager.addWaitingSponsor(hcPlayer, name);
				}else{
					displayHelp(hcPlayer);
				}
			}else{
				displayHelp(hcPlayer);
			}
			
			
		}

		return true;
	}

	private void displayHelp(HCPlayer hcPlayer) {
		FileConfiguration cfg = getAPi().getConfig();
		double unconfirmedReward = cfg.getDouble("sponsors.rewards.unconfirmed",0);
		double validatedReward = cfg.getDouble("sponsors.rewards.validated",0);
		String time = Time.getFormattedTime(cfg.getLong("sponsors.confirmation-delay-seconds",Constants.SPONSORS_CONFIRMATION_DELAY_SECONDS));
		int kills = cfg.getInt("sponsors.confirmation-kills",Constants.SPONSORS_CONFIRMATION_KILLS);
		
		String json = getStringsApi()
			.get("lobby.sponsors.command-help")
			.replace("%unconfirmedmoney%", String.valueOf(unconfirmedReward))
			.replace("%validatedmoney%", String.valueOf(validatedReward))
			.replace("%kills%", String.valueOf(kills))
			.replace("%time%", time)
			.toString();
		
		Texts.tellraw(hcPlayer.getPlayer(), json);
		
	}

}
