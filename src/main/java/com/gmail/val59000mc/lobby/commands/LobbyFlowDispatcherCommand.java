package com.gmail.val59000mc.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.lobby.callbacks.LobbyCallbacks;
import com.gmail.val59000mc.lobby.flowdispatcher.FlowDispatcher;

public class LobbyFlowDispatcherCommand extends HCCommand{

	private FlowDispatcher flowDispatcher;
	
	public LobbyFlowDispatcherCommand(JavaPlugin plugin) {
		super(plugin, "flowdispatcher");
	}
	
	@Override
	public void onRegister(HCGameAPI api){
		this.flowDispatcher = ((LobbyCallbacks)getCallbacksApi()).getFlowDispatcher();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length == 1){
			switch(args[0]){
				case "update":
					flowDispatcher.updateFlowPairs();
					getStringsApi().get("lobby.command.flow-dispatcher.updated-current-server").sendChatP(sender);
					break;
				default:
					displayHelp(sender);
					break;
			}
		}else if(args.length == 4){
			switch(args[0]){
				case "next":
					flowDispatcher.nextMap(args[1], args[2], args[3]);
					getStringsApi().get("lobby.command.flow-dispatcher.saved-new-dispatcher-server").replace("%server%",  args[1]+":"+args[2]).sendChatP(sender);
					break;
				default:
					displayHelp(sender);
					break;
			}
		}else{
			displayHelp(sender);
		}
		return true;
	}

	private void displayHelp(CommandSender sender) {
		getStringsApi().getList("lobby.command.flow-dispatcher.help").sendChatP(sender);
	}
	
}
