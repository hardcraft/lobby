package com.gmail.val59000mc.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class LobbyFlyCommand extends HCCommand{

	public LobbyFlyCommand(JavaPlugin plugin) {
		super(plugin, "fly");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		if(sender instanceof Player){
			Player player = (Player) sender;
			
			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			
			if(hcPlayer != null){
				
				if(player.getAllowFlight()){
					player.setFlying(false);
					player.setAllowFlight(false);
				}else{
					player.setAllowFlight(true);
					player.setFlying(true);
				}
				
				getStringsApi()
					.get((player.isFlying() ? "lobby.command.fly.enabled" : "lobby.command.fly.disabled"))
					.sendChatP(hcPlayer);
				
			}
			
			return true;
		}

		return false;
	}

}
