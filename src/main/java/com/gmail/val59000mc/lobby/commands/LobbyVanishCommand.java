package com.gmail.val59000mc.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class LobbyVanishCommand extends HCCommand{

	public LobbyVanishCommand(JavaPlugin plugin) {
		super(plugin, "vanish");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		if(sender instanceof Player){
			Player player = (Player) sender;
			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			if(hcPlayer != null){
				
				if(player.hasPermission("hardcraftpvp.be-vanished")){
					getAPi().getDependencies().removePerm(player.getName(), "hardcraftpvp.be-vanished");
					getStringsApi()
						.get("lobby.command.vanish.disabled")
						.sendChatP(hcPlayer);
				}else{
					getAPi().getDependencies().addPerm(player.getName(), "hardcraftpvp.be-vanished");
					getStringsApi()
						.get("lobby.command.vanish.enabled")
						.sendChatP(hcPlayer);
				}
				
			}
				
			return true;
		}

		return false;
	}

}
