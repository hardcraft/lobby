package com.gmail.val59000mc.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;

public class LobbySpawnCommand extends HCCommand{

	public LobbySpawnCommand(JavaPlugin plugin) {
		super(plugin, "spawn");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		if(sender instanceof Player){
			((Player) sender).teleport(getAPi().getWorldConfig().getLobby());
		}

		return true;
	}

}
