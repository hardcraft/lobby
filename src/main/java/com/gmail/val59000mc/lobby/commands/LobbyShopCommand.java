package com.gmail.val59000mc.lobby.commands;

import com.gmail.val59000mc.hcgameslib.api.HCDependenciesAPI;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.lobby.callbacks.LobbyCallbacks;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class LobbyShopCommand extends HCCommand{

	private String selectEnderchestRowsSql;
	private String updateEnderchestRowsSql;

	public LobbyShopCommand(JavaPlugin plugin) {
		super(plugin, "shop");
	}

    public void onRegister(HCGameAPI api){
        HCMySQLAPI sql = api.getMySQLAPI();
        selectEnderchestRowsSql = sql.readQueryFromResource(api.getPlugin(), "sql/shop/select_enderchest_rows.sql");
        updateEnderchestRowsSql = sql.readQueryFromResource(api.getPlugin(), "sql/shop/update_enderchest_rows.sql");
    }

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		// Syntax
		// /rank
		// /rank <player>
		
		HCDependenciesAPI dep = getDependenciesApi();

			
		if(args.length > 0){
			switch(args[0]){
				case "kill-skill":
					handleShopKillSkill(sender,args);
					break;
                case "help":
                    displayHelp(sender);
                    break;
				default:
                    syntaxError(sender);
					break;
			}
		}else{
            syntaxError(sender);
		}
			
		return true;
	}

    private void handleShopKillSkill(CommandSender sender, String[] args) {
        if(args.length >= 2){
            switch(args[1]){
                case "enderchest-rows":
                    handleShopKillSkillEnderchestRows(sender,args);
                    break;
                default:
                    syntaxError(sender);
                    break;
            }
        }else{
            syntaxError(sender);
        }
    }

    private void handleShopKillSkillEnderchestRows(CommandSender sender, String[] args) {
        if(args.length >= 5){
            switch(args[2]){
                case "add":
                    handleAddEnderchestRows(sender, args[3], args[4]);
                    break;
                default:
                    syntaxError(sender);
                    break;
            }
        }else{
            syntaxError(sender);
        }
    }

    private void handleAddEnderchestRows(CommandSender sender, String playerName, String rowsToAddStr) {

        HCMySQLAPI sql = getAPi().getMySQLAPI();
        if(sql.isEnabled()){

            getAPi().async(()->sql.selectPlayerByName(playerName), (res) ->{

                try {
                    if(res.next()){
                        int playerId = res.getInt("id");
                        UUID uuid = UUID.fromString(res.getString("uuid"));

                        try{
                            int rowsToAdd = Integer.parseInt(rowsToAddStr);
                            getAPi().async(()->performAddEnderchestRows(playerId, rowsToAdd), (rowsNow) ->{

                                Player targetPlayer = Bukkit.getPlayer(playerName);

                                if(rowsNow != 0){
                                    sender.sendMessage(
                                        getStringsApi()
                                            .get("lobby.command.shop.success-add-kill-skill-enderchest-rows")
                                            .replace("%player%", playerName)
                                            .replace("%rows%", String.valueOf(rowsNow))
                                            .toString()
                                    );
                                    if(targetPlayer != null){
                                        targetPlayer.sendMessage(
                                            getStringsApi()
                                                .get("lobby.command.shop.success-add-kill-skill-enderchest-rows-own")
                                                .replace("%rows%", String.valueOf(rowsNow))
                                                .toString()
                                        );
                                        LobbyPlayer lobbyPlayer = (LobbyPlayer) getPmApi().getHCPlayer(targetPlayer);
                                        ((LobbyCallbacks) getCallbacksApi()).getPerksManager().reloadPlayerPerks(lobbyPlayer);
                                    }
                                }else{
                                    String msg = getStringsApi()
                                        .get("lobby.command.shop.error-add-kill-skill-enderchest-rows")
                                        .replace("%player%", playerName)
                                        .toString();
                                    sender.sendMessage(msg);
                                    if(targetPlayer != null){
                                        targetPlayer.sendMessage(msg);
                                    }
                                }

                            });
                        }catch(IllegalArgumentException e){
                            sender.sendMessage(
                                getStringsApi()
                                    .get("lobby.command.shop.syntax-error-incorrect-value")
                                    .replace("%value%", rowsToAddStr)
                                    .toString()
                            );

                        }

                    }else{
                        sender.sendMessage(
                            getStringsApi()
                                .get("lobby.command.shop.unknown-player")
                                .replace("%player%", playerName)
                                .toString()
                        );
                    }
                } catch (SQLException e) {
                    msg(sender, "mysql-error");
                    e.printStackTrace();
                }

            });

        }else{
            msg(sender, "mysql-disabled");
        }

    }

    private int performAddEnderchestRows(int playerId, int rows) {
        int currentRows = getCurrentEnderchestRows(playerId);
        currentRows += rows;
        if(currentRows > 12)
            currentRows = 12;
        try {
            updateEnderchestRows(playerId, currentRows);
            return currentRows;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private int getCurrentEnderchestRows(int playerId){
        HCMySQLAPI sql = getAPi().getMySQLAPI();
        int currentEnderchestRows = 0;
        try {
            CachedRowSet res = sql.query(sql.prepareStatement(selectEnderchestRowsSql, String.valueOf(playerId)));
            if(res.next()){
                currentEnderchestRows = res.getInt("enderchest_rows");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return currentEnderchestRows;

    }

    private void updateEnderchestRows(int playerId, int rows) throws SQLException{
        HCMySQLAPI sql = getAPi().getMySQLAPI();
        sql.execute(sql.prepareStatement(updateEnderchestRowsSql, String.valueOf(rows), String.valueOf(playerId)));
    }

    private void syntaxError(CommandSender sender){
        msg(sender, "syntax-error");
    }

    private void displayHelp(CommandSender sender) {
        msgList(sender, "help");
    }


    private void msg(CommandSender sender, String subCode){
		sender.sendMessage(getStringsApi().get("lobby.command.shop."+subCode).toString());
	}

    private void msgList(CommandSender sender, String subCode){
        List<String> list = getStringsApi().getList("lobby.command.shop."+subCode).toStringList();
        for(String row : list){
            sender.sendMessage(row);
        }
    }

}
