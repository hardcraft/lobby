package com.gmail.val59000mc.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class LobbyBuildCommand extends HCCommand{

	public LobbyBuildCommand(JavaPlugin plugin) {
		super(plugin, "build");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		if(sender instanceof Player){
			HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) sender);
			if(hcPlayer != null){
				getBungeeApi().sendToServer(hcPlayer, "Build");
			}
		}

		return true;
	}

}
