package com.gmail.val59000mc.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.lobby.callbacks.LobbyCallbacks;
import com.gmail.val59000mc.lobby.killskilldispatcher.KillSkillDispatcher;

public class LobbyKillSkillDispatcherCommand extends HCCommand{

	private KillSkillDispatcher killSkillDispatcher;
	
	public LobbyKillSkillDispatcherCommand(JavaPlugin plugin) {
		super(plugin, "killskilldispatcher");
	}
	
	@Override
	public void onRegister(HCGameAPI api){
		this.killSkillDispatcher = ((LobbyCallbacks)getCallbacksApi()).getKillSkillDispatcher();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length == 1){
			switch(args[0]){
				case "next":
					Integer newServer = killSkillDispatcher.nextServer();
					getStringsApi().get("lobby.command.kill-skill-dispatcher.saved-new-dispatcher-server").replace("%server%",  String.valueOf(newServer)).sendChatP(sender);
					break;
				case "update":
					killSkillDispatcher.updateCurrent();
					getStringsApi().get("lobby.command.kill-skill-dispatcher.updated-current-server").sendChatP(sender);
					break;
				default:
					displayHelp(sender);
					break;
			}
		}else{
			displayHelp(sender);
		}
		return true;
	}

	private void displayHelp(CommandSender sender) {
		getStringsApi().getList("lobby.command.kill-skill-dispatcher.help").sendChatP(sender);
	}
	
}
