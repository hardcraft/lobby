package com.gmail.val59000mc.lobby.commands;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCDependenciesAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;

import net.md_5.bungee.api.ChatColor;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class LobbyRankCommand extends HCCommand{

	private final static Long RANK_PAY_DURATION = 2592000L; // 30 days in seconds
	
	public LobbyRankCommand(JavaPlugin plugin) {
		super(plugin, "rank");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {

		// Syntax
		// /rank
		// /rank <player>
		
		HCDependenciesAPI dep = getDependenciesApi();
		
		if(dep.isPermissionsExEnabled() && dep.isVaultEnabled()){
			
			if(args.length > 0){
				switch(args[0]){
					case "get":
						handleGetRank(sender,args);
						break;
					case "add":
						handleAddRank(sender,args);
						break;
					case "pay":
						if(sender instanceof Player){
							handlePayRank((Player) sender,args);
						}else{
							sender.sendMessage(getStringsApi().get("lobby.command.rank.not-a-player").toString());
						}
						break;
					case "remove":
						handleRemove(sender,args);
					break;
					default:
						sender.sendMessage(getStringsApi().get("lobby.command.rank.error").toString());	
						break;
				}
			}else{
				sender.sendMessage(getStringsApi().get("lobby.command.rank.error").toString());	
			}
			
		}else{
			sender.sendMessage(getStringsApi().get("lobby.command.rank.pex-or-vault-not-installed").toString());
		}
			
		return true;
	}
	
	/**
	 * rank pay <VIP / VIP+>
	 * @param player
	 * @param args
	 */
	private void handlePayRank(Player player, String[] args) {

		String syntaxErrorMessage = ChatColor.RED+"/rank pay <VIP / VIP+>";

		if(player.hasPermission("hardcraftpvp.rank.pay")){
			if(args.length == 2){
				
				if(!args[1].equals("VIP") && !args[1].equals("VIP+")){
					player.sendMessage(syntaxErrorMessage);
					return;
				}else{
					doPayRank(player,args[1], RANK_PAY_DURATION);
				}
			}else{
				player.sendMessage(syntaxErrorMessage);
			}
			
			
		}else{
			player.sendMessage(getStringsApi().get("lobby.command.rank.cannot-pay-rank").toString());
		}
		
	}
	
	/**
	 * rank add <player> <VIP / VIP+> <time>
	 * @param sender
	 * @param args
	 */
	private void handleAddRank(CommandSender sender, String[] args) {

		String syntaxErrorMessage = ChatColor.RED+"/rank add <player> <VIP / VIP+> <time>";

		if(sender.hasPermission("hardcraftpvp.rank.add")){
			if(args.length == 4){
				
				String playerName = args[1];
				
				Long time = 0L;
				try{
					time = Long.parseLong(args[3]);
				}catch(IllegalArgumentException e){
					sender.sendMessage(syntaxErrorMessage);
					return;
				}
				
				if(!args[2].equals("VIP") && !args[2].equals("VIP+")){
					sender.sendMessage(syntaxErrorMessage);
					return;
				}

					doAddRank(sender,playerName,args[2], time);
			}else{
				sender.sendMessage(syntaxErrorMessage);
			}
			
			
		}else{
			sender.sendMessage(getStringsApi().get("lobby.command.rank.cannot-add-rank").toString());
		}
		
	}
	
	
	private void handleRemove(CommandSender sender, String[] args) {
		String syntaxErrorMessage = ChatColor.RED+"/rank remove <player>";

		if(sender.hasPermission("hardcraftpvp.rank.remove")){
			if(args.length == 2){
				
				String playerName = args[1];
				
				List<String> playerRanks = getRanks(playerName);	
				for(String rank : playerRanks){
					removeRank(playerName, rank);
				}

				sender.sendMessage(getStringsApi().get("lobby.command.rank.removed").toString());
				
				getPmApi().refreshRankNameTags(20);
				
			}else{
				sender.sendMessage(syntaxErrorMessage);
			}
			
			
		}else{
			sender.sendMessage(getStringsApi().get("lobby.command.rank.cannot-remove-rank").toString());
		}
	}

	private void doAddRank(CommandSender sender, String playerName, String rankName, Long time) {
		List<String> playerRanks = getRanks(playerName);	
		switch (rankName){
			case "VIP":
				if( playerRanks.contains("Builder") ||
					playerRanks.contains("Helper") ||
					playerRanks.contains("Modérateur") ||
					playerRanks.contains("Administrateur")){
						sender.sendMessage(getStringsApi().get("lobby.command.rank.cannot-buy-lower-rank").toString());
				}else{

					if(playerRanks.contains("VIP+")){
						Long remainingTime = getRemainingTimeRank(playerName, "VIP+");
						if(remainingTime != null){
							removeRank(playerName, "VIP+");
							addRankTime(playerName, rankName, remainingTime + time);
						}
					}else{
						addRankTime(playerName, rankName, time);
					}
					
					sender.sendMessage(getStringsApi().get("lobby.command.rank.added").toString());
					
				}
				break;
				
			case "VIP+":

				if( playerRanks.contains("Builder") ||
					playerRanks.contains("Helper") ||
					playerRanks.contains("Modérateur") ||
					playerRanks.contains("Administrateur")){
						sender.sendMessage(getStringsApi().get("lobby.command.rank.cannot-buy-lower-rank").toString());
				}else{
					if(playerRanks.contains("VIP")){
						Long remainingTime = getRemainingTimeRank(playerName, "VIP");
						if(remainingTime != null){
							removeRank(playerName, "VIP");
							addRankTime(playerName, rankName, time + (remainingTime/2));
						}
					}else{
						addRankTime(playerName, rankName, time);
					}
					
					sender.sendMessage(getStringsApi().get("lobby.command.rank.added").toString());
				}
				break;
				
		}
		getPmApi().refreshRankNameTags(20);
	}
	
	
	private void doPayRank(Player player, String rankName, Long time) {
		List<String> playerRanks = getRanks(player.getName());	
		String playerName = player.getName();
		HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
		if(hcPlayer != null){

			switch (rankName){
				case "VIP":
					if( playerRanks.contains("VIP+") || 
						playerRanks.contains("Builder") ||
						playerRanks.contains("Helper") ||
						playerRanks.contains("Modérateur") ||
						playerRanks.contains("Administrateur")){
							getStringsApi().get("lobby.command.rank.cannot-buy-lower-rank").sendChatP(hcPlayer);
							Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
					}else{
						
						Double vipMonthRankPrice = getAPi().getConfig().getDouble("ranks.VIP", Constants.VIP_MONTH_PRICE);
						
						if(getDependenciesApi().hasEnoughMoney(player, vipMonthRankPrice)){
							getDependenciesApi().removeMoney(player, vipMonthRankPrice);
							addRankTime(playerName, rankName, time);
							getStringsApi().get("lobby.command.rank.purchased").sendChatP(hcPlayer);
							Sounds.play(player, Sound.LEVEL_UP, 1, 2);
						}else{
							getStringsApi().get("lobby.command.rank.not-enough-money").sendChatP(hcPlayer);
							Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
						}
						
					}
					break;
				case "VIP+":
					if( playerRanks.contains("Builder") ||
						playerRanks.contains("Helper") ||
						playerRanks.contains("Modérateur") ||
						playerRanks.contains("Administrateur")){
							getStringsApi().get("lobby.command.rank.cannot-buy-lower-rank").sendChatP(hcPlayer);
							Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
						}else{
							
							Double vipPlusMonthRankPrice = getAPi().getConfig().getDouble("ranks.VIP+", Constants.VIP_PLUS_MONTH_PRICE);
							
							if(getDependenciesApi().hasEnoughMoney(player, vipPlusMonthRankPrice)){
								
								if(playerRanks.contains("VIP")){
									Long remainingTime = getRemainingTimeRank(playerName, "VIP");
									if(remainingTime != null){
										removeRank(playerName, "VIP");
										addRankTime(playerName, rankName, time + (remainingTime/2));
									}
								}else{
									addRankTime(playerName, rankName, time);
								}
								
								getDependenciesApi().removeMoney(player, vipPlusMonthRankPrice);
								getStringsApi().get("lobby.command.rank.purchased").sendChatP(hcPlayer);
								getSoundApi().play(hcPlayer, Sound.LEVEL_UP, 1, 2);
								Sounds.play(player, Sound.LEVEL_UP, 1, 2);
							}else{
								getStringsApi().get("lobby.command.rank.not-enough-money").sendChatP(hcPlayer);
								getSoundApi().play(hcPlayer, Sound.VILLAGER_NO, 1, 2);
							}
							
						}
					break;
			}
			
			getPmApi().refreshRankNameTags(20);
		}
	}

	private void handleGetRank(CommandSender sender, String[] args) {
		
		if(args.length == 1){
			if(sender instanceof Player){
				displayRemainingRanksTime(sender.getName(), sender);
			}else{
				sender.sendMessage(getStringsApi().get("lobby.command.rank.not-a-player").toString());
			}
		}else if(args.length == 2){
			String playerName = args[1];
				
			if(sender instanceof Player){
				Player playerSender = (Player) sender;
				if(playerSender.getName().equals(playerName)){
					displayRemainingRanksTime(playerName,sender);
				}else{
					if(sender.hasPermission("hardcraftpvp.rank.getother")){
						displayRemainingRanksTime(playerName,sender);
					}else{
						sender.sendMessage(getStringsApi().get("lobby.command.rank.cannot-see-other").toString());
					}
				}
			}else{
				displayRemainingRanksTime(playerName,sender);
			}
			
		}else{
			sender.sendMessage(getStringsApi().get("lobby.command.rank.error").toString());
		}
		
	}

	private void displayRemainingRanksTime(String playerName, CommandSender displayTo){
		if(!getDependenciesApi().isPermissionsExEnabled())
			return;
		
		List<String> ranks = getRanks(playerName);
		 
		if(ranks.size() == 0){
			displayTo.sendMessage(getStringsApi().get("lobby.command.rank.no-rank").replace("%player%",playerName).toString());
		}else{
			displayTo.sendMessage(getStringsApi().get("lobby.command.rank.ranks-of-player").replace("%player%", playerName).toString());
			for(String rank : ranks){
				Long remainingTime = getRemainingTimeRank(playerName, rank);
				String message = getStringsApi()
						.get("lobby.command.rank.remaining-time")
						.replace("%rank%", rank)
						.replace("%time%", (remainingTime == null ? "Permanent" : Time.getFormattedTime(remainingTime)))
						.toString();
				displayTo.sendMessage(message);
			}
		}
	}
	
	private void addRankTime(String playerName, String group, Long timeInSeconds){
		if(!getDependenciesApi().isPermissionsExEnabled())
			return;
		
		// Equivalent to /pex user <user> group add <group> [world] [lifetime]
		// For all worlds use /pex user <user> group add <group> "" [lifetime]
		Long remaining = getRemainingTimeRank(playerName,group);
		remaining = (remaining == null) ? 0L : remaining;
		PermissionsEx.getUser(playerName).addGroup(group, null, remaining+timeInSeconds);
		PermissionsEx.getUser(playerName).save();
	}
	
	private void removeRank(String playerName, String group){
		PermissionsEx.getUser(playerName).removeGroup(group);
		PermissionsEx.getUser(playerName).setOption("group-"+group+"-until", null);
	}
	
	private Long getRemainingTimeRank(String playerName, String group){
		if(!getDependenciesApi().isPermissionsExEnabled())
			return null;
		
		// Option : group-VIP-until
		// Option : group-VIP+-until
		Long now = Calendar.getInstance().getTimeInMillis()/1000;
		String timeStr = PermissionsEx.getUser(playerName).getOption("group-"+group+"-until", null);
		if(timeStr == null){
			return null;
		}else{
			Long until;
			try{
				until = Long.parseLong(timeStr);
			}catch(IllegalArgumentException e){
				until = 0L;
			}
			if(until < now){
				return 0L;
			}else{
				return until-now;
			}
		}
	}
	
	private List<String> getRanks(String playerName){
		List<String> groups = new ArrayList<String>();
		if(!getDependenciesApi().isPermissionsExEnabled())
			return groups;
		
		for(PermissionGroup permGroup : PermissionsEx.getUser(playerName).getParents()){
			groups.add(permGroup.getName());
		}
		return groups;
	}

}
