package com.gmail.val59000mc.lobby.stats.inventories.ranking;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.AbstractCachedInventory;
import com.gmail.val59000mc.lobby.stats.inventories.GlobalRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.MonthRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.OpenStatisticsInventoryAction;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.spigotutils.Numbers;
import com.google.common.collect.Lists;

public abstract class RankingItemBuilder{

	public abstract List<String> buildGlobalLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy);
	

	public abstract List<String> buildMonthLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy);


	protected String boldIfSortedBy(SortBy requiredSortBy, SortBy actualSortBy, String string) {
		return requiredSortBy.equals(actualSortBy) ? "§6§l"+ChatColor.stripColor(string) : string;
	}
	
	protected String formatTime(int time) {

		int d,h;
		d = (int) time / (60 * 24);
		time -= d * ( 60 * 24);
		h = (int) time / 60;
		time -= h * 60;
		
		
		if(d == 0){
			if(h==0){
				return time+"m";
			}else{
				return h+"h "+time+"m ";		
			}
		}else{
			return d+"j "+h+"h "+time+"m ";
		}
	}


	public abstract void addGlobalFiltersItems(InventoryStrings s, GlobalRankingInventory inventory);
	
	public abstract void addMonthFiltersItems(InventoryStrings s, MonthRankingInventory inventory);
	

	
	protected double kdRatio(int kills, int deaths){
		if(deaths == 0)
			return kills;
		return Numbers.round((double) kills / (double) deaths, 2);
	}
	
	// enchantement lore on selected sortBy filter

	private Integer changeAmountIfSortBy(SortBy requiredSortBy, SortBy actualSortBy) {
		return requiredSortBy.equals(actualSortBy) ? 0 : 1;
	}

	// empty
	
	public void addEmptyFilter(AbstractCachedInventory inventory, int position){
		Item empty = new Item.ItemBuilder(Material.STONE_BUTTON)
				.withName("  ")
				.withRequireUniqueKey(true)
				.withPosition(position)
				.build();
		inventory.getInventory().addItem(empty);
	}
	
	// kills
	
	public void addKillsFilter(InventoryStrings s, GlobalRankingInventory inventory, int position){
		Item kills = getKillsFilter(s,position,inventory.getSortBy());
		kills.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.KILLS)));
		inventory.getInventory().addItem(kills);
	}
	
	public void addKillsFilter(InventoryStrings s, MonthRankingInventory inventory, int position){
		Item kills = getKillsFilter(s,position,inventory.getSortBy());
		kills.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.KILLS, inventory.getMonth(), inventory.getYear())));
		inventory.getInventory().addItem(kills);
	}
	
	private Item getKillsFilter(InventoryStrings s, int position, SortBy sortBy){
		return new Item.ItemBuilder(Material.IRON_SWORD)
				.withName(s.filterKills)
				.withPosition(position)
				.withRequireUniqueKey(true)
				.withAmount(changeAmountIfSortBy(SortBy.KILLS, sortBy))
				.build();
	}
	
	
	
	// deaths


	public void addDeathsFilter(InventoryStrings s, GlobalRankingInventory inventory, int position){
		Item deaths = getDeathsFilter(s, position,inventory.getSortBy());
		deaths.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.DEATHS)));
		inventory.getInventory().addItem(deaths);
	}
	
	public void addDeathsFilter(InventoryStrings s, MonthRankingInventory inventory, int position){
		Item deaths = getDeathsFilter(s, position,inventory.getSortBy());
		deaths.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.DEATHS, inventory.getMonth(), inventory.getYear())));
		inventory.getInventory().addItem(deaths);
	}
	
	private Item getDeathsFilter(InventoryStrings s, int position, SortBy sortBy){
		return new Item.ItemBuilder(Material.SKULL_ITEM)
				.withDamage((short) 3)
				.withPlayerSkullTexture(Constants.DEAD_SKULL)
				.withName(s.filterDeaths)
				.withPosition(position)
				.withRequireUniqueKey(true)
				.withAmount(changeAmountIfSortBy(SortBy.DEATHS, sortBy))
				.build();
	}

	
	// ratio
	
	public void addRatioFilter(InventoryStrings s, GlobalRankingInventory inventory, int position){
		Item ratio = getRatioFilter(s,position,inventory.getSortBy());
		ratio.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.RATIO)));
		inventory.getInventory().addItem(ratio);
	}
	
	public void addRatioFilter(InventoryStrings s, MonthRankingInventory inventory, int position){
		Item ratio = getRatioFilter(s,position,inventory.getSortBy());
		ratio.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.RATIO, inventory.getMonth(), inventory.getYear())));
		inventory.getInventory().addItem(ratio);
	}
	
	private Item getRatioFilter(InventoryStrings s, int position, SortBy sortBy){
		return new Item.ItemBuilder(Material.SKULL_ITEM)
				.withDamage((short) 3)
				.withPlayerSkullTexture(Constants.TARGET)
				.withName(s.filterRatio)
				.withPosition(position)
				.withRequireUniqueKey(true)
				.withAmount(changeAmountIfSortBy(SortBy.RATIO, sortBy))
				.build();
	}
	
	// coins
	
	public void addCoinsFilter(InventoryStrings s, GlobalRankingInventory inventory, int position){
		Item coins = getCoinsFilter(s, position,inventory.getSortBy());
		coins.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.COINS)));
		inventory.getInventory().addItem(coins);
	}
	
	public void addCoinsFilter(InventoryStrings s, MonthRankingInventory inventory, int position){
		Item coins = getCoinsFilter(s, position,inventory.getSortBy());
		coins.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.COINS, inventory.getMonth(), inventory.getYear())));
		inventory.getInventory().addItem(coins);
	}
	
	private Item getCoinsFilter(InventoryStrings s, int position, SortBy sortBy){
		return new Item.ItemBuilder(Material.EMERALD)
				.withName(s.filterCoins)
				.withPosition(position)
				.withRequireUniqueKey(true)
				.withAmount(changeAmountIfSortBy(SortBy.COINS, sortBy))
				.build();
	}
	
	// play time
	
	public void addPlayTimeFilter(InventoryStrings s, GlobalRankingInventory inventory, int position){
		Item playTime = getPlayTimeFilter(s, position,inventory.getSortBy());
		playTime.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.PLAY_TIME)));
		inventory.getInventory().addItem(playTime);
	}
	
	public void addPlayTimeFilter(InventoryStrings s, MonthRankingInventory inventory, int position){
		Item playTime = getPlayTimeFilter(s, position,inventory.getSortBy());
		playTime.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.PLAY_TIME, inventory.getMonth(), inventory.getYear())));
		inventory.getInventory().addItem(playTime);
	}
	
	private Item getPlayTimeFilter(InventoryStrings s, int position, SortBy sortBy){
		return new Item.ItemBuilder(Material.SKULL_ITEM)
				.withDamage((short) 3)
				.withPlayerSkullTexture(Constants.CLOCK)
				.withName(s.filterPlayTime)
				.withPosition(position)
				.withRequireUniqueKey(true)
				.withAmount(changeAmountIfSortBy(SortBy.PLAY_TIME, sortBy))
				.build();
	}
	
	// wins
	
	public void addWinsFilter(InventoryStrings s, GlobalRankingInventory inventory, int position){
		Item wins = getWinsFilter(s, position,inventory.getSortBy());
		wins.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.WINS)));
		inventory.getInventory().addItem(wins);
	}
	
	public void addWinsFilter(InventoryStrings s, MonthRankingInventory inventory, int position){
		Item wins = getWinsFilter(s, position,inventory.getSortBy());
		wins.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), inventory.getType(), inventory.getGame(), 0, SortBy.WINS, inventory.getMonth(), inventory.getYear())));
		inventory.getInventory().addItem(wins);
	}
	
	private Item getWinsFilter(InventoryStrings s, int position, SortBy sortBy){
		return new Item.ItemBuilder(Material.SKULL_ITEM)
				.withDamage((short) 3)
				.withPlayerSkullTexture(Constants.CHECKBOARD)
				.withName(s.filterWins)
				.withPosition(position)
				.withRequireUniqueKey(true)
				.withAmount(changeAmountIfSortBy(SortBy.WINS, sortBy))
				.build();
	}
	
}
