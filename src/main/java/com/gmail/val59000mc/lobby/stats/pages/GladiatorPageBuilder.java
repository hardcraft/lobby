package com.gmail.val59000mc.lobby.stats.pages;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

import java.util.ArrayList;
import java.util.List;

public class GladiatorPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();
		
		content.add(s.title);
		content.add(s.gladiator);
		
		content.add(" ");
		
		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthGladiatorKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthGladiatorDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthGladiatorCoinsEarned);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthGladiatorPlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalGladiatorKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalGladiatorDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalGladiatorCoinsEarned);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalGladiatorPlayTime));
		return content;
	}

}
