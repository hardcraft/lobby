package com.gmail.val59000mc.lobby.stats.pages;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;

public class PageStrings {

	private HCGameAPI api;
	
	public PageStrings(HCGameAPI api) {
		this.api = api;
		setup();
	}

	public HCGameAPI getApi() {
		return api;
	}

	protected String kills;
	protected String deaths;
	protected String playTime;
	protected String coinsEarned;
	protected String wins;
	protected String globalStats;
	protected String monthStats;
	protected String title;
	
	protected String all;
	protected String lobby;
	protected String killSkill;
	protected String survivalRush;
	protected String evolution;
	protected String ctf;
	protected String gladiator;
	protected String rage;
	protected String tdm;

	public void setup() {
		HCStringsAPI s = api.getStringsAPI();
		
		kills = s.get("lobby.scoreboard.kills").toString(); 
		deaths = s.get("lobby.scoreboard.deaths").toString(); 
		playTime = s.get("lobby.scoreboard.play-time").toString(); 
		coinsEarned = s.get("lobby.scoreboard.coins-earned").toString(); 
		wins = s.get("lobby.scoreboard.wins").toString(); 
		globalStats = s.get("lobby.scoreboard.global-stats").toString(); 
		monthStats = s.get("lobby.scoreboard.month-stats").toString(); 
		title = s.get("lobby.scoreboard.title").toString(); 
		all = s.get("lobby.scoreboard.pages.all").toString(); 
		lobby = s.get("lobby.scoreboard.pages.lobby").toString(); 
		killSkill = s.get("lobby.scoreboard.pages.kill-skill").toString(); 
		survivalRush = s.get("lobby.scoreboard.pages.survival-rush").toString(); 
		evolution = s.get("lobby.scoreboard.pages.evolution").toString(); 
		ctf = s.get("lobby.scoreboard.pages.ctf").toString(); 
		gladiator = s.get("lobby.scoreboard.pages.gladiator").toString(); 
		rage = s.get("lobby.scoreboard.pages.rage").toString();
		tdm = s.get("lobby.scoreboard.pages.tdm").toString();

	}



}
