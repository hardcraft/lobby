package com.gmail.val59000mc.lobby.stats.inventories;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.lobby.stats.InventoryStats;

public enum InventoryType {

	GLOBAL(GlobalInventory.class),
	MONTH(MonthInventory.class),
	RANKING_GLOBAL(GlobalRankingInventory.class),
	RANKING_MONTH(MonthRankingInventory.class);
	
	private Class<?> clazz;

	private InventoryType(Class<?> clazz){
		this.clazz = clazz;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends AbstractCachedInventory> T newCachedInventory(InventoryStats stats, Object... args){
		try {
			
			Object[] constructorParams = new Object[1+args.length];
			Class<?>[] targetParams = new Class<?>[1+args.length];
			constructorParams[0] = stats;
			targetParams[0] = stats.getClass();
			for(int i=0 ; i<args.length ; i++){
				constructorParams[1+i] = args[i];
				targetParams[1+i] = args[i].getClass();
			}
			
			Constructor<?> constructor = clazz.getConstructor(targetParams);
			
			return (T) constructor.newInstance(constructorParams);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				 | SecurityException | NoSuchMethodException e) {
			Log.severe(ExceptionUtils.getStackTrace(e));
		}
		return null;
	}


	/*public String generateName(Object... args){
		try {
			Object obj = clazz.getConstructor().newInstance();
			
			Class<?>[] argsTypes = new Class[args.length];
			for(int i=0 ; i<args.length ; i++){
				argsTypes[i] = args.getClass();
			}
			
			return (String) obj.getClass().getMethod("generateName", argsTypes).invoke(obj, args);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				 | SecurityException | NoSuchMethodException e) {
			throw new IllegalArgumentException("Cannot construct cached inventory instance of type " + this.name());
		}
	}*/
	
}
