 package com.gmail.val59000mc.lobby.stats.pages;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

import java.util.ArrayList;
import java.util.List;

public class TdmPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();

		content.add(s.title);
		content.add(s.tdm);
		
		content.add(" ");
		
		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthTdmKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthTdmDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthTdmCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.monthTdmWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthTdmPlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalTdmKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalTdmDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalTdmCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.globalTdmWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalTdmPlayTime));
		return content;
	}

}
