package com.gmail.val59000mc.lobby.stats.inventories.ranking;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.GlobalRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.MonthRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.google.common.collect.Lists;

import java.util.List;

public class LobbyBuilder extends RankingItemBuilder{

	@Override
	public List<String> buildGlobalLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.globalLobbyPlayTime))
		);
	}

	@Override
	public List<String> buildMonthLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.monthLobbyPlayTime))
		);
	}
	@Override
	public void addGlobalFiltersItems(InventoryStrings s, GlobalRankingInventory inventory) {
		addEmptyFilter(inventory, Constants.KILLS_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.DEATHS_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.RATIO_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.WINS_FILLTER_POSITION);
	}

	@Override
	public void addMonthFiltersItems(InventoryStrings s, MonthRankingInventory inventory) {
		addEmptyFilter(inventory, Constants.KILLS_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.DEATHS_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.RATIO_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.WINS_FILLTER_POSITION);
	}
}
