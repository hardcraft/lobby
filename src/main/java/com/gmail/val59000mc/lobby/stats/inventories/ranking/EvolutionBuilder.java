package com.gmail.val59000mc.lobby.stats.inventories.ranking;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.GlobalRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.MonthRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.google.common.collect.Lists;

import java.util.List;

public class EvolutionBuilder extends RankingItemBuilder{

	@Override
	public List<String> buildGlobalLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.globalEvolutionKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.globalEvolutionDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.globalEvolutionKills, lobbyPlayer.globalEvolutionDeaths)),
			s.starsEarned+lobbyPlayer.globalEvolutionStarsEarned,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.globalEvolutionCoinsEarned),
			boldIfSortedBy(SortBy.WINS, sortBy, s.wins+lobbyPlayer.globalEvolutionWins),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.globalEvolutionPlayTime))
		);
	}

	@Override
	public List<String> buildMonthLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.monthEvolutionKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.monthEvolutionDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.monthEvolutionKills, lobbyPlayer.monthEvolutionDeaths)),
			s.starsEarned+lobbyPlayer.monthEvolutionStarsEarned,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.monthEvolutionCoinsEarned),
			boldIfSortedBy(SortBy.WINS, sortBy, s.wins+lobbyPlayer.monthEvolutionWins),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.monthEvolutionPlayTime))
		);
	}

	@Override
	public void addGlobalFiltersItems(InventoryStrings s, GlobalRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addWinsFilter(s, inventory, Constants.WINS_FILLTER_POSITION);
	}

	@Override
	public void addMonthFiltersItems(InventoryStrings s, MonthRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addWinsFilter(s, inventory, Constants.WINS_FILLTER_POSITION);
	}
	
}
