package com.gmail.val59000mc.lobby.stats.inventories;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.ranking.*;

public enum Game {
	GLOBAL("Tous les jeux", "Global","player","player_stats", new GlobalBuilder()),
	LOBBY("Lobby", "Lobby","lobby_player","lobby_player_stats", new LobbyBuilder()),
	KILL_SKILL("Kill Skill", "K.Skill","killskill_player","killskill_player_stats", new KillSkillBuilder()),
	SURVIVAL_RUSH("Survival Rush", "S.Rush","survivalrush_player","survivalrush_player_stats", new SurvivalRushBuilder()),
	EVOLUTION("Evolution", "Evo","evo_player","evo_player_stats", new EvolutionBuilder()),
	CTF("Capture The Flag", "CTF","ctf_player","ctf_player_stats", new CtfBuilder()),
	GLADIATOR("Gladiator", "Gladia.","gladiator_player","gladiator_player_stats", new GladiatorBuilder()),
	RAGE("Rage", "Rage","rage_player","rage_player_stats", new RageBuilder()),
	TDM("Team Deathmatch", "TDM","tdm_player","tdm_player_stats", new TdmBuilder());

	private String name;
	private String shortName;
	private String globalTable;
	private String monthTable;
	private RankingItemBuilder itemLoreBuilder;
	
	private Game(String name, String shortName, String globalTable, String monthTable, RankingItemBuilder itemLoreBuilder) {
		this.name = name;
		this.shortName = shortName;
		this.globalTable = globalTable;
		this.monthTable = monthTable;
		this.itemLoreBuilder = itemLoreBuilder;
	}
	

	public String getShortName() {
		return shortName;
	}

	public String getName() {
		return name;
	}

	public String getGlobalTable() {
		return globalTable;
	}

	public String getMonthTable() {
		return monthTable;
	}

	public String getRankingGlobalItemLore(InventoryStrings strings, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return "<br>"+String.join("<br>", this.itemLoreBuilder.buildGlobalLore(strings,lobbyPlayer, sortBy));
	}

	public String getRankingMonthItemLore(InventoryStrings strings, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return "<br>"+String.join("<br>", this.itemLoreBuilder.buildMonthLore(strings, lobbyPlayer, sortBy));
	}


	public void addGlobalFiltersItems(InventoryStrings strings, GlobalRankingInventory inventory) {
		this.itemLoreBuilder.addGlobalFiltersItems(strings,inventory);
	}
	
	public void addMonthFiltersItems(InventoryStrings strings, MonthRankingInventory inventory) {
		this.itemLoreBuilder.addMonthFiltersItems(strings,inventory);
	}
	
	
}
