package com.gmail.val59000mc.lobby.stats.inventories;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.google.common.collect.Lists;
import org.bukkit.DyeColor;
import org.bukkit.Material;

import java.time.LocalDateTime;
import java.util.List;

public abstract class AbstractRankingMonthStatsInventory extends AbstractCachedInventory {

    protected int page;
    protected SortBy sortBy;
    protected int month;
    protected int year;
    protected Game game;
    protected String monthString;
    protected List<LobbyPlayer> lobbyPlayers;
    protected LobbyPlayer monthSum;


    public SortBy getSortBy() {
        return sortBy;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public Game getGame() {
        return game;
    }

    public AbstractRankingMonthStatsInventory(InventoryStats stats, Game game, Integer page, SortBy sortBy, Integer month, Integer year) {
        super(
                stats,
                buildName(game, page, sortBy.getColumnName(), month, year)
        );
        this.page = page;
        this.sortBy = sortBy;
        this.game = game;
        initDates(month, year);
    }

    private void initDates(int aMonth, int aYear) {
        if (aMonth == 0 && aYear == 0) {
            LocalDateTime now = LocalDateTime.now();
            this.month = now.getMonth().getValue();
            this.year = now.getYear();
        } else {
            this.month = aMonth;
            this.year = aYear;
        }

        this.monthString = stats.monthIntToString(month);
    }

    private static String buildName(Game aGame, Integer aPage, String aSortBy, Integer aMonth, Integer aYear) {
        String name = "statistics-month-ranking-" + aGame.getName() + "-" + aPage + "-" + aSortBy + "-";
        if (aMonth == 0 && aYear == 0) {
            LocalDateTime now = LocalDateTime.now();
            int currentMonth = now.getMonth().getValue();
            int currentYear = now.getYear();
            name += currentMonth + "-" + currentYear;
        } else {
            name += aMonth + "-" + aYear;
        }
        return name;
    }

    protected void addGameSelectorItems() {

        Inventory inventory = this.getInventory();
        InventoryStrings s = stats.getStrings();

        Item global = new Item.ItemBuilder(Material.SKULL_ITEM)
                .withDamage((short) 3)
                .withPlayerSkullName("{player}")
                .withName(s.all)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.allStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.kills + monthSum.monthKills,
                        s.deaths + monthSum.monthDeaths,
                        s.coinsEarned + monthSum.monthCoinsEarned,
                        s.mediumPlayTime + formatTime(monthSum.monthPlayTime)
                ))
                .withRequireUniqueKey(true)
                .withAmount(changeAmountIfGame(Game.GLOBAL, game))
                .withPosition(0)
                .build();
        global.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.GLOBAL, 0, SortBy.KILLS, month, year)));
        inventory.addItem(global);

        Item lobby = new Item.ItemBuilder(Material.WATCH)
                .withName(s.lobby)
                .withRequireUniqueKey(true)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.lobbyStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.mediumPlayTime + formatTime(monthSum.monthLobbyPlayTime)
                ))
                .withAmount(changeAmountIfGame(Game.LOBBY, game))
                .withPosition(1)
                .build();
        lobby.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.LOBBY, 0, SortBy.PLAY_TIME, month, year)));
        inventory.addItem(lobby);

        Item killSkill = new Item.ItemBuilder(Material.DIAMOND_CHESTPLATE)
                .withName(s.killSkill)
                .withRequireUniqueKey(true)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.killSkillStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.kills + monthSum.monthKillSkillKills,
                        s.deaths + monthSum.monthKillSkillDeaths,
                        s.experienceLooted + monthSum.monthKillSkillExperienceLooted,
                        s.lapisLooted + monthSum.monthKillSkillLapisLooted,
                        s.potionLooted + monthSum.monthKillSkillPotionLooted,
                        s.ironLooted + monthSum.monthKillSkillIronLooted,
                        s.diamondLooted + monthSum.monthKillSkillDiamondLooted,
                        s.blazeLooted + monthSum.monthKillSkillBlazeLooted,
                        s.headLooted + monthSum.monthKillSkillHeadLooted,
                        s.grenadeLooted + monthSum.monthKillSkillGrenadeLooted,
                        s.tntLooted + monthSum.monthKillSkillTntLooted,
                        s.cannonLooted + monthSum.monthKillSkillCannonLooted,
                        s.supplyOpened + monthSum.monthKillSkillSupplyOpened,
                        s.coinsEarned + monthSum.monthKillSkillCoinsEarned,
                        s.playTime + formatTime(monthSum.monthKillSkillPlayTime)
                ))
                .withAmount(changeAmountIfGame(Game.KILL_SKILL, game))
                .withPosition(3)
                .build();
        killSkill.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.KILL_SKILL, 0, SortBy.KILLS, month, year)));
        inventory.addItem(killSkill);

        Item survivalRush = new Item.ItemBuilder(Material.BED)
                .withName(s.survivalRush)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.survivalRushStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.kills + monthSum.monthSurvivalRushKills,
                        s.deaths + monthSum.monthSurvivalRushDeaths,
                        s.skeletonKilled + monthSum.monthSurvivalRushSkeletonsKilled,
                        s.blocksPlaced + monthSum.monthSurvivalRushBlocksPlaced,
                        s.ironMined + monthSum.monthSurvivalRushIronMined,
                        s.diamondMined + monthSum.monthSurvivalRushDiamondMined,
                        s.goldMined + monthSum.monthSurvivalRushGoldMined,
                        s.gravelMined + monthSum.monthSurvivalRushGravelMined,
                        s.coinsEarned + monthSum.monthSurvivalRushCoinsEarned,
                        s.mediumPlayTime + formatTime(monthSum.monthSurvivalRushPlayTime)
                ))
                .withRequireUniqueKey(true)
                .withAmount(changeAmountIfGame(Game.SURVIVAL_RUSH, game))
                .withPosition(4)
                .build();
        survivalRush.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.SURVIVAL_RUSH, 0, SortBy.KILLS, month, year)));
        inventory.addItem(survivalRush);

        Item rage = new Item.ItemBuilder(Material.IRON_SWORD)
                .withName(s.rage)
                .withHideFlags(true)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.rageStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.kills+monthSum.monthRageKills,
                        s.deaths+monthSum.monthRageDeaths,
                        s.rageBestKillStreak+monthSum.monthRageBestKillStreak,
                        s.rageTiers1Gagdets+monthSum.monthRageTier1Gadgets,
                        s.rageTiers2Gagdets+monthSum.monthRageTier2Gadgets,
                        s.rageTiers3Gagdets+monthSum.monthRageTier3Gadgets,
                        s.rageDoubleJumps+monthSum.monthRageDoubleJumps,
                        s.wins+monthSum.monthRageWins,
                        s.mediumPlayTime+formatTime(monthSum.monthRagePlayTime)
                ))
                .withPosition(5)
                .withAmount(changeAmountIfGame(Game.RAGE, game))
                .build();
        rage.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.RAGE, 0, SortBy.KILLS, month, year)));
        inventory.addItem(rage);

        Item evolution = new Item.ItemBuilder(Material.NETHER_STAR)
                .withName(s.evolution)
                .withRequireUniqueKey(true)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.evolutionStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.kills + monthSum.monthEvolutionKills,
                        s.deaths + monthSum.monthEvolutionDeaths,
                        s.starsEarned + monthSum.monthEvolutionStarsEarned,
                        s.coinsEarned + monthSum.monthEvolutionCoinsEarned,
                        s.mediumPlayTime + formatTime(monthSum.monthEvolutionPlayTime)
                ))
                .withAmount(changeAmountIfGame(Game.EVOLUTION, game))
                .withPosition(6)
                .build();
        evolution.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.EVOLUTION, 0, SortBy.KILLS, month, year)));
        inventory.addItem(evolution);

        Item ctf = new Item.ItemBuilder(Material.BANNER)
                .withName(s.ctf)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.ctfStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.kills + monthSum.monthCtfKills,
                        s.deaths + monthSum.monthCtfDeaths,
                        s.flagsTaken + monthSum.monthCtfFlagsTaken,
                        s.flagsSaved + monthSum.monthCtfFlagsSaved,
                        s.mediumPlayTime + formatTime(monthSum.monthCtfPlayTime)
                ))
                .withRequireUniqueKey(true)
                .withFlagColor(DyeColor.BLUE)
                .withHideFlags(true)
                .withAmount(changeAmountIfGame(Game.CTF, game))
                .withPosition(7)
                .build();
        ctf.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.CTF, 0, SortBy.KILLS, month, year)));
        inventory.addItem(ctf);


        Item tdm = new Item.ItemBuilder(Material.IRON_CHESTPLATE)
                .withName(s.tdm)
                .withHideFlags(true)
                .withLore(Lists.newArrayList(
                        s.monthSumItemHeader
                                .replace("%game%", s.tdmStripped)
                                .replace("%month%", monthString)
                                .replace("%year%", String.valueOf(year)),
                        s.kills+monthSum.monthTdmKills,
                        s.deaths+monthSum.monthTdmDeaths,
                        s.wins+monthSum.monthTdmWins,
                        s.mediumPlayTime+formatTime(monthSum.monthTdmPlayTime)
                ))
                .withPosition(8)
                .withAmount(changeAmountIfGame(Game.TDM, game))
                .build();
        tdm.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.TDM, 0, SortBy.KILLS, month, year)));
        inventory.addItem(tdm);
    }
}
