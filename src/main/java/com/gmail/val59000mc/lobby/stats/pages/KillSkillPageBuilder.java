package com.gmail.val59000mc.lobby.stats.pages;

import java.util.ArrayList;
import java.util.List;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.spigotutils.Time;

public class KillSkillPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();
		
		content.add(s.title);
		content.add(s.killSkill);
		
		content.add(" ");
		
		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthKillSkillKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthKillSkillDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthKillSkillCoinsEarned);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthKillSkillPlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalKillSkillKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalKillSkillDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalKillSkillCoinsEarned);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalKillSkillPlayTime));
		return content;
	}

}
