package com.gmail.val59000mc.lobby.stats.pages;

import java.util.ArrayList;
import java.util.List;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.spigotutils.Time;

public class EvolutionPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();
		
		content.add(s.title);
		content.add(s.evolution);
		
		content.add(" ");
		
		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthEvolutionKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthEvolutionDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthEvolutionCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.monthEvolutionWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthEvolutionPlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalEvolutionKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalEvolutionDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalEvolutionCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.globalEvolutionWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalEvolutionPlayTime));
		return content;
	}

}
