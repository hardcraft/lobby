package com.gmail.val59000mc.lobby.stats.inventories;

import org.bukkit.Material;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public class MonthRankingInventory extends AbstractRankingMonthStatsInventory{

	public MonthRankingInventory(InventoryStats stats, Game game, Integer page, SortBy sortBy, Integer month, Integer year) {
		super(
			stats, 
			game,
			page,
			sortBy,
			month,
			year
		);
		this.type = InventoryType.RANKING_MONTH;
	}

	
	@Override
	public Object[] getArgs(){
		return new Object[]{ game, page, sortBy, month, year };
	}

	@Override
	public void loadStats() {
		this.lobbyPlayers = stats.loadMonthRankingNames(game.getMonthTable(), sortBy, page, month, year);
		this.monthSum = stats.loadSumMonthStats(month, year);
	}
	
	@Override
	public Inventory buildInventory() {
		InventoryStrings s = stats.getStrings();
		
		this.inventory = new Inventory(
			getName(), 
			s.monthRankingInventory
				.replace("%sortBy%", sortBy.getColumnDescription())
				.replace("%game%", game.getShortName())
				.replace("%month%", String.valueOf(month))
				.replace("%year%", String.valueOf(year)),
			6
		);
		
		// game selector
		addGameSelectorItems();
		
		// previous players
		int previousPlayersPos = 19;
		if(page == 0){
			Item previousPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(s.noPreviousPlayers)
					.withLore(Lists.newArrayList(s.noPreviousPlayersLore))
					.withPlayerSkullTexture(Constants.LEFT_GRAYED_ARROW)
					.withRequireUniqueKey(true)
					.withPosition(previousPlayersPos)
					.build();
			inventory.addItem(previousPlayers);
		}else{
			Item previousPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullTexture(Constants.LEFT_ARROW)
					.withName(s.previousPlayers)
					.withLore(Lists.newArrayList(s.previousPlayersLore))
					.withRequireUniqueKey(true)
					.withPosition(previousPlayersPos)
					.build();
			previousPlayers.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, game, page-1, sortBy, month, year)));
			inventory.addItem(previousPlayers);
		}

		
		// next players
		int nextPlayersPos = 25;
		if(lobbyPlayers.size() < 5 || page == 11 || (lobbyPlayers.size() == 5 && lobbyPlayers.get(4) == null)){
			Item nextPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(s.noNextPlayers)
					.withLore(Lists.newArrayList(s.noNextPlayersLore))
					.withPlayerSkullTexture(Constants.RIGHT_GRAYED_ARROW)
					.withRequireUniqueKey(true)
					.withPosition(nextPlayersPos)
					.build();
			inventory.addItem(nextPlayers);
		}else{
			Item nextPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullTexture(Constants.RIGHT_ARROW)
					.withName(s.nextPlayers)
					.withLore(Lists.newArrayList(s.nextPlayersLore))
					.withRequireUniqueKey(true)
					.withPosition(nextPlayersPos)
					.build();
			nextPlayers.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, game, page+1, sortBy, month, year)));
			inventory.addItem(nextPlayers);
		}
		
		// players
		int playerPos = 20;
		int index = 0;
		for(LobbyPlayer lobbyPlayer : lobbyPlayers){
			int rank = page*5+index+1;
			Item player = buildPlayerSkull(s, lobbyPlayer, playerPos, index, rank);
			inventory.addItem(player);
			index++;
		}
	
		// month selector
		
		int previousMonthPos = 30;
		if(stats.isMinMonth(month, year)){
			Item noPreviousMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(s.noPreviousMonth)
					.withPlayerSkullTexture(Constants.LEFT_GRAYED_ARROW)
					.withLore(Lists.newArrayList(s.noPreviousMonthLore))
					.withRequireUniqueKey(true)
					.withPosition(previousMonthPos)
					.build();				
			inventory.addItem(noPreviousMonth);

		}else{
			Integer[] previousMonthInts = stats.getPreviousMonth(month, year);
			String previousMonthString = stats.monthIntToString(previousMonthInts[0]);
			Item previousMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
				.withDamage((short) 3)
				.withName(s.previousMonth)
				.withPlayerSkullTexture(Constants.LEFT_ARROW)
				.withLore(Lists.newArrayList(s.previousMonthLore.replace("%month%", previousMonthString).replace("%year%", String.valueOf(previousMonthInts[1]))))
				.withRequireUniqueKey(true)
				.withPosition(previousMonthPos)
				.build();
			previousMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, game, 0, sortBy, previousMonthInts[0], previousMonthInts[1])));
			inventory.addItem(previousMonth);
		}
		
		int nextMonthPos = 32;
		if(stats.isMaxMonth(month, year)){
			Item noNextMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(s.noNextMonth)
					.withPlayerSkullTexture(Constants.RIGHT_GRAYED_ARROW)
					.withLore(Lists.newArrayList(s.noNextMonthLore))
					.withRequireUniqueKey(true)
					.withPosition(nextMonthPos)
					.build();				
			inventory.addItem(noNextMonth);
		}else{
			Integer[] nextMonthInts = stats.getNextMonth(month, year);
			String nextMonthString = stats.monthIntToString(nextMonthInts[0]);
			Item nextMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(s.nextMonth)
					.withPlayerSkullTexture(Constants.RIGHT_ARROW)
					.withLore(Lists.newArrayList(s.nextMonthLore.replace("%month%", nextMonthString).replace("%year%", String.valueOf(nextMonthInts[1]))))
					.withRequireUniqueKey(true)
					.withPosition(nextMonthPos)
					.build();
			nextMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, game, 0, sortBy, nextMonthInts[0], nextMonthInts[1])));
			inventory.addItem(nextMonth);
		}
		

		Item currentMonth = new Item.ItemBuilder(Material.GLOWSTONE_DUST)
				.withName(s.monthSelector)
				.withLore(Lists.newArrayList(s.monthSelectorLore))
				.withRequireUniqueKey(true)
				.withPosition(31)
				.build();		
		currentMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, game, 0, sortBy)));		
		inventory.addItem(currentMonth);
		
		
		// filters
		game.addMonthFiltersItems(s,this);
		
		// glas panes
		addGlassPanes(ImmutableMap.<Integer, Short>builder()
			.put(2,Constants.GREEN_PANE)
			.put(9,Constants.GREEN_PANE)
			.put(10,Constants.WHITE_PANE)
			.put(11,Constants.WHITE_PANE)
			.put(12,Constants.WHITE_PANE)
			.put(13,Constants.WHITE_PANE)
			.put(14,Constants.WHITE_PANE)
			.put(15,Constants.WHITE_PANE)
			.put(16,Constants.WHITE_PANE)
			.put(17,Constants.GREEN_PANE)
			.put(18,Constants.WHITE_PANE)
			.put(26,Constants.WHITE_PANE)
			.put(27,Constants.GREEN_PANE)
			.put(28,Constants.WHITE_PANE)
			.put(29,Constants.WHITE_PANE)
			.put(33,Constants.WHITE_PANE)
			.put(34,Constants.WHITE_PANE)
			.put(35,Constants.GREEN_PANE)
			.put(36,Constants.GREEN_PANE)
			.put(37,Constants.GREEN_PANE)
			.put(38,Constants.GREEN_PANE)
			.put(39,Constants.WHITE_PANE)
			.put(40,Constants.WHITE_PANE)
			.put(41,Constants.WHITE_PANE)
			.put(42,Constants.GREEN_PANE)
			.put(43,Constants.GREEN_PANE)
			.put(44,Constants.GREEN_PANE)
			.put(45,Constants.WHITE_PANE)
			.put(52,Constants.WHITE_PANE)
			.build()
		);
		
		// back
		Item previous = new Item.ItemBuilder(Material.RAW_FISH)
				.withDamage((short) 3)
				.withName(s.back)
				.withLore(Lists.newArrayList(s.backToMain))
				.withPosition(53)
				.build();
		previous.setActions(Lists.newArrayList(new OpenInventoryAction(Constants.STATISTICS_MAIN_INVENTORY)));
		inventory.addItem(previous);
		
		return inventory;
	}
	
	private Item buildPlayerSkull(InventoryStrings s, LobbyPlayer lobbyPlayer, int playerPos, int index, int rank) {
		if(lobbyPlayer == null){
			return new Item.ItemBuilder(Material.SKULL_ITEM)
				.withRequireUniqueKey(true)
				.withPosition(playerPos+index)
				.withName(s.noRankedPlayer)
				.withAmount(rank)
				.withLore(Lists.newArrayList(s.noRankedPlayerLore
						.replace("%rank%", String.valueOf(rank))
						.replace("%game%", game.getName())
				))
				.build();
		}else{
			Item player = new  Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullName(lobbyPlayer.getName())
					.withRequireUniqueKey(true)
					.withPosition(playerPos+index)
					.withName("§a"+lobbyPlayer.getName())
					.withAmount(rank)
					.withLore(Lists.newArrayList(s.monthRankingItemHeader
							.replace("%rank%", String.valueOf(rank))
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", game.getName())
							.replace("%month%", monthString)
							.replace("%year%", String.valueOf(year)),
							game.getRankingMonthItemLore(s,lobbyPlayer, sortBy)
					))
					.build();
			player.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), InventoryType.MONTH, lobbyPlayer.getName(), lobbyPlayer.getId(), month, year)));
			return player;
		}
		
	}

}
