package com.gmail.val59000mc.lobby.stats.inventories.ranking;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.GlobalRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.MonthRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.google.common.collect.Lists;

import java.util.List;

public class GladiatorBuilder extends RankingItemBuilder{

	@Override
	public List<String> buildGlobalLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.globalGladiatorKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.globalGladiatorDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.globalGladiatorKills, lobbyPlayer.globalGladiatorDeaths)),
			s.gladiatorsKilled+lobbyPlayer.globalGladiatorGladiatorsKilled,
			s.slavesKilled+lobbyPlayer.globalGladiatorSlavesKilled,
			s.gladiatorWins+lobbyPlayer.globalGladiatorGladiatorWins,
			s.slaveWins+lobbyPlayer.globalGladiatorSlaveWins,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.globalGladiatorCoinsEarned),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.globalGladiatorPlayTime))
		);
	}

	@Override
	public List<String> buildMonthLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.monthGladiatorKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.monthGladiatorDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.monthGladiatorKills, lobbyPlayer.monthGladiatorDeaths)),
			s.gladiatorsKilled+lobbyPlayer.monthGladiatorGladiatorsKilled,
			s.slavesKilled+lobbyPlayer.monthGladiatorSlavesKilled,
			s.gladiatorWins+lobbyPlayer.monthGladiatorGladiatorWins,
			s.slaveWins+lobbyPlayer.monthGladiatorSlaveWins,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.monthGladiatorCoinsEarned),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.monthGladiatorPlayTime))
		);
	}

	@Override
	public void addGlobalFiltersItems(InventoryStrings s, GlobalRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.WINS_FILLTER_POSITION);
	}

	@Override
	public void addMonthFiltersItems(InventoryStrings s, MonthRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.WINS_FILLTER_POSITION);
	}
	
}
