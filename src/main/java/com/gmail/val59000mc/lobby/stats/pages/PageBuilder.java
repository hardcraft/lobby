package com.gmail.val59000mc.lobby.stats.pages;

import java.util.List;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

public abstract class PageBuilder {

	public abstract List<String> build(PageStrings s, LobbyPlayer lobbyPlayer);
	


	protected String formatTime(int time) {

		int d,h;
		d = (int) time / (60 * 24);
		time -= d * ( 60 * 24);
		h = (int) time / 60;
		time -= h * 60;
		
		
		if(d == 0){
			if(h==0){
				return time+"m";
			}else{
				return h+"h "+time+"m ";		
			}
		}else{
			return d+"j "+h+"h "+time+"m ";
		}
	}
}
