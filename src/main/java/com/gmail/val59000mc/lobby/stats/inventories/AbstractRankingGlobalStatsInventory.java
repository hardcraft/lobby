package com.gmail.val59000mc.lobby.stats.inventories;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.google.common.collect.Lists;
import org.bukkit.DyeColor;
import org.bukkit.Material;

import java.util.List;

public abstract class AbstractRankingGlobalStatsInventory extends AbstractCachedInventory{

	protected int page;
	protected SortBy sortBy;
	protected Game game;
	protected List<LobbyPlayer> lobbyPlayers;
	protected LobbyPlayer globalSum;
	
	
	
	public SortBy getSortBy() {
		return sortBy;
	}


	public Game getGame() {
		return game;
	}


	public AbstractRankingGlobalStatsInventory(InventoryStats stats, Game game, Integer page, SortBy sortBy){
		super(
			stats,
			"statistics-global-ranking-"+game.getName()+"-"+page+"-"+sortBy 
		);
		this.page = page;
		this.sortBy = sortBy;
		this.game = game;
	}
	
	protected void addGameSelectorItems() {

		InventoryStrings s = stats.getStrings();
		
		Item global = new Item.ItemBuilder(Material.SKULL_ITEM)
				.withDamage((short) 3)
				.withPlayerSkullName("{player}")
				.withName(s.all)
				.withLore(Lists.newArrayList(
					s.globalSumItemHeader.replace("%game%", s.allStripped),
					s.kills+globalSum.globalKills,
					s.deaths+globalSum.globalDeaths,
					s.coinsEarned+globalSum.globalCoinsEarned,
					s.mediumPlayTime+formatTime(globalSum.globalPlayTime)
				))
				.withRequireUniqueKey(true)
				.withPosition(0)
				.withAmount(changeAmountIfGame(Game.GLOBAL, game))
				.build();
		global.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.GLOBAL, 0, SortBy.KILLS)));
		inventory.addItem(global);
		
		Item lobby = new Item.ItemBuilder(Material.WATCH)
				.withName(s.lobby)
				.withRequireUniqueKey(true)
				.withLore(Lists.newArrayList(
					s.globalSumItemHeader.replace("%game%", s.lobbyStripped),
					s.mediumPlayTime+formatTime(globalSum.globalLobbyPlayTime)
				))
				.withPosition(1)
				.withAmount(changeAmountIfGame(Game.LOBBY, game))
				.build();
		lobby.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.LOBBY, 0, SortBy.PLAY_TIME)));
		inventory.addItem(lobby);
		
		Item killSkill = new Item.ItemBuilder(Material.DIAMOND_CHESTPLATE)
				.withName(s.killSkill)
				.withRequireUniqueKey(true)
				.withPosition(3)
				.withLore(Lists.newArrayList(
					s.globalSumItemHeader.replace("%game%", s.killSkillStripped),
					s.kills+globalSum.globalKillSkillKills,
					s.deaths+globalSum.globalKillSkillDeaths,
					s.experienceLooted+globalSum.globalKillSkillExperienceLooted,
					s.lapisLooted+globalSum.globalKillSkillLapisLooted,
					s.potionLooted+globalSum.globalKillSkillPotionLooted,
					s.ironLooted+globalSum.globalKillSkillIronLooted,
					s.diamondLooted+globalSum.globalKillSkillDiamondLooted,
					s.blazeLooted+globalSum.globalKillSkillBlazeLooted,
					s.headLooted+globalSum.globalKillSkillHeadLooted,
					s.grenadeLooted+globalSum.globalKillSkillGrenadeLooted,
					s.tntLooted+globalSum.globalKillSkillTntLooted,
					s.cannonLooted+globalSum.globalKillSkillCannonLooted,
					s.supplyOpened+globalSum.globalKillSkillSupplyOpened,
					s.coinsEarned+globalSum.globalKillSkillCoinsEarned,
					s.mediumPlayTime+formatTime(globalSum.globalKillSkillPlayTime)
				))
				.withAmount(changeAmountIfGame(Game.KILL_SKILL, game))
				.build();
		killSkill.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.KILL_SKILL, 0, SortBy.KILLS)));
		inventory.addItem(killSkill);
		
		Item survivalRush = new Item.ItemBuilder(Material.BED)
				.withName(s.survivalRush)
				.withRequireUniqueKey(true)
				.withPosition(4)
				.withLore(Lists.newArrayList(
					s.globalSumItemHeader.replace("%game%", s.survivalRushStripped),
					s.kills+globalSum.globalSurvivalRushKills,
					s.deaths+globalSum.globalSurvivalRushDeaths,
					s.skeletonKilled+globalSum.globalSurvivalRushSkeletonsKilled,
					s.blocksPlaced+globalSum.globalSurvivalRushBlocksPlaced,
					s.ironMined+globalSum.globalSurvivalRushIronMined,
					s.diamondMined+globalSum.globalSurvivalRushDiamondMined,
					s.goldMined+globalSum.globalSurvivalRushGoldMined,
					s.gravelMined+globalSum.globalSurvivalRushGravelMined,
					s.coinsEarned+globalSum.globalSurvivalRushCoinsEarned,
					s.mediumPlayTime+formatTime(globalSum.globalSurvivalRushPlayTime)
				))
				.withAmount(changeAmountIfGame(Game.SURVIVAL_RUSH, game))
				.build();
		survivalRush.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.SURVIVAL_RUSH, 0, SortBy.KILLS)));
		inventory.addItem(survivalRush);

		Item rage = new Item.ItemBuilder(Material.IRON_SWORD)
				.withName(s.rage)
				.withHideFlags(true)
				.withLore(Lists.newArrayList(
						s.globalSumItemHeader.replace("%game%", s.rageStripped),
						s.kills+globalSum.globalRageKills,
						s.deaths+globalSum.globalRageDeaths,
						s.rageBestKillStreak+globalSum.globalRageBestKillStreak,
						s.rageTiers1Gagdets+globalSum.globalRageTier1Gadgets,
						s.rageTiers2Gagdets+globalSum.globalRageTier2Gadgets,
						s.rageTiers3Gagdets+globalSum.globalRageTier3Gadgets,
						s.rageDoubleJumps+globalSum.globalRageDoubleJumps,
						s.wins+globalSum.globalRageWins,
						s.mediumPlayTime+formatTime(globalSum.globalRagePlayTime)
				))
				.withPosition(5)
				.withAmount(changeAmountIfGame(Game.RAGE, game))
				.build();
		rage.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.RAGE, 0, SortBy.KILLS)));
		inventory.addItem(rage);
		
		Item evolution = new Item.ItemBuilder(Material.NETHER_STAR)
				.withName(s.evolution)
				.withRequireUniqueKey(true)
				.withPosition(6)
				.withLore(Lists.newArrayList(
					s.globalSumItemHeader.replace("%game%", s.evolutionStripped),
					s.kills+globalSum.globalEvolutionKills,
					s.deaths+globalSum.globalEvolutionDeaths,
					s.starsEarned+globalSum.globalEvolutionStarsEarned,
					s.coinsEarned+globalSum.globalEvolutionCoinsEarned,
					s.mediumPlayTime+formatTime(globalSum.globalEvolutionPlayTime)
				))
				.withAmount(changeAmountIfGame(Game.EVOLUTION, game))
				.build();
		evolution.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.EVOLUTION, 0, SortBy.KILLS)));
		inventory.addItem(evolution);
		
		Item ctf = new Item.ItemBuilder(Material.BANNER)
				.withName(s.ctf)
				.withLore(Lists.newArrayList(
					s.globalSumItemHeader.replace("%game%", s.ctfStripped),
					s.kills+globalSum.globalCtfKills,
					s.deaths+globalSum.globalCtfDeaths,
					s.flagsTaken+globalSum.globalCtfFlagsTaken,
					s.flagsSaved+globalSum.globalCtfFlagsSaved,
					s.wins+globalSum.globalCtfWins,
					s.mediumPlayTime+formatTime(globalSum.globalCtfPlayTime)
				))
				.withRequireUniqueKey(true)
				.withFlagColor(DyeColor.BLUE)
				.withHideFlags(true)
				.withAmount(changeAmountIfGame(Game.CTF, game))
				.withPosition(7)
				.build();
		ctf.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.CTF, 0, SortBy.KILLS)));
		inventory.addItem(ctf);

		Item tdm = new Item.ItemBuilder(Material.IRON_CHESTPLATE)
				.withName(s.tdm)
				.withHideFlags(true)
				.withLore(Lists.newArrayList(
						s.globalSumItemHeader.replace("%game%", s.tdmStripped),
						s.kills+globalSum.globalTdmKills,
						s.deaths+globalSum.globalTdmDeaths,
						s.wins+globalSum.globalTdmWins,
						s.mediumPlayTime+formatTime(globalSum.globalTdmPlayTime)
				))
				.withPosition(8)
				.withAmount(changeAmountIfGame(Game.TDM, game))
				.build();
		tdm.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.TDM, 0, SortBy.KILLS)));
		inventory.addItem(tdm);
	}
}
