package com.gmail.val59000mc.lobby.stats.inventories;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.InventoryStats;

public abstract class AbstractPlayerStatsInventory extends AbstractCachedInventory{

	protected LobbyPlayer lobbyPlayer;
	protected String preLobbyPlayer;
	protected int preLobbyPlayerId;
	
	public AbstractPlayerStatsInventory(InventoryStats stats, String name, String preLobbyPlayer, int preLobbyPlayerId) {
		super(stats, name);
		this.preLobbyPlayer = preLobbyPlayer;
		this.preLobbyPlayerId = preLobbyPlayerId;
	}

}
