package com.gmail.val59000mc.lobby.stats.inventories;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.AnvilGUI;
import com.gmail.val59000mc.spigotutils.AnvilGUI.AnvilClickEvent;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;

public class OpenAnvilGUIAction extends Action{

	private InventoryType type;
	private Object[] args;
	private InventoryStats stats;
	
	public OpenAnvilGUIAction(InventoryStats stats, InventoryType type, Object... args) {
		super();
		this.stats = stats;
		this.type = type;
		this.args = args;
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		
        AnvilGUI gui = new AnvilGUI(player, new AnvilGUI.AnvilClickEventHandler() {
               
            @Override
            public void onAnvilClick(AnvilClickEvent event) {
                    if(event.getSlot() == AnvilGUI.AnvilSlot.OUTPUT){
                            event.setWillClose(true);
                            event.setWillDestroy(true);
                            
                            String targetPlayerName = event.getName().length() > 16 ? event.getName().substring(0, 16) : event.getName();
                            
                            findPlayerAndOpenInventory(player, sigPlayer, targetPlayerName);
                    }else{
                            event.setWillClose(false);
                            event.setWillDestroy(false);
                    }
            }
            
        }, stats.getApi().getPlugin());
       
        ItemStack skull = new ItemBuilder(Material.BOOK_AND_QUILL)
        		.buildMeta()
        		.withDisplayName(stats.getStrings().chooseNickname)
        		.withLore(stats.getStrings().chooseNicknameLore.split("<br>"))
        		.item()
        		.build();
       
        gui.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT, skull);
        executeNextAction(player, sigPlayer,true);
        
        Bukkit.getScheduler().runTaskLater(stats.getApi().getPlugin(), new Runnable() {
			
			@Override
			public void run() {
		        gui.open();
		        
			}
		}, 1);
       
		
	}
	
	protected void findPlayerAndOpenInventory(Player player, SigPlayer sigPlayer, String targetPlayerName) {
		
		stats.getApi().async(()->stats.getApi().getMySQLAPI().selectPlayerByName(targetPlayerName), (response) -> {

			Object[] invArgs = new Object[2+args.length];
		  	invArgs[0] = targetPlayerName;
	        invArgs[1] = 0;
	        
			try {
				if(response.first()){
					invArgs[1] = response.getInt("id");
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			stats.getApi().sync(()->{
				
			        for(int i=0 ; i<args.length ; i++){
			        	invArgs[1+i] = args[i];
			        }
			        AbstractCachedInventory inv = stats.addOrGetFromCache(type, invArgs);
			        
			        
			        if(inv.isLoaded()){
			        	 Bukkit.getScheduler().runTaskLater(stats.getApi().getPlugin(), new Runnable() {
			     			
			     			@Override
			     			public void run() {
			        			openInventory(inv, player, sigPlayer);
			     			}
			     		}, 1);
					}else{
						stats.getApi().buildTask("wait loading '"+inv.getName()+"' for "+player.getName(), new HCTask() {
							
							@Override
							public void run() {
								if(inv.isLoaded()){
									openInventory(inv, player, sigPlayer);
									scheduler.stop();
								}
							}
						})
						.withInterval(5)
						.withDelay(5)
						.build()
						.start();
					}
			        
			});
		});
      
		
	}

	private void openInventory(AbstractCachedInventory inv, Player player, SigPlayer sigPlayer){
		inv.getInventory().openInventory(player, sigPlayer);
		inv.updateAccessDate();
	}
	
}
