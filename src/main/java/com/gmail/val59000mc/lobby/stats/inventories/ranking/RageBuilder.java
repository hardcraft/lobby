package com.gmail.val59000mc.lobby.stats.inventories.ranking;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.GlobalRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.MonthRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.google.common.collect.Lists;

import java.util.List;

public class RageBuilder extends RankingItemBuilder{

	@Override
	public List<String> buildGlobalLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.globalRageKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.globalRageDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.globalRageKills, lobbyPlayer.globalRageDeaths)),
			s.rageBestKillStreak+lobbyPlayer.globalRageBestKillStreak,
			s.rageTiers1Gagdets+lobbyPlayer.globalRageTier1Gadgets,
			s.rageTiers2Gagdets+lobbyPlayer.globalRageTier2Gadgets,
			s.rageTiers3Gagdets+lobbyPlayer.globalRageTier3Gadgets,
			s.rageDoubleJumps+lobbyPlayer.globalRageDoubleJumps,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.globalRageCoinsEarned),
			boldIfSortedBy(SortBy.WINS, sortBy, s.wins+lobbyPlayer.globalRageWins),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.globalRagePlayTime))
		);
	}

	@Override
	public List<String> buildMonthLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
                boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.monthRageKills),
                boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.monthRageDeaths),
                boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.monthRageKills, lobbyPlayer.monthRageDeaths)),
                s.rageBestKillStreak+lobbyPlayer.monthRageBestKillStreak,
                s.rageTiers1Gagdets+lobbyPlayer.monthRageTier1Gadgets,
                s.rageTiers2Gagdets+lobbyPlayer.monthRageTier2Gadgets,
                s.rageTiers3Gagdets+lobbyPlayer.monthRageTier3Gadgets,
                s.rageDoubleJumps+lobbyPlayer.monthRageDoubleJumps,
                boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.monthRageCoinsEarned),
                boldIfSortedBy(SortBy.WINS, sortBy, s.wins+lobbyPlayer.monthRageWins),
                boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.monthRagePlayTime))
		);
	}


	@Override
	public void addGlobalFiltersItems(InventoryStrings s, GlobalRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addWinsFilter(s, inventory, Constants.WINS_FILLTER_POSITION);
	}

	@Override
	public void addMonthFiltersItems(InventoryStrings s, MonthRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addWinsFilter(s, inventory, Constants.WINS_FILLTER_POSITION);
	}
	
}
