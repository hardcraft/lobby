package com.gmail.val59000mc.lobby.stats.inventories;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.bukkit.DyeColor;
import org.bukkit.Material;

import java.time.LocalDateTime;

public class MonthInventory extends AbstractPlayerStatsInventory{

	private int month;
	private String monthString;
	private int year;
	
	public MonthInventory(InventoryStats stats, String preLobbyPlayer, Integer preLobbyPlayerId, Integer month, Integer year) {
		super(
			stats, 
			buildName(preLobbyPlayer, month, year),
			preLobbyPlayer,
			preLobbyPlayerId
		);
		initDates(month, year);
		this.type = InventoryType.MONTH;
	}
	
	@Override
	public Object[] getArgs(){
		return new Object[]{ preLobbyPlayer, month, year };
	}
	
	private void initDates(int aMonth, int aYear) {
		if(aMonth==0 && aYear == 0){
			LocalDateTime now = LocalDateTime.now();
			this.month = now.getMonth().getValue();
			this.year = now.getYear();
		}else{
			this.month = aMonth;
			this.year = aYear;
		}
		
		this.monthString = stats.monthIntToString(month);
	}


	private static String buildName(String preLobbyPlayer, int aMonth, int aYear) {
		String name = "statistics-month-player-"+preLobbyPlayer+"-";
		if(aMonth==0 && aYear == 0){
			LocalDateTime now = LocalDateTime.now();
			int currentMonth = now.getMonth().getValue();
			int currentYear = now.getYear();
			name += currentMonth+"-"+currentYear;
		}else{
			name += aMonth+"-"+aYear;
		}
		return name;
	}


	@Override
	public void loadStats() {
		this.lobbyPlayer = stats.loadMonthStats(preLobbyPlayer, preLobbyPlayerId, month, year);
	}
	
	@Override
	public Inventory buildInventory() {
		InventoryStrings strings = stats.getStrings();
		
		this.inventory = new Inventory(getName(), strings.monthInventory
				.replace("%player%", preLobbyPlayer)
				.replace("%month%", String.valueOf(month))
				.replace("%year%", String.valueOf(year))
			, 3);
		
		if(lobbyPlayer == null){
			Item notFound = new Item.ItemBuilder(Material.BARRIER)
				.withName(strings.playerNotFoundName)
				.withLore(Lists.newArrayList(strings.playerNotFoundLore.replace("%player%", preLobbyPlayer)))
				.withPosition(13)
				.build();
			inventory.addItem(notFound);
			
			// glass panes
			addGlassPanes(ImmutableMap.<Integer, Short>builder()
				.put(0, Constants.WHITE_PANE)
				.put(1,Constants.GREEN_PANE)
				.put(2,Constants.WHITE_PANE)
				.put(3,Constants.GREEN_PANE)
				.put(4,Constants.WHITE_PANE)
				.put(5,Constants.GREEN_PANE)
				.put(6,Constants.WHITE_PANE)
				.put(7,Constants.GREEN_PANE)
				.put(8,Constants.WHITE_PANE)
				.put(9,Constants.GREEN_PANE)
				.put(10,Constants.WHITE_PANE)
				.put(11,Constants.GREEN_PANE)
				.put(12,Constants.WHITE_PANE)
				.put(14,Constants.WHITE_PANE)
				.put(15,Constants.GREEN_PANE)
				.put(16,Constants.WHITE_PANE)
				.put(17,Constants.GREEN_PANE)
				.put(18, Constants.WHITE_PANE)
				.put(19,Constants.GREEN_PANE)
				.put(20,Constants.WHITE_PANE)
				.put(21,Constants.GREEN_PANE)
				.put(22,Constants.WHITE_PANE)
				.put(23,Constants.GREEN_PANE)
				.put(24,Constants.WHITE_PANE)
				.put(25,Constants.GREEN_PANE)
				.build()
			);
			
		}else{
			
			Item global = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullName(lobbyPlayer.getName())
					.withName(strings.all)
					.withLore(Lists.newArrayList(
						strings.monthItemHeader
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", strings.allStripped)
							.replace("%month%", monthString)
							.replace("%year%", String.valueOf(year)),
						strings.kills+lobbyPlayer.monthKills,
						strings.deaths+lobbyPlayer.monthDeaths,
						strings.ratio+kdRatio(lobbyPlayer.monthKills, lobbyPlayer.monthDeaths),
						strings.coinsEarned+lobbyPlayer.monthCoinsEarned,
						strings.wins+lobbyPlayer.monthWins,
						strings.playTime+formatTime(lobbyPlayer.monthPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(3)
					.build();
			global.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.GLOBAL, 0, SortBy.KILLS, month, year)));
			inventory.addItem(global);
			
			Item lobby = new Item.ItemBuilder(Material.WATCH)
					.withName(strings.lobby)
					.withLore(Lists.newArrayList(
						strings.monthItemHeader
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", strings.lobbyStripped)
							.replace("%month%", monthString)
							.replace("%year%", String.valueOf(year)),
						strings.playTime+formatTime(lobbyPlayer.monthLobbyPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(5)
					.build();
			lobby.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.LOBBY, 0, SortBy.PLAY_TIME, month, year)));
			inventory.addItem(lobby);
			
			Item killskill = new Item.ItemBuilder(Material.DIAMOND_CHESTPLATE)
					.withName(strings.killSkill)
					.withLore(Lists.newArrayList(
						strings.monthItemHeader
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", strings.killSkillStripped)
							.replace("%month%", monthString)
							.replace("%year%", String.valueOf(year)),
						strings.kills+lobbyPlayer.monthKillSkillKills,
						strings.deaths+lobbyPlayer.monthKillSkillDeaths,
						strings.ratio+kdRatio(lobbyPlayer.monthKillSkillKills, lobbyPlayer.monthKillSkillDeaths),
						strings.experienceLooted+lobbyPlayer.monthKillSkillExperienceLooted,
						strings.lapisLooted+lobbyPlayer.monthKillSkillLapisLooted,
						strings.potionLooted+lobbyPlayer.monthKillSkillPotionLooted,
						strings.ironLooted+lobbyPlayer.monthKillSkillIronLooted,
						strings.diamondLooted+lobbyPlayer.monthKillSkillDiamondLooted,
						strings.blazeLooted+lobbyPlayer.monthKillSkillBlazeLooted,
						strings.headLooted+lobbyPlayer.monthKillSkillHeadLooted,
						strings.grenadeLooted+lobbyPlayer.monthKillSkillGrenadeLooted,
						strings.tntLooted+lobbyPlayer.monthKillSkillTntLooted,
						strings.cannonLooted+lobbyPlayer.monthKillSkillCannonLooted,
						strings.supplyOpened+lobbyPlayer.monthKillSkillSupplyOpened,
						strings.coinsEarned+lobbyPlayer.monthKillSkillCoinsEarned,
						strings.playTime+formatTime(lobbyPlayer.monthKillSkillPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(10)
					.build();
			killskill.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.KILL_SKILL, 0, SortBy.KILLS, month, year)));
			inventory.addItem(killskill);
			
			Item survivalRush = new Item.ItemBuilder(Material.BED)
					.withName(strings.survivalRush)
					.withLore(Lists.newArrayList(
						strings.monthItemHeader
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", strings.survivalRushStripped)
							.replace("%month%", monthString)
							.replace("%year%", String.valueOf(year)),
						strings.kills+lobbyPlayer.monthSurvivalRushKills,
						strings.deaths+lobbyPlayer.monthSurvivalRushDeaths,
						strings.ratio+kdRatio(lobbyPlayer.monthSurvivalRushKills, lobbyPlayer.monthSurvivalRushDeaths),
						strings.skeletonKilled+lobbyPlayer.monthSurvivalRushSkeletonsKilled,
						strings.blocksPlaced+lobbyPlayer.monthSurvivalRushBlocksPlaced,
						strings.ironMined+lobbyPlayer.monthSurvivalRushIronMined,
						strings.diamondMined+lobbyPlayer.monthSurvivalRushDiamondMined,
						strings.goldMined+lobbyPlayer.monthSurvivalRushGoldMined,
						strings.gravelMined+lobbyPlayer.monthSurvivalRushGravelMined,
						strings.coinsEarned+lobbyPlayer.monthSurvivalRushCoinsEarned,
						strings.wins+lobbyPlayer.monthSurvivalRushWins,
						strings.playTime+formatTime(lobbyPlayer.monthSurvivalRushPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(11)
					.build();
			survivalRush.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.SURVIVAL_RUSH, 0, SortBy.KILLS, month, year)));
			inventory.addItem(survivalRush);

            Item rage = new Item.ItemBuilder(Material.IRON_SWORD)
                    .withName(strings.rage)
                    .withLore(Lists.newArrayList(
                            strings.monthItemHeader
                                    .replace("%player%", lobbyPlayer.getName())
                                    .replace("%game%", strings.rageStripped)
                                    .replace("%month%", monthString)
                                    .replace("%year%", String.valueOf(year)),
                            strings.kills+lobbyPlayer.monthRageKills,
                            strings.deaths+lobbyPlayer.monthRageDeaths,
                            strings.ratio+kdRatio(lobbyPlayer.monthRageKills, lobbyPlayer.monthRageDeaths),
                            strings.rageBestKillStreak+lobbyPlayer.monthRageBestKillStreak,
                            strings.rageTiers1Gagdets+lobbyPlayer.monthRageTier1Gadgets,
                            strings.rageTiers2Gagdets+lobbyPlayer.monthRageTier2Gadgets,
                            strings.rageTiers3Gagdets+lobbyPlayer.monthRageTier3Gadgets,
                            strings.rageDoubleJumps+lobbyPlayer.monthRageDoubleJumps,
                            strings.coinsEarned+lobbyPlayer.monthRageCoinsEarned,
                            strings.wins+lobbyPlayer.monthRageWins,
                            strings.playTime+formatTime(lobbyPlayer.monthRagePlayTime)
                    ))
                    .withRequireUniqueKey(true)
                    .withPosition(12)
                    .build();
            rage.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.RAGE, 0, SortBy.KILLS, month, year)));
            inventory.addItem(rage);
			
			Item evolution = new Item.ItemBuilder(Material.NETHER_STAR)
					.withName(strings.evolution)
					.withLore(Lists.newArrayList(
						strings.monthItemHeader
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", strings.evolutionStripped)
							.replace("%month%", monthString)
							.replace("%year%", String.valueOf(year)),
						strings.kills+lobbyPlayer.monthEvolutionKills,
						strings.deaths+lobbyPlayer.monthEvolutionDeaths,
						strings.ratio+kdRatio(lobbyPlayer.monthEvolutionKills, lobbyPlayer.monthEvolutionDeaths),
						strings.starsEarned+lobbyPlayer.monthEvolutionStarsEarned,
						strings.coinsEarned+lobbyPlayer.monthEvolutionCoinsEarned,
						strings.wins+lobbyPlayer.monthEvolutionWins,
						strings.playTime+formatTime(lobbyPlayer.monthEvolutionPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(14)
					.build();
			evolution.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.EVOLUTION, 0, SortBy.KILLS, month, year)));
			inventory.addItem(evolution);
			
			Item ctf = new Item.ItemBuilder(Material.BANNER)
					.withName(strings.ctf)
					.withFlagColor(DyeColor.BLUE)
					.withHideFlags(true)
					.withLore(Lists.newArrayList(
						strings.monthItemHeader
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", strings.ctfStripped)
							.replace("%month%", monthString)
							.replace("%year%", String.valueOf(year)),
						strings.kills+lobbyPlayer.monthCtfKills,
						strings.deaths+lobbyPlayer.monthCtfDeaths,
						strings.ratio+kdRatio(lobbyPlayer.monthCtfKills, lobbyPlayer.monthCtfDeaths),
						strings.flagsTaken+lobbyPlayer.monthCtfFlagsTaken,
						strings.flagsSaved+lobbyPlayer.monthCtfFlagsSaved,
						strings.coinsEarned+lobbyPlayer.monthCtfCoinsEarned,
						strings.wins+lobbyPlayer.monthCtfWins,
						strings.playTime+formatTime(lobbyPlayer.monthCtfPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(15)
					.build();
			ctf.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.CTF, 0, SortBy.KILLS, month, year)));
			inventory.addItem(ctf);

            Item tdm = new Item.ItemBuilder(Material.IRON_CHESTPLATE)
                    .withName(strings.tdm)
                    .withHideFlags(true)
                    .withLore(Lists.newArrayList(
                            strings.monthItemHeader
                                    .replace("%player%", lobbyPlayer.getName())
                                    .replace("%game%", strings.tdmStripped)
                                    .replace("%month%", monthString)
                                    .replace("%year%", String.valueOf(year)),
                            strings.kills+lobbyPlayer.monthTdmKills,
                            strings.deaths+lobbyPlayer.monthTdmDeaths,
                            strings.ratio+kdRatio(lobbyPlayer.monthTdmKills, lobbyPlayer.monthTdmDeaths),
                            strings.coinsEarned+lobbyPlayer.monthTdmCoinsEarned,
                            strings.wins+lobbyPlayer.monthTdmWins,
                            strings.playTime+formatTime(lobbyPlayer.monthTdmPlayTime)
                    ))
                    .withRequireUniqueKey(true)
                    .withPosition(16)
                    .build();
            tdm.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, Game.TDM, 0, SortBy.KILLS, month, year)));
            inventory.addItem(tdm);
			
			int previousMonthPos = 21;
			if(stats.isMinMonth(month, year)){
				Item noPreviousMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
						.withDamage((short) 3)
						.withName(strings.noPreviousMonth)
						.withPlayerSkullTexture(Constants.LEFT_GRAYED_ARROW)
						.withLore(Lists.newArrayList(strings.noPreviousMonthLore))
						.withRequireUniqueKey(true)
						.withPosition(previousMonthPos)
						.build();				
				inventory.addItem(noPreviousMonth);

			}else{
				Integer[] previousMonthInts = stats.getPreviousMonth(month, year);
				String previousMonthString = stats.monthIntToString(previousMonthInts[0]);
				Item previousMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(strings.previousMonth)
					.withPlayerSkullTexture(Constants.LEFT_ARROW)
					.withLore(Lists.newArrayList(strings.previousMonthLore.replace("%month%", previousMonthString).replace("%year%", String.valueOf(previousMonthInts[1]))))
					.withRequireUniqueKey(true)
					.withPosition(previousMonthPos)
					.build();
				previousMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.MONTH, lobbyPlayer.getName(), lobbyPlayer.getId(), previousMonthInts[0], previousMonthInts[1])));
				inventory.addItem(previousMonth);
			}
			
			int nextMonthPos = 23;
			if(stats.isMaxMonth(month, year)){
				Item noNextMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
						.withDamage((short) 3)
						.withName(strings.noNextMonth)
						.withPlayerSkullTexture(Constants.RIGHT_GRAYED_ARROW)
						.withLore(Lists.newArrayList(strings.noNextMonthLore))
						.withRequireUniqueKey(true)
						.withPosition(nextMonthPos)
						.build();				
				inventory.addItem(noNextMonth);
			}else{
				Integer[] nextMonthInts = stats.getNextMonth(month, year);
				String nextMonthString = stats.monthIntToString(nextMonthInts[0]);
				Item nextMonth = new Item.ItemBuilder(Material.SKULL_ITEM)
						.withDamage((short) 3)
						.withName(strings.nextMonth)
						.withPlayerSkullTexture(Constants.RIGHT_ARROW)
						.withLore(Lists.newArrayList(strings.nextMonthLore.replace("%month%", nextMonthString).replace("%year%", String.valueOf(nextMonthInts[1]))))
						.withRequireUniqueKey(true)
						.withPosition(nextMonthPos)
						.build();
				nextMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.MONTH, lobbyPlayer.getName(), lobbyPlayer.getId(), nextMonthInts[0], nextMonthInts[1])));
				inventory.addItem(nextMonth);
			}
			

			Item currentMonth = new Item.ItemBuilder(Material.GLOWSTONE_DUST)
					.withName(strings.monthSelector)
					.withLore(Lists.newArrayList(strings.monthSelectorLore))
					.withRequireUniqueKey(true)
					.withPosition(22)
					.build();		
			currentMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.GLOBAL, lobbyPlayer.getName(), lobbyPlayer.getId())));		
			inventory.addItem(currentMonth);
			
			// glass panes
			addGlassPanes(ImmutableMap.<Integer, Short>builder()
				.put(0, Constants.WHITE_PANE)
				.put(1,Constants.GREEN_PANE)
				.put(2,Constants.WHITE_PANE)
				.put(6,Constants.WHITE_PANE)
				.put(7,Constants.GREEN_PANE)
				.put(8,Constants.WHITE_PANE)
				.put(9,Constants.GREEN_PANE)
				.put(17,Constants.GREEN_PANE)
				.put(18,Constants.WHITE_PANE)
				.put(19,Constants.GREEN_PANE)
				.put(20,Constants.WHITE_PANE)
				.put(24,Constants.WHITE_PANE)
				.put(25,Constants.GREEN_PANE)
				.build()
			);
			
		}
		
		Item previous = new Item.ItemBuilder(Material.RAW_FISH)
				.withDamage((short) 3)
				.withName(strings.back)
				.withLore(Lists.newArrayList(strings.backToMain))
				.withPosition(26)
				.build();
		previous.setActions(Lists.newArrayList(new OpenInventoryAction(Constants.STATISTICS_MAIN_INVENTORY)));
		inventory.addItem(previous);
		
		return inventory;
	}

}
