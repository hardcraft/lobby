package com.gmail.val59000mc.lobby.stats.pages;

public enum Page {
	GLOBAL(new GlobalPageBuilder()), // welcome scoreboard
	LOBBY(new LobbyPageBuilder()),
	KILL_SKILL(new KillSkillPageBuilder()), // global stats
	SURVIVAL_RUSH(new SurvivalRushPageBuilder()),
	EVOLUTION(new EvolutionPageBuilder()),
	CTF(new CtfPageBuilder()),
	//GLADIATOR(new GladiatorPageBuilder()),
	RAGE(new RagePageBuilder()),
    TDM(new TdmPageBuilder());
	
	private PageBuilder pageBuilder;
	
	private Page(PageBuilder pageBuilder){
		this.pageBuilder = pageBuilder;
	}
	
	public PageBuilder getPageBuilder() {
		return pageBuilder;
	}

	public Page next(){
		return values()[(ordinal() + 1) % values().length];
	}
	
	public boolean isLastPage(){
		return ordinal()+1 == values().length;
	}
}
