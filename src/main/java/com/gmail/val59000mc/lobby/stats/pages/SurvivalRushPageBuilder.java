package com.gmail.val59000mc.lobby.stats.pages;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

import java.util.ArrayList;
import java.util.List;

public class SurvivalRushPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();
		
		content.add(s.title);
		content.add(s.survivalRush);
		
		content.add(" ");
		
		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthSurvivalRushKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthSurvivalRushDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthSurvivalRushCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.monthSurvivalRushWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthSurvivalRushPlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalSurvivalRushKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalSurvivalRushDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalSurvivalRushCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.globalSurvivalRushWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalSurvivalRushPlayTime));
		return content;
	}

}
