package com.gmail.val59000mc.lobby.stats.inventories;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.lobby.stats.InventoryStats;

public class InventoryStrings {

	private HCGameAPI api;
	private InventoryStats stats;
	
	public InventoryStrings(HCGameAPI api, InventoryStats stats) {
		this.api = api;
		this.stats = stats;
	}

	public HCGameAPI getApi() {
		return api;
	}

	public InventoryStats getStats() {
		return stats;
	}

	public String kills;
	public String deaths;
	public String ratio;
	public String playTime;
	public String mediumPlayTime;
	public String coinsEarned;
	public String wins;
	
	// kill skill
	public String experienceLooted;
	public String lapisLooted;
	public String potionLooted;
	public String ironLooted;
	public String diamondLooted;
	public String blazeLooted;
	public String headLooted;
	public String grenadeLooted;
	public String tntLooted;
	public String cannonLooted;
	public String supplyOpened;
	
	// survival rush
	public String skeletonKilled;
	public String blocksPlaced;
	public String ironMined;
	public String diamondMined;
	public String goldMined;
	public String gravelMined;
	
	// evolution
	public String starsEarned;
	
	// ctf
	public String flagsTaken;
	public String flagsSaved;
	
	// gladiator
	public String gladiatorsKilled;
	public String slavesKilled;
	public String gladiatorWins;
	public String slaveWins;

	// rage
	public String rageBestKillStreak;
	public String rageTiers1Gagdets;
	public String rageTiers2Gagdets;
	public String rageTiers3Gagdets;
	public String rageDoubleJumps;
	
	public String globalInventory;
	public String monthInventory;
	public String globalRankingInventory;
	public String monthRankingInventory;
	
	public String globalItemHeader;
	public String monthItemHeader;
	public String globalSumItemHeader;
	public String monthSumItemHeader;
	public String globalRankingItemHeader;
	public String monthRankingItemHeader;
	
	public String back;
	public String backToMain;
	public String playerNotFoundName;
	public String playerNotFoundLore;
	public String chooseNickname;
	public String chooseNicknameLore;
	public String previousPlayers;
	public String previousPlayersLore;
	public String nextPlayers;
	public String nextPlayersLore;
	public String previousMonth;
	public String previousMonthLore;
	public String nextMonth;
	public String nextMonthLore;
	public String noPreviousMonth;
	public String noPreviousMonthLore;
	public String noPreviousPlayers;
	public String noPreviousPlayersLore;
	public String noNextPlayers;
	public String noNextPlayersLore;
	public String noRankedPlayer;
	public String noRankedPlayerLore;
	public String noNextMonth;
	public String noNextMonthLore;
	public String filterKills;
	public String filterDeaths;
	public String filterRatio;
	public String filterCoins;
	public String filterPlayTime;
	public String filterWins;
	public String globalSelector;
	public String globalSelectorLore;
	public String monthSelector;
	public String monthSelectorLore;

	public String all;
	public String lobby;
	public String killSkill;
	public String survivalRush;
	public String evolution;
	public String ctf;
	public String gladiator;
	public String rage;
	public String tdm;

	public String allStripped;
	public String lobbyStripped;
	public String killSkillStripped;
	public String survivalRushStripped;
	public String evolutionStripped;
	public String ctfStripped;
	public String gladiatorStripped;
	public String rageStripped;
	public String tdmStripped;

	public void setup() {
		HCStringsAPI s = api.getStringsAPI();
		
		String c = "lobby.statistics.item-content.";
		String g = "lobby.statistics.games.";
		
		kills = s.get(c+"kills").toString(); 
		deaths = s.get(c+"deaths").toString(); 
		ratio = s.get(c+"ratio").toString(); 
		playTime = s.get(c+"play-time").toString(); 
		mediumPlayTime = s.get(c+"medium-play-time").toString(); 
		coinsEarned = s.get(c+"coins-earned").toString(); 
		wins = s.get(c+"wins").toString(); 
		
		// kill skill
		experienceLooted = s.get(c+"experience-looted").toString();
		lapisLooted = s.get(c+"lapis-looted").toString();
		potionLooted = s.get(c+"potion-looted").toString();
		ironLooted = s.get(c+"iron-looted").toString();
		diamondLooted = s.get(c+"diamond-looted").toString();
		blazeLooted = s.get(c+"blaze-looted").toString();
		headLooted = s.get(c+"head-looted").toString();
		grenadeLooted = s.get(c+"grenade-looted").toString();
		tntLooted = s.get(c+"tnt-looted").toString();
		cannonLooted = s.get(c+"cannon-looted").toString();
		supplyOpened = s.get(c+"supply-opened").toString();
		
		// survival rush
		skeletonKilled = s.get(c+"skeletons-killed").toString();
		blocksPlaced = s.get(c+"blocks-placed").toString();
		ironMined = s.get(c+"iron-mined").toString();
		diamondMined = s.get(c+"diamond-mined").toString();
		goldMined = s.get(c+"gold-mined").toString();
		gravelMined = s.get(c+"gravel-mined").toString();
		
		// evolution
		starsEarned = s.get(c+"stars-earned").toString();
		
		// ctf
		flagsSaved = s.get(c+"flags-saved").toString();
		flagsTaken = s.get(c+"flags-taken").toString();
		
		// gladiator
		gladiatorsKilled = s.get(c+"gladiator-killed").toString();
		slavesKilled = s.get(c+"slaves-killed").toString();
		gladiatorWins = s.get(c+"gladiator-wins").toString();
		slaveWins = s.get(c+"slave-wins").toString();

		// rage
		rageBestKillStreak = s.get(c+"best-kill-streak").toString();
		rageTiers1Gagdets = s.get(c+"tier-1-gagdets").toString();
		rageTiers2Gagdets = s.get(c+"tier-2-gagdets").toString();
		rageTiers3Gagdets = s.get(c+"tier-3-gagdets").toString();
		rageDoubleJumps = s.get(c+"double-jumps").toString();
		
		globalInventory = s.get("lobby.statistics.global-inventory.title").toString();
		monthInventory = s.get("lobby.statistics.month-inventory.title").toString();
		globalRankingInventory = s.get("lobby.statistics.global-ranking-inventory.title").toString();
		monthRankingInventory = s.get("lobby.statistics.month-ranking-inventory.title").toString();
		
		globalItemHeader = s.get("lobby.statistics.global-item-header").toString();
		monthItemHeader = s.get("lobby.statistics.month-item-header").toString();
		globalSumItemHeader = s.get("lobby.statistics.global-sum-item-header").toString();
		monthSumItemHeader = s.get("lobby.statistics.month-sum-item-header").toString();
		globalRankingItemHeader = s.get("lobby.statistics.global-ranking-item-header").toString();
		monthRankingItemHeader = s.get("lobby.statistics.month-ranking-item-header").toString();
		
		back = s.get("lobby.statistics.back").toString();
		backToMain = s.get("lobby.statistics.back-to-main-lore").toString();
		playerNotFoundName = s.get("lobby.statistics.player-not-found.name").toString();
		playerNotFoundLore = s.get("lobby.statistics.player-not-found.lore").toString();
		chooseNickname = s.get("lobby.statistics.choose-nickname.name").toString();
		chooseNicknameLore = s.get("lobby.statistics.choose-nickname.lore").toString();
		previousPlayers = s.get("lobby.statistics.previous-players.name").toString();
		previousPlayersLore = s.get("lobby.statistics.previous-players.lore").toString();
		nextPlayers = s.get("lobby.statistics.next-players.name").toString();
		nextPlayersLore = s.get("lobby.statistics.next-players.lore").toString();
		previousMonth = s.get("lobby.statistics.previous-month.name").toString();
		previousMonthLore = s.get("lobby.statistics.previous-month.lore").toString();
		nextMonth = s.get("lobby.statistics.next-month.name").toString();
		nextMonthLore = s.get("lobby.statistics.next-month.lore").toString();
		noPreviousMonth = s.get("lobby.statistics.no-previous-month.name").toString();
		noPreviousMonthLore = s.get("lobby.statistics.no-previous-month.lore").toString();
		noPreviousPlayers = s.get("lobby.statistics.no-previous-players.name").toString();
		noPreviousPlayersLore = s.get("lobby.statistics.no-previous-players.lore").toString();
		noNextPlayers = s.get("lobby.statistics.no-next-players.name").toString();
		noNextPlayersLore = s.get("lobby.statistics.no-next-players.lore").toString();
		noRankedPlayer = s.get("lobby.statistics.no-ranked-player.name").toString();
		noRankedPlayerLore = s.get("lobby.statistics.no-ranked-player.lore").toString();
		noNextMonth = s.get("lobby.statistics.no-next-month.name").toString();
		noNextMonthLore = s.get("lobby.statistics.no-next-month.lore").toString();
		filterKills = s.get("lobby.statistics.filters.kills").toString();
		filterDeaths = s.get("lobby.statistics.filters.deaths").toString();
		filterRatio = s.get("lobby.statistics.filters.ratio").toString();
		filterCoins = s.get("lobby.statistics.filters.coins-earned").toString();
		filterPlayTime = s.get("lobby.statistics.filters.play-time").toString();
		filterWins = s.get("lobby.statistics.filters.wins").toString();
		globalSelector = s.get("lobby.statistics.global-selector.name").toString();
		globalSelectorLore = s.get("lobby.statistics.global-selector.lore").toString();
		monthSelector = s.get("lobby.statistics.month-selector.name").toString();
		monthSelectorLore = s.get("lobby.statistics.month-selector.lore").toString();
		
		
		all = s.get(g+"all").toString(); 
		lobby = s.get(g+"lobby").toString(); 
		killSkill = s.get(g+"kill-skill").toString(); 
		survivalRush = s.get(g+"survival-rush").toString(); 
		evolution = s.get(g+"evolution").toString(); 
		ctf = s.get(g+"ctf").toString(); 
		gladiator = s.get(g+"gladiator").toString(); 
		rage = s.get(g+"rage").toString();
		tdm = s.get(g+"tdm").toString();

		allStripped = s.getNoColor(g+"all").toString(); 
		lobbyStripped = s.getNoColor(g+"lobby").toString(); 
		killSkillStripped = s.getNoColor(g+"kill-skill").toString(); 
		survivalRushStripped = s.getNoColor(g+"survival-rush").toString(); 
		evolutionStripped = s.getNoColor(g+"evolution").toString(); 
		ctfStripped = s.getNoColor(g+"ctf").toString(); 
		gladiatorStripped = s.getNoColor(g+"gladiator").toString(); 
		rageStripped = s.getNoColor(g+"rage").toString();
		tdmStripped = s.getNoColor(g+"tdm").toString();

	}



}
