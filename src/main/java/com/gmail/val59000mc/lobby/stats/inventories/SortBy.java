package com.gmail.val59000mc.lobby.stats.inventories;

public enum SortBy {
	KILLS("kills","player.id > 0","kills"),
	DEATHS("deaths","player.id > 0","morts"),
	RATIO("kills / IF(game.deaths = 0, 1, game.deaths)", "game.kills > 300","ratio"),
	COINS("coins_earned","player.id > 0","coins"),
	PLAY_TIME("play_time","player.id > 0","temps"),
	WINS("wins","player.id > 0","wins");
	
	private String columnName;
	private String where;
	private String columnDescription;
	
	private SortBy(String columnName, String where, String columnDescription) {
		this.columnName = columnName;
		this.where = where;
		this.columnDescription = columnDescription;
	}
	

	public String getColumnName() {
		return columnName;
	}

	public String getWhere() {
		return where;
	}

	public String getColumnDescription() {
		return columnDescription;
	}
	
	
}
