package com.gmail.val59000mc.lobby.stats.inventories.ranking;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.GlobalRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.MonthRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.google.common.collect.Lists;

import java.util.List;

public class KillSkillBuilder extends RankingItemBuilder{

	@Override
	public List<String> buildGlobalLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.globalKillSkillKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.globalKillSkillDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.globalKillSkillKills, lobbyPlayer.globalKillSkillDeaths)),
			s.experienceLooted+lobbyPlayer.globalKillSkillExperienceLooted,
			s.lapisLooted+lobbyPlayer.globalKillSkillLapisLooted,
			s.potionLooted+lobbyPlayer.globalKillSkillPotionLooted,
			s.ironLooted+lobbyPlayer.globalKillSkillIronLooted,
			s.diamondLooted+lobbyPlayer.globalKillSkillDiamondLooted,
			s.blazeLooted+lobbyPlayer.globalKillSkillBlazeLooted,
			s.headLooted+lobbyPlayer.globalKillSkillHeadLooted,
			s.grenadeLooted+lobbyPlayer.globalKillSkillGrenadeLooted,
			s.tntLooted+lobbyPlayer.globalKillSkillTntLooted,
			s.cannonLooted+lobbyPlayer.globalKillSkillCannonLooted,
			s.supplyOpened+lobbyPlayer.globalKillSkillSupplyOpened,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.globalKillSkillCoinsEarned),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.globalKillSkillPlayTime))
		);
	}

	@Override
	public List<String> buildMonthLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.monthKillSkillKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.monthKillSkillDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.monthKillSkillKills, lobbyPlayer.monthKillSkillDeaths)),
			s.experienceLooted+lobbyPlayer.monthKillSkillExperienceLooted,
			s.lapisLooted+lobbyPlayer.monthKillSkillLapisLooted,
			s.potionLooted+lobbyPlayer.monthKillSkillPotionLooted,
			s.ironLooted+lobbyPlayer.monthKillSkillIronLooted,
			s.diamondLooted+lobbyPlayer.monthKillSkillDiamondLooted,
			s.blazeLooted+lobbyPlayer.monthKillSkillBlazeLooted,
			s.headLooted+lobbyPlayer.monthKillSkillHeadLooted,
			s.grenadeLooted+lobbyPlayer.monthKillSkillGrenadeLooted,
			s.tntLooted+lobbyPlayer.monthKillSkillTntLooted,
			s.cannonLooted+lobbyPlayer.monthKillSkillCannonLooted,
			s.supplyOpened+lobbyPlayer.monthKillSkillSupplyOpened,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.monthKillSkillCoinsEarned),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.monthKillSkillPlayTime))
		);
	}
	
	@Override
	public void addGlobalFiltersItems(InventoryStrings s, GlobalRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.WINS_FILLTER_POSITION);
	}

	@Override
	public void addMonthFiltersItems(InventoryStrings s, MonthRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addEmptyFilter(inventory, Constants.WINS_FILLTER_POSITION);
	}

}
