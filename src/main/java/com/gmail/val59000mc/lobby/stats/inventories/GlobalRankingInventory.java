package com.gmail.val59000mc.lobby.stats.inventories;

import java.time.LocalDateTime;

import org.bukkit.Material;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public class GlobalRankingInventory extends AbstractRankingGlobalStatsInventory{

	public GlobalRankingInventory(InventoryStats stats, Game game, Integer page, SortBy sortBy) {
		super(
			stats, 
			game,
			page,
			sortBy
		);
		this.type = InventoryType.RANKING_GLOBAL;
	}


	@Override
	public void loadStats() {
		this.lobbyPlayers = stats.loadGlobalRankingPlayers(game.getGlobalTable(), game.equals(Game.GLOBAL) ? "id" : "player_id", sortBy, page);
		this.globalSum = stats.loadSumGlobalStats();
	}
	
	@Override
	public Object[] getArgs(){
		return new Object[]{ game, page, sortBy };
	}
	
	@Override
	public Inventory buildInventory() {
		InventoryStrings s = stats.getStrings();
		
		this.inventory = new Inventory(
			getName(), 
			s.globalRankingInventory
				.replace("%sortBy%", sortBy.getColumnDescription())
				.replace("%game%", game.getShortName()),
			6
		);
		
		// game selector
		addGameSelectorItems();
		
		// previous players
		int previousPlayersPos = 19;
		if(page == 0){
			Item previousPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(s.noPreviousPlayers)
					.withLore(Lists.newArrayList(s.noPreviousPlayersLore))
					.withPlayerSkullTexture(Constants.LEFT_GRAYED_ARROW)
					.withRequireUniqueKey(true)
					.withPosition(previousPlayersPos)
					.build();
			inventory.addItem(previousPlayers);
		}else{
			Item previousPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullTexture(Constants.LEFT_ARROW)
					.withName(s.previousPlayers)
					.withLore(Lists.newArrayList(s.previousPlayersLore))
					.withRequireUniqueKey(true)
					.withPosition(previousPlayersPos)
					.build();
			previousPlayers.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, game, page-1, sortBy)));
			inventory.addItem(previousPlayers);
		}

		
		// next players
		int nextPlayersPos = 25;
		if(lobbyPlayers.size() < 5 || page == 11 || (lobbyPlayers.size() == 5 && lobbyPlayers.get(4) == null)){
			Item nextPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withName(s.noNextPlayers)
					.withLore(Lists.newArrayList(s.noNextPlayersLore))
					.withPlayerSkullTexture(Constants.RIGHT_GRAYED_ARROW)
					.withRequireUniqueKey(true)
					.withPosition(nextPlayersPos)
					.build();
			inventory.addItem(nextPlayers);
		}else{
			Item nextPlayers = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullTexture(Constants.RIGHT_ARROW)
					.withName(s.nextPlayers)
					.withLore(Lists.newArrayList(s.nextPlayersLore))
					.withRequireUniqueKey(true)
					.withPosition(nextPlayersPos)
					.build();
			nextPlayers.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, game, page+1, sortBy)));
			inventory.addItem(nextPlayers);
		}
		
		// players
		int playerPos = 20;
		int index = 0;
		for(LobbyPlayer lobbyPlayer : lobbyPlayers){
			int rank = page*5+index+1;
			Item player = buildPlayerSkull(s, lobbyPlayer, playerPos, index, rank);
			inventory.addItem(player);
			index++;
		}
	
		// filters
		game.addGlobalFiltersItems(s,this);
		
		// months
		
		Item noPreviousMonth = new Item.ItemBuilder(Material.STONE_BUTTON)
				.withName("  ")
				.withRequireUniqueKey(true)
				.withPosition(30)
				.build();
		inventory.addItem(noPreviousMonth);
		
		
		LocalDateTime now = LocalDateTime.now();
		int currentMonthNb = now.getMonth().getValue();
		int currentYearNb = now.getYear();
		Item currentMonth = new Item.ItemBuilder(Material.SULPHUR)
				.withName(s.globalSelector)
				.withLore(Lists.newArrayList(s.globalSelectorLore))
				.withRequireUniqueKey(true)
				.withPosition(31)
				.build();		
		currentMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_MONTH, game, 0, sortBy, currentMonthNb, currentYearNb)));		
		inventory.addItem(currentMonth);
		
		Item noNextMonth = new Item.ItemBuilder(Material.STONE_BUTTON)
				.withName("  ")
				.withRequireUniqueKey(true)
				.withPosition(32)
				.build();
		inventory.addItem(noNextMonth);
		
		// glass panes
		addGlassPanes(ImmutableMap.<Integer, Short>builder()
			.put(2,Constants.GREEN_PANE)
			.put(9,Constants.GREEN_PANE)
			.put(10,Constants.WHITE_PANE)
			.put(11,Constants.WHITE_PANE)
			.put(12,Constants.WHITE_PANE)
			.put(13,Constants.WHITE_PANE)
			.put(14,Constants.WHITE_PANE)
			.put(15,Constants.WHITE_PANE)
			.put(16,Constants.WHITE_PANE)
			.put(17,Constants.GREEN_PANE)
			.put(18,Constants.WHITE_PANE)
			.put(26,Constants.WHITE_PANE)
			.put(27,Constants.GREEN_PANE)
			.put(28,Constants.WHITE_PANE)
			.put(29,Constants.WHITE_PANE)
			.put(33,Constants.WHITE_PANE)
			.put(34,Constants.WHITE_PANE)
			.put(35,Constants.GREEN_PANE)
			.put(36,Constants.GREEN_PANE)
			.put(37,Constants.GREEN_PANE)
			.put(38,Constants.GREEN_PANE)
			.put(39,Constants.WHITE_PANE)
			.put(40,Constants.WHITE_PANE)
			.put(41,Constants.WHITE_PANE)
			.put(42,Constants.GREEN_PANE)
			.put(43,Constants.GREEN_PANE)
			.put(44,Constants.GREEN_PANE)
			.put(45,Constants.WHITE_PANE)
			.put(52,Constants.WHITE_PANE)
			.build()
		);
		
		// back
		Item previous = new Item.ItemBuilder(Material.RAW_FISH)
				.withDamage((short) 3)
				.withName(s.back)
				.withLore(Lists.newArrayList(s.backToMain))
				.withPosition(53)
				.build();
		previous.setActions(Lists.newArrayList(new OpenInventoryAction(Constants.STATISTICS_MAIN_INVENTORY)));
		inventory.addItem(previous);
		
		return inventory;
	}


	private Item buildPlayerSkull(InventoryStrings s, LobbyPlayer lobbyPlayer, int playerPos, int index, int rank) {
		if(lobbyPlayer == null){
			return new Item.ItemBuilder(Material.SKULL_ITEM)
				.withRequireUniqueKey(true)
				.withPosition(playerPos+index)
				.withName(s.noRankedPlayer)
				.withAmount(rank)
				.withLore(Lists.newArrayList(s.noRankedPlayerLore
						.replace("%rank%", String.valueOf(rank))
						.replace("%game%", game.getName())
				))
				.build();
		}else{
			Item player = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullName(lobbyPlayer.getName())
					.withRequireUniqueKey(true)
					.withPosition(playerPos+index)
					.withName("§a"+lobbyPlayer.getName())
					.withAmount(rank)
					.withLore(Lists.newArrayList(s.globalRankingItemHeader
							.replace("%rank%", String.valueOf(rank))
							.replace("%player%", lobbyPlayer.getName())
							.replace("%game%", game.getName()),
							game.getRankingGlobalItemLore(s,lobbyPlayer, sortBy)
					))
					.build();
			player.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(s.getStats(), InventoryType.GLOBAL, lobbyPlayer.getName(), lobbyPlayer.getId())));
			return player;
		}
		
	}

}
