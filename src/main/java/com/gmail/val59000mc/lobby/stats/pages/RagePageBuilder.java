package com.gmail.val59000mc.lobby.stats.pages;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

import java.util.ArrayList;
import java.util.List;

public class RagePageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();

		content.add(s.title);
		content.add(s.rage);
		
		content.add(" ");
		
		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthRageKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthRageDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthRageCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.monthRageWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthRagePlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalRageKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalRageDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalRageCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.globalRageWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalRagePlayTime));
		return content;
	}

}
