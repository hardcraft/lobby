package com.gmail.val59000mc.lobby.stats;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.AbstractCachedInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryType;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;

public class InventoryStats{

	private HCGameAPI api;
	private Map<String, AbstractCachedInventory> cache;
	private InventoryStrings strings;
	private InventoryManager manager;
	private long cacheExpiration;
	private long accessExpiration;
	
	//sql
	private String selectGlobalStats;
	private String selectMonthStats;
	private String selectSumGlobalStats;
	private String selectSumMonthStats;
	private String selectGlobalRankingNames;
	private String selectMonthRankingNames;
	
	public InventoryStats(HCGameAPI api, InventoryManager manager, Long cacheExpiration, Long accessExpiration){
		this.api = api;
		this.manager = manager;
		this.cache = new HashMap<String,AbstractCachedInventory>();
		this.cacheExpiration = cacheExpiration;
		this.accessExpiration = accessExpiration;
		this.strings = new InventoryStrings(api, this);
		this.strings.setup();
		setup();
	}



	public InventoryStrings getStrings() {
		return strings;
	}
	
	
	private void setup() {
		HCMySQLAPI sql = api.getMySQLAPI();
		if(sql.isEnabled()){
			selectGlobalStats = sql.readQueryFromResource(api.getPlugin(),"sql/select_inv_global_stats.sql");
			selectMonthStats = sql.readQueryFromResource(api.getPlugin(),"sql/select_inv_month_stats.sql");
			selectSumGlobalStats = sql.readQueryFromResource(api.getPlugin(),"sql/select_sum_global_stats.sql");
			selectSumMonthStats = sql.readQueryFromResource(api.getPlugin(),"sql/select_sum_month_stats.sql");
			selectGlobalRankingNames = sql.readQueryFromResource(api.getPlugin(),"sql/select_global_ranking_names.sql");
			selectMonthRankingNames = sql.readQueryFromResource(api.getPlugin(),"sql/select_month_ranking_names.sql");
		}
		
		api.buildTask("Purge inventory stats cache", new HCTask() {
			
			@Override
			public void run() {
				synchronized (cache) {
					Iterator<Map.Entry<String,AbstractCachedInventory>> it = cache.entrySet().iterator();
					while(it.hasNext()){
						Entry<String,AbstractCachedInventory> entry = it.next();
						AbstractCachedInventory inv = entry.getValue();
						if(hasExpired(inv)){
							forceCloseBukkitInventoryAndReload(inv);
							manager.removeInventory(inv.getInventory());
							it.remove();
						}
					}
				}
			}
		})
		.withInterval(6000)
		.withDelay(6000) // 5 minutes
		.build()
		.start();
	}



	public HCGameAPI getApi() {
		return api;
	}

	public AbstractCachedInventory addOrGetFromCache(InventoryType type, Object... args){
		AbstractCachedInventory newInv = type.newCachedInventory(this, args);
		String name = newInv.getName();

		synchronized (cache) {
			if(isCached(name)){
				if(hasExpired(name)){
					removeFromCache(name);
				}else{
					return getFromCache(name);
				}
			}
			return addToCache(newInv);
		}
		
	}
	
	private AbstractCachedInventory addToCache(AbstractCachedInventory newInv) {
		synchronized (cache) {
			String name = newInv.getName();
			if(cache.containsKey(name))
				throw new IllegalStateException("Inventory "+name+" is already cached !");
			cache.put(name, newInv);
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				Log.debug("Start loading "+newInv.getName());
				long start = System.currentTimeMillis();
				newInv.loadStats();
				Log.debug("End loading "+newInv.getName()+" time="+(System.currentTimeMillis()-start));
				Bukkit.getScheduler().runTask(api.getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						Inventory sigInv = newInv.buildInventory();
						manager.addInventory(sigInv);
						newInv.setLoaded();
					}
				});
			}
		});
		
		return newInv;
	}
	
	private boolean removeFromCache(String inventoryName){
		if(cache.containsKey(inventoryName)){
			AbstractCachedInventory inv = cache.get(inventoryName);
			removeFromCache(inv);
			return true;
		}
		return false;
	}
	
	private boolean removeFromCache(AbstractCachedInventory inv){
		if(inv != null){
			Inventory inventory = inv.getInventory();
			forceCloseBukkitInventoryAndReload(inv);
			manager.removeInventory(inventory);
			cache.remove(inv.getName());
			return true;
		}
		return false;
	}

	private void forceCloseBukkitInventoryAndReload(AbstractCachedInventory statsInv) {
		if(statsInv == null || statsInv.getInventory() == null){
			return;
		}
		String title = statsInv.getInventory().getTitle();
		
		for(Player player : Bukkit.getOnlinePlayers()){
			InventoryView view = player.getOpenInventory();
			if(view != null){
				org.bukkit.inventory.Inventory inv = view.getTopInventory();
				if(inv != null && inv.getName() != null && inv.getName().equals(title)){
					inv.clear();
					LobbyPlayer lobbyPlayer = (LobbyPlayer) api.getPlayersManagerAPI().getHCPlayer(player);
					if(lobbyPlayer != null){
						api.getStringsAPI().get("lobby.statistics.force-close-inventory").replace("%time%", Time.getFormattedTime(cacheExpiration/1000)).sendChatP(lobbyPlayer);						
						
						Object[] args = statsInv.getArgs();
						InventoryType type = statsInv.getType();
						String playerName = player.getName();
						

						// auto reload inventory after expiration
						Bukkit.getScheduler().runTaskLater(api.getPlugin(), new Runnable() {
							
							@Override
							public void run() {
								
								// Load player
								Player player = Bukkit.getPlayer(playerName);
								if(player == null) return;
								SigPlayer sigPlayer = PlayersManager.getSigPlayer(player);
								if(sigPlayer == null) return;
								
								
								AbstractCachedInventory inv = addOrGetFromCache(type, args);
								Sounds.play(player, Sound.NOTE_STICKS, 0.3f, 2);
								
								if(inv.isLoaded()){
									inv.getInventory().openInventory(player, sigPlayer);
									inv.updateAccessDate();
								}else{
									api.buildTask("wait loading '"+inv.getName()+"' for "+player.getName(), new HCTask() {
										
										@Override
										public void run() {
											if(inv.isLoaded()){
												inv.getInventory().openInventory(player, sigPlayer);
												inv.updateAccessDate();
												scheduler.stop();
											}
										}
									})
									.withInterval(5)
									.withDelay(1)
									.build()
									.start();
								}
							}
						}, 2);
					}
				}
			}
		}
	}
	
	public AbstractCachedInventory getFromCache(String inventoryName){
		return cache.get(inventoryName);
	}

	public boolean isCached(String invName){
		return cache.containsKey(invName);
	}
	
	public boolean hasExpired(String inventoryName){
		if(cache.containsKey(inventoryName)){
			AbstractCachedInventory cached = cache.get(inventoryName);
			return hasExpired(cached);
		}
		return false;
	}

	public boolean hasExpired(AbstractCachedInventory inv){
		if(inv != null){
			Long now = System.currentTimeMillis();
			return (
						now-inv.getCreatedAt() > cacheExpiration 
						&& now-inv.getLastAccessedAt() > accessExpiration
				   )
				   || now-inv.getCreatedAt() > 2*cacheExpiration; // force expire if too many access after normal expiration
		}
		return false;
	}



	public LobbyPlayer loadGlobalStats(String preLobbyPlayer, Integer preLobbyPlayerId) {
		
		LobbyPlayer lobbyPlayer = new LobbyPlayer(preLobbyPlayer);
		lobbyPlayer.setId(preLobbyPlayerId);
		
		try{

			HCMySQLAPI sql = api.getMySQLAPI();
			
			CachedRowSet stats = sql.query(sql.prepareStatement(selectGlobalStats, preLobbyPlayerId));
			
			boolean found = stats.first();
			
			if(!found){
				return null; // no player found with this name
			}
			
			return extractGlobalDataToPlayer(lobbyPlayer, stats);
			
		} catch (SQLException e) {
			e.printStackTrace();
			Log.severe("Couldn't read statistics for player "+lobbyPlayer.getName()+" Read error above.");
		}
		
		return null;
	}
	
	public LobbyPlayer loadSumGlobalStats() {
		
		LobbyPlayer lobbyPlayer = new LobbyPlayer("sum");
		
		try{

			HCMySQLAPI sql = api.getMySQLAPI();
			
			CachedRowSet stats = sql.query(sql.prepareStatement(selectSumGlobalStats));
			
			if(stats.first()){
				return extractGlobalDataToPlayer(lobbyPlayer, stats);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			Log.severe("Couldn't read global sum statistics. Read error above.");
		}
		
		return null;
	}
	
	private LobbyPlayer extractGlobalDataToPlayer(LobbyPlayer lobbyPlayer, CachedRowSet stats) throws SQLException{
		lobbyPlayer.globalKills = stats.getInt("globalKills");
		lobbyPlayer.globalDeaths = stats.getInt("globalDeaths");
		lobbyPlayer.globalCoinsEarned = stats.getDouble("globalCoinsEarned");
		lobbyPlayer.globalPlayTime = (int) Math.round(stats.getDouble("globalPlayTime"));
		lobbyPlayer.globalWins = stats.getInt("globalWins");
		
		
		lobbyPlayer.globalLobbyPlayTime = (int) Math.round(stats.getDouble("globalLobbyPlayTime"));
		

		lobbyPlayer.globalKillSkillKills = stats.getInt("globalKillSkillKills");
		lobbyPlayer.globalKillSkillDeaths = stats.getInt("globalKillSkillDeaths");
		lobbyPlayer.globalKillSkillExperienceLooted = stats.getInt("globalKillSkillExperienceLooted");
		lobbyPlayer.globalKillSkillLapisLooted = stats.getInt("globalKillSkillLapisLooted");
		lobbyPlayer.globalKillSkillPotionLooted = stats.getInt("globalKillSkillPotionLooted");
		lobbyPlayer.globalKillSkillIronLooted = stats.getInt("globalKillSkillIronLooted");
		lobbyPlayer.globalKillSkillDiamondLooted = stats.getInt("globalKillSkillDiamondLooted");
		lobbyPlayer.globalKillSkillBlazeLooted = stats.getInt("globalKillSkillBlazeLooted");
		lobbyPlayer.globalKillSkillHeadLooted = stats.getInt("globalKillSkillHeadLooted");
		lobbyPlayer.globalKillSkillGrenadeLooted = stats.getInt("globalKillSkillGrenadeLooted");
		lobbyPlayer.globalKillSkillTntLooted = stats.getInt("globalKillSkillTntLooted");
		lobbyPlayer.globalKillSkillCannonLooted = stats.getInt("globalKillSkillCannonLooted");
		lobbyPlayer.globalKillSkillSupplyOpened = stats.getInt("globalKillSkillSupplyOpened");
		lobbyPlayer.globalKillSkillCoinsEarned = stats.getDouble("globalKillSkillCoinsEarned");
		lobbyPlayer.globalKillSkillPlayTime = (int) Math.round(stats.getDouble("globalKillSkillPlayTime"));
		

		lobbyPlayer.globalSurvivalRushKills = stats.getInt("globalSurvivalRushKills");
		lobbyPlayer.globalSurvivalRushDeaths = stats.getInt("globalSurvivalRushDeaths");
		lobbyPlayer.globalSurvivalRushSkeletonsKilled = stats.getInt("globalSurvivalRushSkeletonsKilled");
		lobbyPlayer.globalSurvivalRushBlocksPlaced = stats.getInt("globalSurvivalRushBlocksPlaced");
		lobbyPlayer.globalSurvivalRushIronMined = stats.getInt("globalSurvivalRushIronMined");
		lobbyPlayer.globalSurvivalRushDiamondMined = stats.getInt("globalSurvivalRushDiamondMined");
		lobbyPlayer.globalSurvivalRushGoldMined = stats.getInt("globalSurvivalRushGoldMined");
		lobbyPlayer.globalSurvivalRushGravelMined = stats.getInt("globalSurvivalRushGravelMined");
		lobbyPlayer.globalSurvivalRushCoinsEarned = stats.getDouble("globalSurvivalRushCoinsEarned");
		lobbyPlayer.globalSurvivalRushPlayTime = (int) Math.round(stats.getDouble("globalSurvivalRushPlayTime"));
		lobbyPlayer.globalSurvivalRushWins = stats.getInt("globalSurvivalRushWins");
		

		lobbyPlayer.globalEvolutionKills = stats.getInt("globalEvolutionKills");
		lobbyPlayer.globalEvolutionDeaths = stats.getInt("globalEvolutionDeaths");
		lobbyPlayer.globalEvolutionStarsEarned = stats.getInt("globalEvolutionStarsEarned");
		lobbyPlayer.globalEvolutionCoinsEarned = stats.getDouble("globalEvolutionCoinsEarned");
		lobbyPlayer.globalEvolutionPlayTime = (int) Math.round(stats.getDouble("globalEvolutionPlayTime"));
		lobbyPlayer.globalEvolutionWins = stats.getInt("globalEvolutionWins");
		

		lobbyPlayer.globalCtfKills = stats.getInt("globalCtfKills");
		lobbyPlayer.globalCtfDeaths = stats.getInt("globalCtfDeaths");
		lobbyPlayer.globalCtfFlagsTaken = stats.getInt("globalCtfFlagsTaken");
		lobbyPlayer.globalCtfFlagsSaved = stats.getInt("globalCtfFlagsSaved");
		lobbyPlayer.globalCtfCoinsEarned = stats.getDouble("globalCtfCoinsEarned");
		lobbyPlayer.globalCtfPlayTime = (int) Math.round(stats.getDouble("globalCtfPlayTime"));
		lobbyPlayer.globalCtfWins = stats.getInt("globalCtfWins");
		

		lobbyPlayer.globalGladiatorKills = stats.getInt("globalGladiatorKills");
		lobbyPlayer.globalGladiatorDeaths = stats.getInt("globalGladiatorDeaths");
		lobbyPlayer.globalGladiatorGladiatorsKilled = stats.getInt("globalGladiatorGladiatorsKilled");
		lobbyPlayer.globalGladiatorSlavesKilled = stats.getInt("globalGladiatorSlavesKilled");
		lobbyPlayer.globalGladiatorGladiatorWins = stats.getInt("globalGladiatorGladiatorWins");
		lobbyPlayer.globalGladiatorSlaveWins = stats.getInt("globalGladiatorSlaveWins");
		lobbyPlayer.globalGladiatorCoinsEarned = stats.getDouble("globalGladiatorCoinsEarned");
		lobbyPlayer.globalGladiatorPlayTime = (int) Math.round(stats.getDouble("globalGladiatorPlayTime"));

		lobbyPlayer.globalRageKills = stats.getInt("globalRageKills");
		lobbyPlayer.globalRageDeaths = stats.getInt("globalRageDeaths");
		lobbyPlayer.globalRageBestKillStreak = stats.getInt("globalRageBestKillStreak");
		lobbyPlayer.globalRageTier1Gadgets = stats.getInt("globalRageTier1Gadgets");
		lobbyPlayer.globalRageTier2Gadgets = stats.getInt("globalRageTier2Gadgets");
		lobbyPlayer.globalRageTier3Gadgets = stats.getInt("globalRageTier3Gadgets");
		lobbyPlayer.globalRageDoubleJumps = stats.getInt("globalRageDoubleJumps");
		lobbyPlayer.globalRageCoinsEarned = stats.getDouble("globalRageCoinsEarned");
		lobbyPlayer.globalRagePlayTime = (int) Math.round(stats.getDouble("globalRagePlayTime"));
		lobbyPlayer.globalRageWins = stats.getInt("globalRageWins");


		lobbyPlayer.globalTdmKills = stats.getInt("globalTdmKills");
		lobbyPlayer.globalTdmDeaths = stats.getInt("globalTdmDeaths");
		lobbyPlayer.globalTdmCoinsEarned = stats.getDouble("globalTdmCoinsEarned");
		lobbyPlayer.globalTdmPlayTime = (int) Math.round(stats.getDouble("globalTdmPlayTime"));
		lobbyPlayer.globalTdmWins = stats.getInt("globalTdmWins");
		return lobbyPlayer;
	}
	
	public LobbyPlayer loadMonthStats(String preLobbyPlayer, Integer preLobbyPlayerId, int month, int year) {
		
		LobbyPlayer lobbyPlayer = new LobbyPlayer(preLobbyPlayer);
		lobbyPlayer.setId(preLobbyPlayerId);
		
		try{

			HCMySQLAPI sql = api.getMySQLAPI();
			
			CachedRowSet stats = sql.query(sql.prepareStatement(selectMonthStats.replace("%month%", String.valueOf(month)).replace("%year%", String.valueOf(year)), preLobbyPlayerId));
			
			boolean found = stats.first();
			
			if(!found){
				return null; // no player found with this name
			}
			
			return extractMonthDataToPlayer(lobbyPlayer, stats);
			
		} catch (SQLException e) {
			e.printStackTrace();
			Log.severe("Couldn't read statistics for player "+lobbyPlayer.getName()+" Read error above.");
		}
		
		return null;
	}
	
	public LobbyPlayer loadSumMonthStats(int month, int year) {
		
		LobbyPlayer lobbyPlayer = new LobbyPlayer("sum");
		
		try{

			HCMySQLAPI sql = api.getMySQLAPI();
			
			CachedRowSet stats = sql.query(sql.prepareStatement(selectSumMonthStats.replace("%month%", String.valueOf(month)).replace("%year%", String.valueOf(year))));
			
			if(stats.first()){
				return extractMonthDataToPlayer(lobbyPlayer, stats);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			Log.severe("Couldn't read month sum statistics. Read error above.");
		}
		
		return null;
	}
	
	private LobbyPlayer extractMonthDataToPlayer(LobbyPlayer lobbyPlayer, CachedRowSet stats) throws SQLException {
		lobbyPlayer.monthKills = stats.getInt("monthKills");
		lobbyPlayer.monthDeaths = stats.getInt("monthDeaths");
		lobbyPlayer.monthCoinsEarned = stats.getDouble("monthCoinsEarned");
		lobbyPlayer.monthPlayTime = (int) Math.round(stats.getDouble("monthPlayTime"));
		lobbyPlayer.monthWins = stats.getInt("monthWins");
		
		
		lobbyPlayer.monthLobbyPlayTime = (int) Math.round(stats.getDouble("monthLobbyPlayTime"));
		

		lobbyPlayer.monthKillSkillKills = stats.getInt("monthKillSkillKills");
		lobbyPlayer.monthKillSkillDeaths = stats.getInt("monthKillSkillDeaths");
		lobbyPlayer.monthKillSkillExperienceLooted = stats.getInt("monthKillSkillExperienceLooted");
		lobbyPlayer.monthKillSkillLapisLooted = stats.getInt("monthKillSkillLapisLooted");
		lobbyPlayer.monthKillSkillPotionLooted = stats.getInt("monthKillSkillPotionLooted");
		lobbyPlayer.monthKillSkillIronLooted = stats.getInt("monthKillSkillIronLooted");
		lobbyPlayer.monthKillSkillDiamondLooted = stats.getInt("monthKillSkillDiamondLooted");
		lobbyPlayer.monthKillSkillBlazeLooted = stats.getInt("monthKillSkillBlazeLooted");
		lobbyPlayer.monthKillSkillHeadLooted = stats.getInt("monthKillSkillHeadLooted");
		lobbyPlayer.monthKillSkillGrenadeLooted = stats.getInt("monthKillSkillGrenadeLooted");
		lobbyPlayer.monthKillSkillTntLooted = stats.getInt("monthKillSkillTntLooted");
		lobbyPlayer.monthKillSkillCannonLooted = stats.getInt("monthKillSkillCannonLooted");
		lobbyPlayer.monthKillSkillSupplyOpened = stats.getInt("monthKillSkillSupplyOpened");
		lobbyPlayer.monthKillSkillCoinsEarned = stats.getDouble("monthKillSkillCoinsEarned");
		lobbyPlayer.monthKillSkillPlayTime = (int) Math.round(stats.getDouble("monthKillSkillPlayTime"));
		

		lobbyPlayer.monthSurvivalRushKills = stats.getInt("monthSurvivalRushKills");
		lobbyPlayer.monthSurvivalRushDeaths = stats.getInt("monthSurvivalRushDeaths");
		lobbyPlayer.monthSurvivalRushSkeletonsKilled = stats.getInt("monthSurvivalRushSkeletonsKilled");
		lobbyPlayer.monthSurvivalRushBlocksPlaced = stats.getInt("monthSurvivalRushBlocksPlaced");
		lobbyPlayer.monthSurvivalRushIronMined = stats.getInt("monthSurvivalRushIronMined");
		lobbyPlayer.monthSurvivalRushDiamondMined = stats.getInt("monthSurvivalRushDiamondMined");
		lobbyPlayer.monthSurvivalRushGoldMined = stats.getInt("monthSurvivalRushGoldMined");
		lobbyPlayer.monthSurvivalRushGravelMined = stats.getInt("monthSurvivalRushGravelMined");
		lobbyPlayer.monthSurvivalRushCoinsEarned = stats.getDouble("monthSurvivalRushCoinsEarned");
		lobbyPlayer.monthSurvivalRushPlayTime = (int) Math.round(stats.getDouble("monthSurvivalRushPlayTime"));
		lobbyPlayer.monthSurvivalRushWins = stats.getInt("monthSurvivalRushWins");
		

		lobbyPlayer.monthEvolutionKills = stats.getInt("monthEvolutionKills");
		lobbyPlayer.monthEvolutionDeaths = stats.getInt("monthEvolutionDeaths");
		lobbyPlayer.monthEvolutionStarsEarned = stats.getInt("monthEvolutionStarsEarned");
		lobbyPlayer.monthEvolutionCoinsEarned = stats.getDouble("monthEvolutionCoinsEarned");
		lobbyPlayer.monthEvolutionPlayTime = (int) Math.round(stats.getDouble("monthEvolutionPlayTime"));
		lobbyPlayer.monthEvolutionWins = stats.getInt("monthEvolutionWins");
		

		lobbyPlayer.monthCtfKills = stats.getInt("monthCtfKills");
		lobbyPlayer.monthCtfDeaths = stats.getInt("monthCtfDeaths");
		lobbyPlayer.monthCtfFlagsTaken = stats.getInt("monthCtfFlagsTaken");
		lobbyPlayer.monthCtfFlagsSaved = stats.getInt("monthCtfFlagsSaved");
		lobbyPlayer.monthCtfCoinsEarned = stats.getDouble("monthCtfCoinsEarned");
		lobbyPlayer.monthCtfPlayTime = (int) Math.round(stats.getDouble("monthCtfPlayTime"));
		lobbyPlayer.monthCtfWins = stats.getInt("monthCtfWins");
		

		lobbyPlayer.monthGladiatorKills = stats.getInt("monthGladiatorKills");
		lobbyPlayer.monthGladiatorDeaths = stats.getInt("monthGladiatorDeaths");
		lobbyPlayer.monthGladiatorGladiatorsKilled = stats.getInt("monthGladiatorGladiatorsKilled");
		lobbyPlayer.monthGladiatorSlavesKilled = stats.getInt("monthGladiatorSlavesKilled");
		lobbyPlayer.monthGladiatorGladiatorWins = stats.getInt("monthGladiatorGladiatorWins");
		lobbyPlayer.monthGladiatorSlaveWins = stats.getInt("monthGladiatorSlaveWins");
		lobbyPlayer.monthGladiatorCoinsEarned = stats.getDouble("monthGladiatorCoinsEarned");
		lobbyPlayer.monthGladiatorPlayTime = (int) Math.round(stats.getDouble("monthGladiatorPlayTime"));


		lobbyPlayer.monthRageKills = stats.getInt("monthRageKills");
		lobbyPlayer.monthRageDeaths = stats.getInt("monthRageDeaths");
		lobbyPlayer.monthRageBestKillStreak = stats.getInt("monthRageBestKillStreak");
		lobbyPlayer.monthRageTier1Gadgets = stats.getInt("monthRageTier1Gadgets");
		lobbyPlayer.monthRageTier2Gadgets = stats.getInt("monthRageTier2Gadgets");
		lobbyPlayer.monthRageTier3Gadgets = stats.getInt("monthRageTier3Gadgets");
		lobbyPlayer.monthRageDoubleJumps = stats.getInt("monthRageDoubleJumps");
		lobbyPlayer.monthRageCoinsEarned = stats.getDouble("monthRageCoinsEarned");
		lobbyPlayer.monthRagePlayTime = (int) Math.round(stats.getDouble("monthRagePlayTime"));
		lobbyPlayer.monthRageWins = stats.getInt("monthRageWins");


		lobbyPlayer.monthTdmKills = stats.getInt("monthTdmKills");
		lobbyPlayer.monthTdmDeaths = stats.getInt("monthTdmDeaths");
		lobbyPlayer.monthTdmCoinsEarned = stats.getDouble("monthTdmCoinsEarned");
		lobbyPlayer.monthTdmPlayTime = (int) Math.round(stats.getDouble("monthTdmPlayTime"));
		lobbyPlayer.monthTdmWins = stats.getInt("monthTdmWins");
		return lobbyPlayer;
	}



	public List<LobbyPlayer> loadGlobalRankingPlayers(String table, String joinId, SortBy sortBy, Integer page){
		
		List<LobbyPlayer> lobbyPlayers = new ArrayList<LobbyPlayer>();
		
		try{
			
			HCMySQLAPI sql = api.getMySQLAPI();
			
			CachedRowSet stats = sql.query(sql.prepareStatement(selectGlobalRankingNames
					.replace("%table%", table)
					.replace("%join%", joinId)
					.replace("%where%", sortBy.getWhere())
					.replace("%sortBy%", sortBy.getColumnName())
					.replace("%offset%", String.valueOf(5*page))
					)
			);
			
			boolean next = stats.first();
			
			if(!next){
				lobbyPlayers.addAll(Lists.newArrayList(null,null,null,null,null));
				return lobbyPlayers;
			}
			
			while(next){
				LobbyPlayer preLobbyPlayer = new LobbyPlayer(stats.getString("name"));
				preLobbyPlayer.setId(stats.getInt("id"));
				lobbyPlayers.add(loadGlobalStats(preLobbyPlayer.getName(), preLobbyPlayer.getId()));
				next = stats.next();
			}
			
			while(lobbyPlayers.size() < 5){
				lobbyPlayers.add(null);
			}
			
			return lobbyPlayers;
			
		} catch (SQLException e) {
			e.printStackTrace();
			Log.severe("Couldn't read global ranking statistics, table="+table+" joinId="+joinId+" sortBy="+sortBy+" page="+page);
		}
		
		return lobbyPlayers;
	}
	
	public List<LobbyPlayer> loadMonthRankingNames(String table, SortBy sortBy, Integer page, int month, int year){

		List<LobbyPlayer> lobbyPlayers = new ArrayList<LobbyPlayer>();
		
		try{

			HCMySQLAPI sql = api.getMySQLAPI();
			
			CachedRowSet stats = sql.query(sql.prepareStatement(selectMonthRankingNames
					.replace("%table%", table)
					.replace("%where%", sortBy.getWhere())
					.replace("%sortBy%", sortBy.getColumnName())
					.replace("%offset%", String.valueOf(5*page))
					.replace("%month%", String.valueOf(month))
					.replace("%year%", String.valueOf(year))
				)
			);
			
			
			
			boolean next = stats.first();
			
			if(!next){
				lobbyPlayers.addAll(Lists.newArrayList(null,null,null,null,null));
				return lobbyPlayers;
			}

			while(next){
				LobbyPlayer preLobbyPlayer = new LobbyPlayer(stats.getString("name"));
				preLobbyPlayer.setId(stats.getInt("id"));
				lobbyPlayers.add(loadMonthStats(preLobbyPlayer.getName(), preLobbyPlayer.getId(), month, year));
				next = stats.next();
			}
			
			while(lobbyPlayers.size() < 5){
				lobbyPlayers.add(null);
			}
			
			return lobbyPlayers;
		
		} catch (SQLException e) {
			e.printStackTrace();
			Log.severe("Couldn't read month ranking statistics, table="+table+" sortBy="+sortBy+" page="+page+" month="+month+" year="+year);
		}
	
		return lobbyPlayers;
	}



	public Integer[] getPreviousMonth(int month, int year) {
		if(month == 1){
			return new Integer[]{ 12 , year-1};
		}else{
			return new Integer[]{ month-1 , year};
		}
	}
	
	public Integer[] getNextMonth(int month, int year) {
		if(month == 12){
			return new Integer[]{ 1 , year+1};
		}else{
			return new Integer[]{ month+1 , year};
		}
	}
	
	public boolean isMinMonth(int month, int year){
		return month == 12 && year == 2015;
	}
	
	public boolean isMaxMonth(int month, int year){
		LocalDateTime now = LocalDateTime.now();
		return month == now.getMonth().getValue() && year == now.getYear();
	}

	public String monthIntToString(Integer month) {
		return StringUtils.capitalize(new DateFormatSymbols(Locale.FRANCE).getMonths()[month-1]);
	}

}
