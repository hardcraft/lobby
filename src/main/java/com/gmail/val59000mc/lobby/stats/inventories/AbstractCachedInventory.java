package com.gmail.val59000mc.lobby.stats.inventories;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;

import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.spigotutils.Numbers;

public abstract class AbstractCachedInventory{

	protected Inventory inventory;
	protected String name;
	protected String errorMessage;
	private Long createdAt;
	private Long lastAccessedAt;
	protected InventoryType type;
	protected boolean isLoaded;
	protected InventoryStats stats;
	
	public AbstractCachedInventory(InventoryStats stats, String name){
		Long now = System.currentTimeMillis();
		this.name = name;
		this.stats = stats;
		this.createdAt = now;
		this.lastAccessedAt = now;
		this.isLoaded = false;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public abstract Object[] getArgs();
	
	public abstract Inventory buildInventory();
	
	public abstract void loadStats();

	public Long getCreatedAt() {
		return createdAt;
	}

	public Long getLastAccessedAt() {
		return lastAccessedAt;
	}

	public void updateAccessDate() {
		this.lastAccessedAt = System.currentTimeMillis();
	}

	public InventoryType getType() {
		return type;
	}
	
	public boolean isLoaded(){
		return isLoaded;
	}
	
	public void setLoaded(){
		this.isLoaded = true;
	}

	public String getName() {
		return name;
	}
	
	protected String formatTime(int time) {

		int d,h;
		d = (int) time / (60 * 24);
		time -= d * ( 60 * 24);
		h = (int) time / 60;
		time -= h * 60;
		
		
		if(d == 0){
			if(h==0){
				return time+"m";
			}else{
				return h+"h "+time+"m ";		
			}
		}else{
			return d+"j "+h+"h "+time+"m ";
		}
	}

	protected Integer changeAmountIfGame(Game requiredGame, Game actualGame) {
		return requiredGame.equals(actualGame) ? 0 : 1;
	}
	
	protected void addGlassPanes(Map<Integer,Short> positions){
		for(Entry<Integer, Short> entry : positions.entrySet()){
			inventory.addItem(new Item.ItemBuilder(Material.STAINED_GLASS_PANE)
				.withDamage(entry.getValue())
				.withPosition(entry.getKey())
				.withRequireUniqueKey(true)
				.withName("  ")
				.build()
			);
		}
	}
	
	protected double kdRatio(int kills, int deaths){
		if(deaths == 0)
			return kills;
		return Numbers.round((double) kills / (double) deaths, 2);
	}
}
