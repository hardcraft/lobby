package com.gmail.val59000mc.lobby.stats.pages;

import java.util.ArrayList;
import java.util.List;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.spigotutils.Time;

public class GlobalPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();
		
		content.add(s.title);
		content.add(s.all);
		
		content.add(" ");

		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.monthWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthPlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.globalWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalPlayTime));
		return content;
	}

}
