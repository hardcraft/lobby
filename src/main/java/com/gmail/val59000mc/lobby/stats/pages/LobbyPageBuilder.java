package com.gmail.val59000mc.lobby.stats.pages;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

import java.util.ArrayList;
import java.util.List;

public class LobbyPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();
		
		content.add(s.title);
		content.add(s.lobby);
		
		content.add(" ");

		content.add(s.monthStats);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthLobbyPlayTime));

		content.add(s.globalStats);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalLobbyPlayTime));
		return content;
	}

}
