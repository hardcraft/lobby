package com.gmail.val59000mc.lobby.stats.inventories.ranking;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.inventories.GlobalRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.InventoryStrings;
import com.gmail.val59000mc.lobby.stats.inventories.MonthRankingInventory;
import com.gmail.val59000mc.lobby.stats.inventories.SortBy;
import com.google.common.collect.Lists;

import java.util.List;

public class SurvivalRushBuilder extends RankingItemBuilder{

	@Override
	public List<String> buildGlobalLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.globalSurvivalRushKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.globalSurvivalRushDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.globalSurvivalRushKills, lobbyPlayer.globalSurvivalRushDeaths)),
			s.skeletonKilled+lobbyPlayer.globalSurvivalRushSkeletonsKilled,
			s.blocksPlaced+lobbyPlayer.globalSurvivalRushBlocksPlaced,
			s.ironMined+lobbyPlayer.globalSurvivalRushIronMined,
			s.diamondMined+lobbyPlayer.globalSurvivalRushDiamondMined,
			s.goldMined+lobbyPlayer.globalSurvivalRushGoldMined,
			s.gravelMined+lobbyPlayer.globalSurvivalRushGravelMined,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.globalSurvivalRushCoinsEarned),
			boldIfSortedBy(SortBy.WINS, sortBy, s.wins+lobbyPlayer.globalSurvivalRushWins),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.globalSurvivalRushPlayTime))
		);
	}

	@Override
	public List<String> buildMonthLore(InventoryStrings s, LobbyPlayer lobbyPlayer, SortBy sortBy) {
		return Lists.newArrayList(
			boldIfSortedBy(SortBy.KILLS, sortBy, s.kills+lobbyPlayer.monthSurvivalRushKills),
			boldIfSortedBy(SortBy.DEATHS, sortBy, s.deaths+lobbyPlayer.monthSurvivalRushDeaths),
			boldIfSortedBy(SortBy.RATIO, sortBy, s.ratio+kdRatio(lobbyPlayer.monthSurvivalRushKills, lobbyPlayer.monthSurvivalRushDeaths)),
			s.skeletonKilled+lobbyPlayer.monthSurvivalRushSkeletonsKilled,
			s.blocksPlaced+lobbyPlayer.monthSurvivalRushBlocksPlaced,
			s.ironMined+lobbyPlayer.monthSurvivalRushIronMined,
			s.diamondMined+lobbyPlayer.monthSurvivalRushDiamondMined,
			s.goldMined+lobbyPlayer.monthSurvivalRushGoldMined,
			s.gravelMined+lobbyPlayer.monthSurvivalRushGravelMined,
			boldIfSortedBy(SortBy.COINS, sortBy, s.coinsEarned+lobbyPlayer.monthSurvivalRushCoinsEarned),
			boldIfSortedBy(SortBy.WINS, sortBy, s.wins+lobbyPlayer.monthSurvivalRushWins),
			boldIfSortedBy(SortBy.PLAY_TIME, sortBy, s.playTime+formatTime(lobbyPlayer.monthSurvivalRushPlayTime))
		);
	}
	
	@Override
	public void addGlobalFiltersItems(InventoryStrings s, GlobalRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addWinsFilter(s, inventory, Constants.WINS_FILLTER_POSITION);
	}

	@Override
	public void addMonthFiltersItems(InventoryStrings s, MonthRankingInventory inventory) {
		addKillsFilter(s, inventory, Constants.KILLS_FILLTER_POSITION);
		addDeathsFilter(s, inventory, Constants.DEATHS_FILLTER_POSITION);
		addRatioFilter(s, inventory, Constants.RATIO_FILLTER_POSITION);
		addCoinsFilter(s, inventory, Constants.COINS_FILLTER_POSITION);
		addPlayTimeFilter(s, inventory, Constants.PLAY_TIME_FILLTER_POSITION);
		addWinsFilter(s, inventory, Constants.WINS_FILLTER_POSITION);
	}

}
