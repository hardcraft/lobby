package com.gmail.val59000mc.lobby.stats.pages;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

import java.util.ArrayList;
import java.util.List;

public class CtfPageBuilder extends PageBuilder{

	@Override
	public List<String> build(PageStrings s, LobbyPlayer lobbyPlayer){
		List<String> content = new ArrayList<String>();

		content.add(s.title);
		content.add(s.ctf);
		
		content.add(" ");
		
		content.add(s.monthStats);
		content.add(s.kills+"§a"+lobbyPlayer.monthCtfKills);
		content.add(s.deaths+"§a"+lobbyPlayer.monthCtfDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.monthCtfCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.monthCtfWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.monthCtfPlayTime));

		content.add(s.globalStats);
		content.add(s.kills+"§a"+lobbyPlayer.globalCtfKills);
		content.add(s.deaths+"§a"+lobbyPlayer.globalCtfDeaths);
		content.add(s.coinsEarned+"§a"+lobbyPlayer.globalCtfCoinsEarned);
		content.add(s.wins+"§a"+lobbyPlayer.globalCtfWins);
		content.add(s.playTime+"§a"+formatTime(lobbyPlayer.globalCtfPlayTime));
		return content;
	}

}
