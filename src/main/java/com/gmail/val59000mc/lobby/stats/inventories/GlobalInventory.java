package com.gmail.val59000mc.lobby.stats.inventories;

import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.bukkit.DyeColor;
import org.bukkit.Material;

import java.time.LocalDateTime;

public class GlobalInventory extends AbstractPlayerStatsInventory{

	public GlobalInventory(InventoryStats stats, String preLobbyPlayer, Integer preLobbyPlayerId) {
		super(
			stats, 
			"statistics-global-player-"+preLobbyPlayer, 
			preLobbyPlayer,
			preLobbyPlayerId
		);
		this.type = InventoryType.GLOBAL;
	}


	@Override
	public void loadStats() {
		this.lobbyPlayer = stats.loadGlobalStats(preLobbyPlayer, preLobbyPlayerId);
	}
	
	@Override
	public Object[] getArgs(){
		return new Object[]{ preLobbyPlayer };
	}
	
	@Override
	public Inventory buildInventory() {
		InventoryStrings strings = stats.getStrings();
		
		this.inventory = new Inventory(getName(), strings.globalInventory.replace("%player%", preLobbyPlayer), 3);
		
		if(lobbyPlayer == null){
			Item notFound = new Item.ItemBuilder(Material.BARRIER)
				.withName(strings.playerNotFoundName)
				.withLore(Lists.newArrayList(strings.playerNotFoundLore.replace("%player%", preLobbyPlayer)))
				.withPosition(13)
				.build();
			inventory.addItem(notFound);
			
			// glass panes
			addGlassPanes(ImmutableMap.<Integer, Short>builder()
                    .put(0, Constants.WHITE_PANE)
                    .put(1,Constants.GREEN_PANE)
                    .put(2,Constants.WHITE_PANE)
                    .put(3,Constants.GREEN_PANE)
                    .put(4,Constants.WHITE_PANE)
                    .put(5,Constants.GREEN_PANE)
                    .put(6,Constants.WHITE_PANE)
                    .put(7,Constants.GREEN_PANE)
                    .put(8,Constants.WHITE_PANE)
                    .put(9,Constants.GREEN_PANE)
                    .put(10,Constants.WHITE_PANE)
                    .put(11,Constants.GREEN_PANE)
                    .put(12,Constants.WHITE_PANE)
                    .put(14,Constants.WHITE_PANE)
                    .put(15,Constants.GREEN_PANE)
                    .put(16,Constants.WHITE_PANE)
                    .put(17,Constants.GREEN_PANE)
                    .put(18, Constants.WHITE_PANE)
                    .put(19,Constants.GREEN_PANE)
                    .put(20,Constants.WHITE_PANE)
                    .put(21,Constants.GREEN_PANE)
                    .put(22,Constants.WHITE_PANE)
                    .put(23,Constants.GREEN_PANE)
                    .put(24,Constants.WHITE_PANE)
                    .put(25,Constants.GREEN_PANE)
				.build()
			);
		}else{
			
			Item global = new Item.ItemBuilder(Material.SKULL_ITEM)
					.withDamage((short) 3)
					.withPlayerSkullName(lobbyPlayer.getName())
					.withName(strings.all)
					.withLore(Lists.newArrayList(
						strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.allStripped),
						strings.kills+lobbyPlayer.globalKills,
						strings.deaths+lobbyPlayer.globalDeaths,
						strings.ratio+kdRatio(lobbyPlayer.globalKills,lobbyPlayer.globalDeaths),
						strings.coinsEarned+lobbyPlayer.globalCoinsEarned,
						strings.wins+lobbyPlayer.globalWins,
						strings.playTime+formatTime(lobbyPlayer.globalPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(3)
					.build();
			global.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.GLOBAL, 0, SortBy.KILLS)));
			inventory.addItem(global);
			
			Item lobby = new Item.ItemBuilder(Material.WATCH)
					.withName(strings.lobby)
					.withLore(Lists.newArrayList(
						strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.lobbyStripped),
						strings.playTime+formatTime(lobbyPlayer.globalLobbyPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(5)
					.build();
			lobby.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.LOBBY, 0, SortBy.PLAY_TIME)));
			inventory.addItem(lobby);
			
			Item killskill = new Item.ItemBuilder(Material.DIAMOND_CHESTPLATE)
					.withName(strings.killSkill)
					.withLore(Lists.newArrayList(
						strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.killSkillStripped),
						strings.kills+lobbyPlayer.globalKillSkillKills,
						strings.deaths+lobbyPlayer.globalKillSkillDeaths,
						strings.ratio+kdRatio(lobbyPlayer.globalKillSkillKills, lobbyPlayer.globalKillSkillDeaths),
						strings.experienceLooted+lobbyPlayer.globalKillSkillExperienceLooted,
						strings.lapisLooted+lobbyPlayer.globalKillSkillLapisLooted,
						strings.potionLooted+lobbyPlayer.globalKillSkillPotionLooted,
						strings.ironLooted+lobbyPlayer.globalKillSkillIronLooted,
						strings.diamondLooted+lobbyPlayer.globalKillSkillDiamondLooted,
						strings.blazeLooted+lobbyPlayer.globalKillSkillBlazeLooted,
						strings.headLooted+lobbyPlayer.globalKillSkillHeadLooted,
						strings.grenadeLooted+lobbyPlayer.globalKillSkillGrenadeLooted,
						strings.tntLooted+lobbyPlayer.globalKillSkillTntLooted,
						strings.cannonLooted+lobbyPlayer.globalKillSkillCannonLooted,
						strings.supplyOpened+lobbyPlayer.globalKillSkillSupplyOpened,
						strings.coinsEarned+lobbyPlayer.globalKillSkillCoinsEarned,
						strings.playTime+formatTime(lobbyPlayer.globalKillSkillPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(10)
					.build();
			killskill.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.KILL_SKILL, 0, SortBy.KILLS)));
			inventory.addItem(killskill);
			
			Item survivalRush = new Item.ItemBuilder(Material.BED)
					.withName(strings.survivalRush)
					.withLore(Lists.newArrayList(
						strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.survivalRushStripped),
						strings.kills+lobbyPlayer.globalSurvivalRushKills,
						strings.deaths+lobbyPlayer.globalSurvivalRushDeaths,
						strings.ratio+kdRatio(lobbyPlayer.globalSurvivalRushKills, lobbyPlayer.globalSurvivalRushDeaths),
						strings.skeletonKilled+lobbyPlayer.globalSurvivalRushSkeletonsKilled,
						strings.blocksPlaced+lobbyPlayer.globalSurvivalRushBlocksPlaced,
						strings.ironMined+lobbyPlayer.globalSurvivalRushIronMined,
						strings.diamondMined+lobbyPlayer.globalSurvivalRushDiamondMined,
						strings.goldMined+lobbyPlayer.globalSurvivalRushGoldMined,
						strings.gravelMined+lobbyPlayer.globalSurvivalRushGravelMined,
						strings.coinsEarned+lobbyPlayer.globalSurvivalRushCoinsEarned,
						strings.wins+lobbyPlayer.globalSurvivalRushWins,
						strings.playTime+formatTime(lobbyPlayer.globalSurvivalRushPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(11)
					.build();
			survivalRush.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.SURVIVAL_RUSH, 0, SortBy.KILLS)));
			inventory.addItem(survivalRush);

			Item rage = new Item.ItemBuilder(Material.IRON_SWORD)
					.withName(strings.rage)
					.withLore(Lists.newArrayList(
							strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.rageStripped),
							strings.kills+lobbyPlayer.globalRageKills,
							strings.deaths+lobbyPlayer.globalRageDeaths,
							strings.ratio+kdRatio(lobbyPlayer.globalRageKills, lobbyPlayer.globalRageDeaths),
							strings.rageBestKillStreak+lobbyPlayer.globalRageBestKillStreak,
							strings.rageTiers1Gagdets+lobbyPlayer.globalRageTier1Gadgets,
							strings.rageTiers2Gagdets+lobbyPlayer.globalRageTier2Gadgets,
							strings.rageTiers3Gagdets+lobbyPlayer.globalRageTier3Gadgets,
							strings.rageDoubleJumps+lobbyPlayer.globalRageDoubleJumps,
							strings.coinsEarned+lobbyPlayer.globalRageCoinsEarned,
							strings.wins+lobbyPlayer.globalRageWins,
							strings.playTime+formatTime(lobbyPlayer.globalRagePlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(12)
					.build();
			rage.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.RAGE, 0, SortBy.KILLS)));
			inventory.addItem(rage);
			
			Item evolution = new Item.ItemBuilder(Material.NETHER_STAR)
					.withName(strings.evolution)
					.withLore(Lists.newArrayList(
						strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.evolutionStripped),
						strings.kills+lobbyPlayer.globalEvolutionKills,
						strings.deaths+lobbyPlayer.globalEvolutionDeaths,
						strings.ratio+kdRatio(lobbyPlayer.globalEvolutionKills, lobbyPlayer.globalEvolutionDeaths),
						strings.starsEarned+lobbyPlayer.globalEvolutionStarsEarned,
						strings.coinsEarned+lobbyPlayer.globalEvolutionCoinsEarned,
						strings.wins+lobbyPlayer.globalEvolutionWins,
						strings.playTime+formatTime(lobbyPlayer.globalEvolutionPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(14)
					.build();
			evolution.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.EVOLUTION, 0, SortBy.KILLS)));
			inventory.addItem(evolution);
			
			Item ctf = new Item.ItemBuilder(Material.BANNER)
					.withName(strings.ctf)
					.withFlagColor(DyeColor.BLUE)
					.withHideFlags(true)
					.withLore(Lists.newArrayList(
						strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.ctfStripped),
						strings.kills+lobbyPlayer.globalCtfKills,
						strings.deaths+lobbyPlayer.globalCtfDeaths,
						strings.ratio+kdRatio(lobbyPlayer.globalCtfKills, lobbyPlayer.globalCtfDeaths),
						strings.flagsTaken+lobbyPlayer.globalCtfFlagsTaken,
						strings.flagsSaved+lobbyPlayer.globalCtfFlagsSaved,
						strings.coinsEarned+lobbyPlayer.globalCtfCoinsEarned,
						strings.wins+lobbyPlayer.globalCtfWins,
						strings.playTime+formatTime(lobbyPlayer.globalCtfPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(15)
					.build();
			ctf.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.CTF, 0, SortBy.KILLS)));
			inventory.addItem(ctf);
			
			Item tdm = new Item.ItemBuilder(Material.IRON_CHESTPLATE)
					.withName(strings.tdm)
					.withHideFlags(true)
					.withLore(Lists.newArrayList(
						strings.globalItemHeader.replace("%player%", lobbyPlayer.getName()).replace("%game%", strings.tdmStripped),
						strings.kills+lobbyPlayer.globalTdmKills,
						strings.deaths+lobbyPlayer.globalTdmDeaths,
						strings.ratio+kdRatio(lobbyPlayer.globalTdmKills, lobbyPlayer.globalTdmDeaths),
						strings.coinsEarned+lobbyPlayer.globalTdmCoinsEarned,
                        strings.wins+lobbyPlayer.globalTdmWins,
						strings.playTime+formatTime(lobbyPlayer.globalTdmPlayTime)
					))
					.withRequireUniqueKey(true)
					.withPosition(16)
					.build();
            tdm.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.RANKING_GLOBAL, Game.TDM, 0, SortBy.KILLS)));
			inventory.addItem(tdm);


			// months
			
			Item noPreviousMonth = new Item.ItemBuilder(Material.STONE_BUTTON)
					.withName("  ")
					.withRequireUniqueKey(true)
					.withPosition(21)
					.build();				
			inventory.addItem(noPreviousMonth);
			
			LocalDateTime now = LocalDateTime.now();
			int currentMonthNb = now.getMonth().getValue();
			int currentYearNb = now.getYear();
			Item currentMonth = new Item.ItemBuilder(Material.SULPHUR)
					.withName(strings.globalSelector)
					.withLore(Lists.newArrayList(strings.globalSelectorLore))
					.withRequireUniqueKey(true)
					.withPosition(22)
					.build();		
			currentMonth.setActions(Lists.newArrayList(new OpenStatisticsInventoryAction(stats, InventoryType.MONTH, lobbyPlayer.getName(), lobbyPlayer.getId(), currentMonthNb, currentYearNb)));		
			inventory.addItem(currentMonth);
			
			Item noNextMonth = new Item.ItemBuilder(Material.STONE_BUTTON)
					.withName("  ")
					.withRequireUniqueKey(true)
					.withPosition(23)
					.build();				
			inventory.addItem(noNextMonth);
			
			// glass panes
			addGlassPanes(ImmutableMap.<Integer, Short>builder()
				.put(0, Constants.WHITE_PANE)
				.put(1,Constants.GREEN_PANE)
				.put(2,Constants.WHITE_PANE)
				.put(6,Constants.WHITE_PANE)
				.put(7,Constants.GREEN_PANE)
				.put(8,Constants.WHITE_PANE)
				.put(9,Constants.GREEN_PANE)
				.put(17,Constants.GREEN_PANE)
				.put(18,Constants.WHITE_PANE)
				.put(19,Constants.GREEN_PANE)
				.put(20,Constants.WHITE_PANE)
				.put(24,Constants.WHITE_PANE)
				.put(25,Constants.GREEN_PANE)
				.build()
			);
			
		}
		
		/// back
		
		Item previous = new Item.ItemBuilder(Material.RAW_FISH)
				.withDamage((short) 3)
				.withName(strings.back)
				.withLore(Lists.newArrayList(strings.backToMain))
				.withPosition(26)
				.build();
		previous.setActions(Lists.newArrayList(new OpenInventoryAction(Constants.STATISTICS_MAIN_INVENTORY)));
		inventory.addItem(previous);
		
		return inventory;
	}

}
