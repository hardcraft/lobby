package com.gmail.val59000mc.lobby.stats.inventories;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.lobby.stats.InventoryStats;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Sounds;

public class OpenStatisticsInventoryAction extends Action{

	private InventoryType type;
	private Object[] args;
	private InventoryStats stats;
	
	public OpenStatisticsInventoryAction(InventoryStats stats, InventoryType type, Object... args) {
		super();
		this.stats = stats;
		this.type = type;
		this.args = args;
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		
		Object[] invArgs = new Object[args.length];
		System.arraycopy(args, 0, invArgs, 0, args.length);
		
		// replace placeholder (if found) name by the name of the player who clicked
		if(type.equals(InventoryType.GLOBAL) || type.equals(InventoryType.MONTH)){
			if(invArgs[0].equals("%player%") && ((Integer) invArgs[1]) == 0){

				HCPlayer hcPlayer = stats.getApi().getPlayersManagerAPI().getHCPlayer(player);
				if(hcPlayer == null || hcPlayer.getId() == 0){
					executeNextAction(player, sigPlayer, false);
					return;
				}
				
				invArgs[0] = player.getName();
				invArgs[1] = hcPlayer.getId();
			}
		}
		
		AbstractCachedInventory inv = stats.addOrGetFromCache(type, invArgs);
		Sounds.play(player, Sound.NOTE_STICKS, 0.3f, 2);
		
		if(inv.isLoaded()){
			openInventory(inv, player, sigPlayer);
		}else{
			stats.getApi().buildTask("wait loading '"+inv.getName()+"' for "+player.getName(), new HCTask() {
				
				@Override
				public void run() {
					if(inv.isLoaded()){
						openInventory(inv, player, sigPlayer);
						scheduler.stop();
					}
				}
			})
			.withInterval(5)
			.withDelay(5)
			.build()
			.start();
		}
		
	}
	
	private void openInventory(AbstractCachedInventory inv, Player player, SigPlayer sigPlayer){
		inv.getInventory().openInventory(player, sigPlayer);
		inv.updateAccessDate();
		executeNextAction(player, sigPlayer, true);
	}
	
}
