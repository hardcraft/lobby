package com.gmail.val59000mc.lobby.stats;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.api.HCPlayersManagerAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.lobby.stats.pages.Page;
import com.gmail.val59000mc.lobby.stats.pages.PageBuilder;
import com.gmail.val59000mc.lobby.stats.pages.PageStrings;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ScoreboardStats {

	private HCGameAPI api;

	private List<String> defaultLoadingPage;
	private Page page;
	private PageStrings strings;
	private Map<LobbyPlayer,List<String>> cachedData;
	
	private String selectStats;
	
	public ScoreboardStats(HCGameAPI api) {
		this.api = api;
		
		this.defaultLoadingPage = api.getStringsAPI().getList("lobby.scoreboard.loading").toStringList();
		this.page = Page.GLOBAL;
		this.cachedData = new HashMap<LobbyPlayer,List<String>>();
		
		this.strings = new PageStrings(api);
		this.strings.setup();
		
		HCMySQLAPI sql = api.getMySQLAPI();
		if(sql.isEnabled()){
			selectStats = sql.readQueryFromResource(api.getPlugin(),"sql/select_scoreboard_stats.sql");
		}
		
	}
	public void addPlayer(LobbyPlayer lobbyPlayer) {
		if(!cachedData.containsKey(lobbyPlayer)){
			cachedData.put(lobbyPlayer, null);
		}
	}
	
	public void nextPage() {
		this.page = page.next();
	}
	
	public Page getPage() {
		return page;
	}

	public List<String> getScoreboardContent(LobbyPlayer lobbyPlayer) {
		if(cachedData.containsKey(lobbyPlayer)){
			List<String> content = cachedData.get(lobbyPlayer);
			if(content == null)
				return defaultLoadingPage;
			else
				return content;
		}else{
			// if not available, fetch data
			return defaultLoadingPage;
		}
	}

	public void cleanOldScoreboardCache(){
		HCPlayersManagerAPI pm = api.getPlayersManagerAPI();
		
		// Remove offline players
		synchronized (cachedData) {

			Iterator<Map.Entry<LobbyPlayer,List<String>>> it = cachedData.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry<LobbyPlayer,List<String>> entry = it.next();
				LobbyPlayer lobbyPlayer = entry.getKey();
				if(pm.getHCPlayer(lobbyPlayer) == null){
					it.remove();
				}
			}
			
		}
	}
	

	public void refreshPlayersStats(){
		for(HCPlayer hcPlayer : api.getPlayersManagerAPI().getPlayers(null, null)){
			refreshPlayerStats((LobbyPlayer) hcPlayer);
		}
	}

	public void refreshPlayerStats(LobbyPlayer hcPlayer) {

		if(api.getMySQLAPI().isEnabled() && hcPlayer.getId() != 0){
					
			HCMySQLAPI sql = api.getMySQLAPI();

			LobbyPlayer lobbyPlayer = (LobbyPlayer) hcPlayer;
			
			
			try {
				CachedRowSet stats = sql.query(sql.prepareStatement(selectStats.replace("%month%", sql.getMonth()).replace("%year%", sql.getYear()), String.valueOf(lobbyPlayer.getId())));
				
				if(stats.first()){

					lobbyPlayer.globalKills = stats.getInt("globalKills");
					lobbyPlayer.globalDeaths = stats.getInt("globalDeaths");
					lobbyPlayer.globalPlayTime = stats.getInt("globalPlayTime");
					lobbyPlayer.globalCoinsEarned = stats.getDouble("globalCoinsEarned");
					lobbyPlayer.globalWins = stats.getInt("globalWins");

					lobbyPlayer.monthKills = stats.getInt("monthKills");
					lobbyPlayer.monthDeaths = stats.getInt("monthDeaths");
					lobbyPlayer.monthPlayTime = stats.getInt("monthPlayTime");
					lobbyPlayer.monthCoinsEarned = stats.getDouble("monthCoinsEarned");
					lobbyPlayer.monthWins = stats.getInt("monthWins");
					
					lobbyPlayer.globalLobbyPlayTime = stats.getInt("globalLobbyPlayTime");
					
					lobbyPlayer.monthLobbyPlayTime = stats.getInt("monthLobbyPlayTime");

					lobbyPlayer.globalKillSkillKills = stats.getInt("globalKillSkillKills");
					lobbyPlayer.globalKillSkillDeaths = stats.getInt("globalKillSkillDeaths");
					lobbyPlayer.globalKillSkillPlayTime = stats.getInt("globalKillSkillPlayTime");
					lobbyPlayer.globalKillSkillCoinsEarned = stats.getDouble("globalKillSkillCoinsEarned");
					
					lobbyPlayer.monthKillSkillKills = stats.getInt("monthKillSkillKills");
					lobbyPlayer.monthKillSkillDeaths = stats.getInt("monthKillSkillDeaths");
					lobbyPlayer.monthKillSkillPlayTime = stats.getInt("monthKillSkillPlayTime");
					lobbyPlayer.monthKillSkillCoinsEarned = stats.getDouble("monthKillSkillCoinsEarned");
					

					lobbyPlayer.globalSurvivalRushKills = stats.getInt("globalSurvivalRushKills");
					lobbyPlayer.globalSurvivalRushDeaths = stats.getInt("globalSurvivalRushDeaths");
					lobbyPlayer.globalSurvivalRushPlayTime = stats.getInt("globalSurvivalRushPlayTime");
					lobbyPlayer.globalSurvivalRushCoinsEarned = stats.getDouble("globalSurvivalRushCoinsEarned");
					lobbyPlayer.globalSurvivalRushWins = stats.getInt("globalSurvivalRushWins");
					
					lobbyPlayer.monthSurvivalRushKills = stats.getInt("monthSurvivalRushKills");
					lobbyPlayer.monthSurvivalRushDeaths = stats.getInt("monthSurvivalRushDeaths");
					lobbyPlayer.monthSurvivalRushPlayTime = stats.getInt("monthSurvivalRushPlayTime");
					lobbyPlayer.monthSurvivalRushCoinsEarned = stats.getDouble("monthSurvivalRushCoinsEarned");
					lobbyPlayer.monthSurvivalRushWins = stats.getInt("monthSurvivalRushWins");
					

					lobbyPlayer.globalEvolutionKills = stats.getInt("globalEvolutionKills");
					lobbyPlayer.globalEvolutionDeaths = stats.getInt("globalEvolutionDeaths");
					lobbyPlayer.globalEvolutionPlayTime = stats.getInt("globalEvolutionPlayTime");
					lobbyPlayer.globalEvolutionCoinsEarned = stats.getDouble("globalEvolutionCoinsEarned");
					lobbyPlayer.globalEvolutionWins = stats.getInt("globalEvolutionWins");
					
					lobbyPlayer.monthEvolutionKills = stats.getInt("monthEvolutionKills");
					lobbyPlayer.monthEvolutionDeaths = stats.getInt("monthEvolutionDeaths");
					lobbyPlayer.monthEvolutionPlayTime = stats.getInt("monthEvolutionPlayTime");
					lobbyPlayer.monthEvolutionCoinsEarned = stats.getDouble("monthEvolutionCoinsEarned");
					lobbyPlayer.monthEvolutionWins = stats.getInt("monthEvolutionWins");
					

					lobbyPlayer.globalCtfKills = stats.getInt("globalCtfKills");
					lobbyPlayer.globalCtfDeaths = stats.getInt("globalCtfDeaths");
					lobbyPlayer.globalCtfPlayTime = stats.getInt("globalCtfPlayTime");
					lobbyPlayer.globalCtfCoinsEarned = stats.getDouble("globalCtfCoinsEarned");
					lobbyPlayer.globalCtfWins = stats.getInt("globalCtfWins");
					
					lobbyPlayer.monthCtfKills = stats.getInt("monthCtfKills");
					lobbyPlayer.monthCtfDeaths = stats.getInt("monthCtfDeaths");
					lobbyPlayer.monthCtfPlayTime = stats.getInt("monthCtfPlayTime");
					lobbyPlayer.monthCtfCoinsEarned = stats.getDouble("monthCtfCoinsEarned");
					lobbyPlayer.monthCtfWins = stats.getInt("monthCtfWins");
					

					lobbyPlayer.globalGladiatorKills = stats.getInt("globalGladiatorKills");
					lobbyPlayer.globalGladiatorDeaths = stats.getInt("globalGladiatorDeaths");
					lobbyPlayer.globalGladiatorPlayTime = stats.getInt("globalGladiatorPlayTime");
					lobbyPlayer.globalGladiatorCoinsEarned = stats.getDouble("globalGladiatorCoinsEarned");
					
					lobbyPlayer.monthGladiatorKills = stats.getInt("monthGladiatorKills");
					lobbyPlayer.monthGladiatorDeaths = stats.getInt("monthGladiatorDeaths");
					lobbyPlayer.monthGladiatorPlayTime = stats.getInt("monthGladiatorPlayTime");
					lobbyPlayer.monthGladiatorCoinsEarned = stats.getDouble("monthGladiatorCoinsEarned");


					lobbyPlayer.globalRageKills = stats.getInt("globalRageKills");
					lobbyPlayer.globalRageDeaths = stats.getInt("globalRageDeaths");
					lobbyPlayer.globalRagePlayTime = stats.getInt("globalRagePlayTime");
					lobbyPlayer.globalRageCoinsEarned = stats.getDouble("globalRageCoinsEarned");
					lobbyPlayer.globalRageWins = stats.getInt("globalRageWins");

					lobbyPlayer.monthRageKills = stats.getInt("monthRageKills");
					lobbyPlayer.monthRageDeaths = stats.getInt("monthRageDeaths");
					lobbyPlayer.monthRagePlayTime = stats.getInt("monthRagePlayTime");
					lobbyPlayer.monthRageCoinsEarned = stats.getDouble("monthRageCoinsEarned");
					lobbyPlayer.monthRageWins = stats.getInt("monthRageWins");


					lobbyPlayer.globalTdmKills = stats.getInt("globalTdmKills");
					lobbyPlayer.globalTdmDeaths = stats.getInt("globalTdmDeaths");
					lobbyPlayer.globalTdmPlayTime = stats.getInt("globalTdmPlayTime");
					lobbyPlayer.globalTdmCoinsEarned = stats.getDouble("globalTdmCoinsEarned");
					lobbyPlayer.globalTdmWins = stats.getInt("globalTdmWins");

					lobbyPlayer.monthTdmKills = stats.getInt("monthTdmKills");
					lobbyPlayer.monthTdmDeaths = stats.getInt("monthTdmDeaths");
					lobbyPlayer.monthTdmPlayTime = stats.getInt("monthTdmPlayTime");
					lobbyPlayer.monthTdmCoinsEarned = stats.getDouble("monthTdmCoinsEarned");
					lobbyPlayer.monthTdmWins = stats.getInt("monthTdmWins");
				}

			} catch (SQLException e) {
				e.printStackTrace();
				Log.severe("Couldn't read statistics for player "+lobbyPlayer.getName()+" Read error above.");
			}
		}
	}
	
	public void buildPages() {
		synchronized (cachedData) {
			PageBuilder builder = page.getPageBuilder();
			for(Entry<LobbyPlayer,List<String>> entry : cachedData.entrySet()){
				entry.setValue(builder.build(strings, entry.getKey()));
			}
		}
	}
	
	public void buildPage(LobbyPlayer lobbyPlayer) {
		PageBuilder builder = page.getPageBuilder();
		synchronized (cachedData) {
			if(cachedData.containsKey(lobbyPlayer)){
				cachedData.put(lobbyPlayer, builder.build(strings, lobbyPlayer));
			}
		}
	}
	public boolean isPlayerCached(LobbyPlayer lobbyPlayer) {
		return cachedData.containsKey(lobbyPlayer);
	}
}
