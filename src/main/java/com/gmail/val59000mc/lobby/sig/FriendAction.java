package com.gmail.val59000mc.lobby.sig;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

import de.simonsator.partyandfriendsgui.api.PartyFriendsAPI;

public class FriendAction extends Action{

	private JavaPlugin plugin;
	
	public FriendAction(JavaPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			
			@Override
			public void run() {
				PartyFriendsAPI.openMainInventory(player);
			}
		}, 2);
		
		executeNextAction(player, sigPlayer, true);
	}

}
