package com.gmail.val59000mc.lobby.sig;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class FriendActionParser extends ActionParser{

	private JavaPlugin plugin;
	
	public FriendActionParser(JavaPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public String getType() {
		return "open-friend-inventory";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		return new FriendAction(plugin);
	}

	

}
