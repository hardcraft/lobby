package com.gmail.val59000mc.lobby.perks;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

public class Perk {
	
	private String name;
	private PerkGame game;
	private PerkName perkName;
	private Perk requiredPerk;
	private double price;
	private Object value;
	private PerkChecker checker;
	
	private Perk(Builder builder) {
		super();
		this.name = "perk";
		this.game = builder.game;
		this.perkName =builder.perkName;
		this.requiredPerk = builder.requiredPerk;
		this.price = builder.price;
		this.value = builder.value;
		this.checker = builder.checker;
	}

	public PerkGame getGame() {
		return game;
	}
	
	public PerkName getPerkName() {
		return perkName;
	}

	public Perk getRequiredPerk() {
		return requiredPerk;
	}

	public Object getValue() {
		return value;
	}

	public Object getRoundedValue() {
		if(Double.class.isInstance(getValue())){
			return (int) Math.round((double) getValue());
		}
		
		return getValue();
	}

	public PerkChecker getChecker() {
		return checker;
	}

	public double getPrice() {
		return price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRoundedPrice() {
		return (int) Math.round(getPrice());
	}
	
	public boolean hasUnlockedPerk(LobbyPlayer lobbyPlayer){
		return checker.hasPerkLevel(lobbyPlayer, getValue());
	}

	public boolean hasRequiredPerk(LobbyPlayer lobbyPlayer){
		if(requiredPerk == null)
			return true;
		else
			return checker.hasPerkLevel(lobbyPlayer, requiredPerk.getValue());
	}
	
	public static class Builder {
		
		private PerkGame game;
		private PerkName perkName;
		private Perk requiredPerk;
		private double price;
		private Object value;
		private PerkChecker checker;

		public Builder(PerkGame game, PerkName perkName, PerkChecker checker) {
			this.game = game;
			this.perkName = perkName;
			this.checker = checker;
			this.price = 0;
			this.value = 0;
			this.requiredPerk = null;
		}

		public Builder setValue(Object value) {
			this.value = value;
			return this;
		}

		public Builder setPrice(double price) {
			this.price = price;
			return this;
		}

		public Builder setRequiredPerk(Perk requiredPerk) {
			this.requiredPerk = requiredPerk;
			return this;
		}

		public Perk build() {
			return new Perk(this);
		}

	}
	
	
	
}
