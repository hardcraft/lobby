package com.gmail.val59000mc.lobby.perks;

public enum PerkName {
	
	KILL_SKILL_EXPERIENCE_AMOUNT("experience_amount"),
	KILL_SKILL_LAPIS_CHANCE("lapis_chance"),
	KILL_SKILL_POTION_CHANCE("potion_chance"),
	KILL_SKILL_IRON_CHANCE("iron_chance"),
	KILL_SKILL_DIAMOND_CHANCE("diamond_chance"),
	KILL_SKILL_BLAZE_CHANCE("blaze_chance"),
	KILL_SKILL_SUPPLY_AMOUNT("supply_amount"),
	KILL_SKILL_SUPPLY_LEVEL("supply_level"),
	KILL_SKILL_ENDERCHEST_ROWS("enderchest_rows"),

	EVOLUTION_STAR("star"),
	
	CTF_TNT_POWER("tnt_power"),
	CTF_TNT_PROTECTION("tnt_protection"),
	CTF_TNT_AMOUNT("tnt_amount"),
	
	GLADIATOR_GLADIATOR_HALF_HEARTS("gladiator_half_hearts"),
	GLADIATOR_SLAVE_BOW_CHANCE("slave_bow_chance"),
	GLADIATOR_SLAVE_EXTRA_CHANCE("slave_extra_chance"),
	GLADIATOR_SLAVE_POTION_CHANCE("slave_potion_chance"),
	GLADIATOR_ENCHANT_CHANCE("enchant_chance"),
	
	SURVIVAL_RUSH_IRON_PICKAXE("iron_pickaxe"),
	SURVIVAL_RUSH_LOOTING_SWORD("looting_sword"),
	SURVIVAL_RUSH_IRON_LOOT_CHANCE("iron_loot_chance"),
	SURVIVAL_RUSH_SKELETONS_PER_TNT("skeletons_per_tnt"),
	SURVIVAL_RUSH_NBR_BLOCKS_RESPAWN("nbr_blocks_respawn"),

	RAGE_ARROWS_PER_SPAWN("arrows_per_spawn"),
	RAGE_XP_REGAIN_SPEED("xp_regain_speed"),
	RAGE_GADGETS_LEVEL("gagdets_level");
	
	private String column;
	
	private PerkName(String table){
		this.column = table;
	}

	public String getColumn() {
		return column;
	}
	
}
