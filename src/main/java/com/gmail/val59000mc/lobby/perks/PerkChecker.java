package com.gmail.val59000mc.lobby.perks;

import com.gmail.val59000mc.lobby.players.LobbyPlayer;

public interface  PerkChecker{

	public abstract boolean hasPerkLevel(LobbyPlayer lobbyPlayer, Object value);
}