package com.gmail.val59000mc.lobby.perks;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.lobby.perks.Perk.Builder;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.google.common.collect.Lists;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PerksManager {

	private HCGameAPI api;
	private PerksStrings s;
	
	private String selectPlayerPerksSQL;
	private String updatePlayerPerkSQL;

	public PerksManager(HCGameAPI api, PerksStrings s) {
		this.api = api;
		this.s = s;
		
		if(api.getMySQLAPI().isEnabled()){
			this.selectPlayerPerksSQL = api.getMySQLAPI().readQueryFromResource(api.getPlugin(), "sql/perks/select_player_perks.sql");
			this.updatePlayerPerkSQL = api.getMySQLAPI().readQueryFromResource(api.getPlugin(), "sql/perks/update_one_player_perk.sql");
		}
	}

	public List<Inventory> buildKillSkillInventories() {

		// experience
		Inventory experienceShop = new Inventory(Constants.SHOP_KILL_SKILL_EXPERIENCE, s.killSkillExperienceTitle, 2);
		Perk.Builder experiencePerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_EXPERIENCE_AMOUNT,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillExperienceAmount >= (int) value
		);
		
		experienceShop.addItem(buildBalanceItem(0));
		experienceShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(experienceShop, s.killSkillExperience, s.killSkillExperienceLore, 
				Lists.newArrayList(Material.EXP_BOTTLE), 
				Lists.newArrayList(0), 
				Lists.newArrayList(1,2,3,4,5,6,7,8,9),
				Lists.newArrayList(9,10,11,12,13,14,15,16,17),
				buildPerksChain(experiencePerkBuilder,
						Lists.newArrayList(0, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000),
						Lists.newArrayList(1,2,3,4,5,6,7,8,9)
				)
		);

		// lapis
		Inventory lapisShop = new Inventory(Constants.SHOP_KILL_SKILL_LAPIS, s.killSkillLapisTitle, 2);
		Perk.Builder lapisPerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_LAPIS_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillLapisChance >= (int) value
		);
		
		lapisShop.addItem(buildBalanceItem(0));
		lapisShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(lapisShop, s.killSkillLapis, s.killSkillLapisLore, 
				Lists.newArrayList(Material.INK_SACK), 
				Lists.newArrayList(4), 
				Lists.newArrayList(5,7,9,11,13,15,17,19,21),
				Lists.newArrayList(9,10,11,12,13,14,15,16,17),
				buildPerksChain(lapisPerkBuilder,
						Lists.newArrayList(0, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000),
						Lists.newArrayList(5,7,9,11,13,15,17,19,21)
				)
		);

		// potion
		Inventory potionShop = new Inventory(Constants.SHOP_KILL_SKILL_POTION, s.killSkillPotionTitle, 2);
		Perk.Builder potionPerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_POTION_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillPotionChance >= (int) value
		);
		
		potionShop.addItem(buildBalanceItem(0));
		potionShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(potionShop, s.killSkillPotion, s.killSkillPotionLore, 
				Lists.newArrayList(Material.POTION), 
				Lists.newArrayList(8229), 
				Lists.newArrayList(20,25,30,35,40,45,50,55,60),
				Lists.newArrayList(9,10,11,12,13,14,15,16,17),
				buildPerksChain(potionPerkBuilder,
						Lists.newArrayList(0, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000),
						Lists.newArrayList(20,25,30,35,40,45,50,55,60)
				)
		);

		// iron
		Inventory ironShop = new Inventory(Constants.SHOP_KILL_SKILL_IRON, s.killSkillIronTitle, 2);
		Perk.Builder ironPerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_IRON_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillIronChance >= (int) value
		);
		
		ironShop.addItem(buildBalanceItem(0));
		ironShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(ironShop, s.killSkillIron, s.killSkillIronLore, 
				Lists.newArrayList(Material.IRON_INGOT), 
				Lists.newArrayList(0), 
				Lists.newArrayList(5,10,15,20,25,30,35,40,45),
				Lists.newArrayList(9,10,11,12,13,14,15,16,17),
				buildPerksChain(ironPerkBuilder,
						Lists.newArrayList(0, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000),
						Lists.newArrayList(5,10,15,20,25,30,35,40,45)
				)
		);

		// diamond
		Inventory diamondShop = new Inventory(Constants.SHOP_KILL_SKILL_DIAMOND, s.killSkillDiamondTitle, 2);
		Perk.Builder diamondPerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_DIAMOND_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillDiamondChance >= (int) value
		);
		
		diamondShop.addItem(buildBalanceItem(0));
		diamondShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(diamondShop, s.killSkillDiamond, s.killSkillDiamondLore, 
				Lists.newArrayList(Material.DIAMOND), 
				Lists.newArrayList(0), 
				Lists.newArrayList(5,7,9,11,13,15,17,19,21),
				Lists.newArrayList(9,10,11,12,13,14,15,16,17),
				buildPerksChain(diamondPerkBuilder,
						Lists.newArrayList(0, 1000, 2000, 4000, 6000, 8000, 10000, 12000, 14000),
						Lists.newArrayList(5,7,9,11,13,15,17,19,21)
				)
		);

		// blaze
		Inventory blazeShop = new Inventory(Constants.SHOP_KILL_SKILL_BLAZE, s.killSkillBlazeTitle, 1);
		Perk.Builder blazePerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_BLAZE_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillBlazeChance >= (int) value
		);
		
		blazeShop.addItem(buildBalanceItem(0));
		blazeShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(blazeShop, s.killSkillBlaze, s.killSkillBlazeLore, 
				Lists.newArrayList(Material.BLAZE_POWDER), 
				Lists.newArrayList(0), 
				Lists.newArrayList(6,12,18,24,30,36,42),
				Lists.newArrayList(1,2,3,4,5,6,7),
				buildPerksChain(blazePerkBuilder,
						Lists.newArrayList(0, 1000, 2000, 4000, 6000, 8000, 10000),
						Lists.newArrayList(6,12,18,24,30,36,42)
				)
		);

		// supply-amount
		Inventory supplyAmountShop = new Inventory(Constants.SHOP_KILL_SKILL_SUPPLY_AMOUNT, s.killSkillSupplyAmountTitle, 1);
		Perk.Builder supplyAmountPerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_SUPPLY_AMOUNT,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillSupplyAmount >= (int) value
		);
		
		supplyAmountShop.addItem(buildBalanceItem(0));
		supplyAmountShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(supplyAmountShop, s.killSkillSupplyAmount, s.killSkillSupplyAmountLore, 
				Lists.newArrayList(Material.CHEST), 
				Lists.newArrayList(0), 
				Lists.newArrayList(2,3,4,5,6,7,8),
				Lists.newArrayList(1,2,3,4,5,6,7),
				buildPerksChain(supplyAmountPerkBuilder,
						Lists.newArrayList(0, 1000, 2000, 4000, 6000, 8000, 10000),
						Lists.newArrayList(2,3,4,5,6,7,8)
				)
		);

		// supply-level
		Inventory supplyLevelShop = new Inventory(Constants.SHOP_KILL_SKILL_SUPPLY_LEVEL, s.killSkillSupplyLevelTitle, 1);
		Perk.Builder supplyLevelPerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_SUPPLY_LEVEL,
			(lobbyPlayer, value) -> lobbyPlayer.killSkillSupplyLevel >= (int) value
		);
		
		supplyLevelShop.addItem(buildBalanceItem(0));
		supplyLevelShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
		addPerksItems(supplyLevelShop, s.killSkillSupplyLevel, s.killSkillSupplyLevelLore, 
				Lists.newArrayList(Material.TRIPWIRE_HOOK), 
				Lists.newArrayList(0), 
				Lists.newArrayList(1,2,3,4,5,6,7),
				Lists.newArrayList(1,2,3,4,5,6,7),
				buildPerksChain(supplyLevelPerkBuilder,
						Lists.newArrayList(0, 1000, 2000, 4000, 6000, 8000, 10000),
						Lists.newArrayList(1,2,3,4,5,6,7)
				)
		);

        // enderchest
        Inventory enderchestShop = new Inventory(Constants.SHOP_KILL_SKILL_ENDERCHEST, s.killSkillEnderchestTitle, 1);
        Perk.Builder enderchestPerkBuilder = new Perk.Builder(PerkGame.KILL_SKILL, PerkName.KILL_SKILL_ENDERCHEST_ROWS,
                (lobbyPlayer, value) -> lobbyPlayer.killSkillEnderchestRows >= (int) value
        );

        enderchestShop.addItem(buildBalanceItem(0));
        enderchestShop.addItem(buildBackItem(8, Constants.SHOP_KILL_SKILL));
        addPerksItems(enderchestShop, s.killSkillEnderchest, s.killSkillEnderchestLore,
                Lists.newArrayList(Material.ENDER_CHEST),
                Lists.newArrayList(0),
                Lists.newArrayList(6,7,8,9,10,11,12),
                Lists.newArrayList(1,2,3,4,5,6,7),
                buildPerksChain(enderchestPerkBuilder,
                        Lists.newArrayList(0,15000,15000,15000,15000,15000,15000),
                        Lists.newArrayList(6,7,8,9,10,11,12)
                )
        );
		
		return Lists.newArrayList(experienceShop,lapisShop, potionShop, ironShop, diamondShop, blazeShop, supplyAmountShop, supplyLevelShop, enderchestShop);
	}

	private List<Perk> buildPerksChain(Builder perkBuilder, List<Integer> prices, List<Object> values) {
		List<Perk> perks = new ArrayList<Perk>();
		for(int i=0 ; i<values.size() ; i++){
			Perk requiredPerk = perks.size() > 0 ? perks.get(perks.size()-1) : null;
			int price = prices.size() > i ? prices.get(i) : prices.get(prices.size()-1);
			Object value = values.size() > i ? values.get(i) : values.get(values.size()-1);
			perks.add(perkBuilder.setPrice(price).setValue(value).setRequiredPerk(requiredPerk).build());
		}
		return perks;
	}

	public List<Inventory> buildEvolutionInventories() {
		
		// star
		Inventory starShop = new Inventory(Constants.SHOP_EVOLUTION, s.evolutionStarTitle, 1);
		Perk.Builder starPerkBuilder = new Perk.Builder(PerkGame.EVOLUTION, PerkName.EVOLUTION_STAR,
			(lobbyPlayer, value) -> lobbyPlayer.evoDoubleStarsChance >= (int) value
		);
		
		starShop.addItem(buildBalanceItem(0));
		starShop.addItem(buildBackItem(8, Constants.SHOP_GAMES));
		addPerksItems(starShop, s.evolutionStar, s.evolutionStarLore, 
				Lists.newArrayList(Material.NETHER_STAR), 
				Lists.newArrayList(0), 
				Lists.newArrayList(5,7,9,11,13,15,17),
				Lists.newArrayList(1,2,3,4,5,6,7),
				buildPerksChain(starPerkBuilder,
						Lists.newArrayList(500, 1000, 2000, 3000, 4000, 5000, 6000),
						Lists.newArrayList(5,7,9,11,13,15,17)
				)
		);
		
		return Lists.newArrayList(starShop);
	}

	public List<Inventory> buildCtfInventories() {
		// tnt amount
		Inventory tntAmountShop = new Inventory(Constants.SHOP_CTF_TNT_AMOUNT, s.ctfTntAmountTitle, 1);
		Perk.Builder tntAmountPerkBuilder = new Perk.Builder(PerkGame.CTF, PerkName.CTF_TNT_AMOUNT,
			(lobbyPlayer, value) -> lobbyPlayer.ctfTntAmount >= (int) value
		);
		
		tntAmountShop.addItem(buildBalanceItem(0));
		tntAmountShop.addItem(buildBackItem(8, Constants.SHOP_CTF));
		addPerksItems(tntAmountShop, s.ctfTntAmount, s.ctfTntAmountLore, 
				Lists.newArrayList(Material.TNT), 
				Lists.newArrayList(0),
				Lists.newArrayList(1,2,3,4,5),
				Lists.newArrayList(2,3,4,5,6),
				buildPerksChain(tntAmountPerkBuilder,
						Lists.newArrayList(0,1000,2000,4000,8000),
						Lists.newArrayList(1,2,3,4,5)
				)
		);
		
		// tnt power
		Inventory tntPowerShop = new Inventory(Constants.SHOP_CTF_TNT_POWER, s.ctfTntPowerTitle, 1);
		Perk.Builder tntPowerPerkBuilder = new Perk.Builder(PerkGame.CTF, PerkName.CTF_TNT_POWER,
			(lobbyPlayer, value) -> lobbyPlayer.ctfTntPower >= (double) value
		);
		
		tntPowerShop.addItem(buildBalanceItem(0));
		tntPowerShop.addItem(buildBackItem(8, Constants.SHOP_CTF));
		addPerksItems(tntPowerShop, s.ctfTntPower, s.ctfTntPowerLore, 
				Lists.newArrayList(Material.FIREBALL), 
				Lists.newArrayList(0), 
				Lists.newArrayList(2,3,4,5,6,7,8),
				Lists.newArrayList(1,2,3,4,5,6,7),
				buildPerksChain(tntPowerPerkBuilder,
						Lists.newArrayList(0,500,1000,2000,4000,6000,8000),
						Lists.newArrayList(20d,25d,30d,35d,40d,45d,50d)
				)
		);
		
		// tnt protection
		Inventory tntProtectionShop = new Inventory(Constants.SHOP_CTF_TNT_PROTECTION, s.ctfTntProtectionTitle, 1);
		Perk.Builder tntProtectionPerkBuilder = new Perk.Builder(PerkGame.CTF, PerkName.CTF_TNT_PROTECTION,
			(lobbyPlayer, value) -> lobbyPlayer.ctfTntProtection >= (double) value
		);
		
		tntProtectionShop.addItem(buildBalanceItem(0));
		tntProtectionShop.addItem(buildBackItem(8, Constants.SHOP_CTF));
		addPerksItems(tntProtectionShop, s.ctfTntProtection, s.ctfTntProtectionLore, 
				Lists.newArrayList(Material.IRON_CHESTPLATE), 
				Lists.newArrayList(0), 
				Lists.newArrayList(1,2,3,4,5),
				Lists.newArrayList(2,3,4,5,6),
				buildPerksChain(tntProtectionPerkBuilder,
						Lists.newArrayList(500,1000,2000,4000,8000),
						Lists.newArrayList(10d,20d,30d,40d,50d)
				)
		);
		
		return Lists.newArrayList(tntAmountShop,tntPowerShop, tntProtectionShop);
	}

	public List<Inventory> buildGladiatorInventories() {

		// gladiator hearts
		Inventory gladiatorHeartsShop = new Inventory(Constants.SHOP_GLADIATOR_GLADIATOR_HALF_HEART, s.gladiatorGladiatorHalfHeartsTitle, 2);
		Perk.Builder gladiatorHeartsPerkBuilder = new Perk.Builder(PerkGame.GLADIATOR, PerkName.GLADIATOR_GLADIATOR_HALF_HEARTS,
			(lobbyPlayer, value) -> lobbyPlayer.gladiatorGladiatorHalfHearts >= (int) value
		);
		
		gladiatorHeartsShop.addItem(buildBalanceItem(0));
		gladiatorHeartsShop.addItem(buildBackItem(8, Constants.SHOP_GLADIATOR));
		addPerksItems(gladiatorHeartsShop, s.gladiatorGladiatorHalfHearts, s.gladiatorGladiatorHalfHeartsLore, 
				Lists.newArrayList(Material.APPLE), 
				Lists.newArrayList(0), 
				Lists.newArrayList(41,42,43,44,45,46,47,48,49,50),
				Lists.newArrayList(2,3,4,5,6,11,12,13,14,15),
				buildPerksChain(gladiatorHeartsPerkBuilder,
						Lists.newArrayList(500,1000,2000,3000,4000,5000,6000,7000,8000,9000),
						Lists.newArrayList(41,42,43,44,45,46,47,48,49,50)
				)
		);
		
		// enchant chance
		Inventory enchantChanceShop = new Inventory(Constants.SHOP_GLADIATOR_ENCHANT_CHANCE, s.gladiatorEnchantChanceTitle, 1);
		Perk.Builder enchantChancePerkBuilder = new Perk.Builder(PerkGame.GLADIATOR, PerkName.GLADIATOR_ENCHANT_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.gladiatorEnchantChance >= (int) value
		);
		
		enchantChanceShop.addItem(buildBalanceItem(0));
		enchantChanceShop.addItem(buildBackItem(8, Constants.SHOP_GLADIATOR));
		addPerksItems(enchantChanceShop, s.gladiatorEnchantChance, s.gladiatorEnchantChanceLore, 
				Lists.newArrayList(Material.ENCHANTED_BOOK), 
				Lists.newArrayList(0), 
				Lists.newArrayList(2,3,4,5,6),
				Lists.newArrayList(2,3,4,5,6),
				buildPerksChain(enchantChancePerkBuilder,
						Lists.newArrayList(0,1000,2000,4000,6000),
						Lists.newArrayList(20,30,40,50,60)
				)
		);
		
		// slave bow chance
		Inventory slaveBowChanceShop = new Inventory(Constants.SHOP_GLADIATOR_SLAVE_BOW_CHANCE, s.gladiatorSlaveBowChanceTitle, 1);
		Perk.Builder slaveBowPerkBuilder = new Perk.Builder(PerkGame.GLADIATOR, PerkName.GLADIATOR_SLAVE_BOW_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.gladiatorSlaveBowChance >= (int) value
		);
		
		slaveBowChanceShop.addItem(buildBalanceItem(0));
		slaveBowChanceShop.addItem(buildBackItem(8, Constants.SHOP_GLADIATOR));
		addPerksItems(slaveBowChanceShop, s.gladiatorSlaveBowChance, s.gladiatorSlaveBowChanceLore, 
				Lists.newArrayList(Material.BOW), 
				Lists.newArrayList(0), 
				Lists.newArrayList(2,3,4,5,6),
				Lists.newArrayList(2,3,4,5,6),
				buildPerksChain(slaveBowPerkBuilder,
						Lists.newArrayList(0,1000,2000,4000,6000),
						Lists.newArrayList(20,30,40,50,60)
				)
		);
		
		// slave extra chance
		Inventory slaveExtraChanceShop = new Inventory(Constants.SHOP_GLADIATOR_SLAVE_EXTRA_CHANCE, s.gladiatorSlaveExtraChanceTitle, 1);
		Perk.Builder slaveExtraPerkBuilder = new Perk.Builder(PerkGame.GLADIATOR, PerkName.GLADIATOR_SLAVE_EXTRA_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.gladiatorSlaveExtraChance >= (int) value
		);
		
		slaveExtraChanceShop.addItem(buildBalanceItem(0));
		slaveExtraChanceShop.addItem(buildBackItem(8, Constants.SHOP_GLADIATOR));
		addPerksItems(slaveExtraChanceShop, s.gladiatorSlaveExtraChance, s.gladiatorSlaveExtraChanceLore, 
				Lists.newArrayList(Material.MONSTER_EGG), 
				Lists.newArrayList(62), 
				Lists.newArrayList(2,3,4,5,6),
				Lists.newArrayList(2,3,4,5,6),
				buildPerksChain(slaveExtraPerkBuilder,
						Lists.newArrayList(0,1000,2000,4000,6000),
						Lists.newArrayList(20,30,40,50,60)
				)
		);
		
		// slave potion chance
		Inventory slavePotionChanceShop = new Inventory(Constants.SHOP_GLADIATOR_SLAVE_POTION_CHANCE, s.gladiatorSlavePotionChanceTitle, 1);
		Perk.Builder slavePotionPerkBuilder = new Perk.Builder(PerkGame.GLADIATOR, PerkName.GLADIATOR_SLAVE_POTION_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.gladiatorSlavePotionChance >= (int) value
		);
		
		slavePotionChanceShop.addItem(buildBalanceItem(0));
		slavePotionChanceShop.addItem(buildBackItem(8, Constants.SHOP_GLADIATOR));
		addPerksItems(slavePotionChanceShop, s.gladiatorSlavePotionChance, s.gladiatorSlavePotionChanceLore, 
				Lists.newArrayList(Material.POTION), 
				Lists.newArrayList(0), 
				Lists.newArrayList(2,3,4,5,6),
				Lists.newArrayList(2,3,4,5,6),
				buildPerksChain(slavePotionPerkBuilder,
						Lists.newArrayList(0,1000,2000,4000,6000),
						Lists.newArrayList(20,30,40,50,60)
				)
		);
		
		return Lists.newArrayList(gladiatorHeartsShop, enchantChanceShop, slaveBowChanceShop, slaveExtraChanceShop, slavePotionChanceShop);
	}

	public List<Inventory> buildRageInventories(){

        // xp regain speed
        Inventory xpSpeedShop = new Inventory(Constants.SHOP_RAGE_XP_SPEED, s.rageXpRegainSpeedTitle, 1);
        Perk.Builder xpSpeedPerkBuilder = new Perk.Builder(PerkGame.RAGE, PerkName.RAGE_XP_REGAIN_SPEED,
                (lobbyPlayer, value) -> lobbyPlayer.rageXpRegainSpeed >= (int) value
        );

        xpSpeedShop.addItem(buildBalanceItem(0));
        xpSpeedShop.addItem(buildBackItem(8, Constants.SHOP_RAGE));
        addPerksItems(xpSpeedShop, s.rageXpRegainSpeed, s.rageXpRegainSpeedLore,
                Lists.newArrayList(Material.EXP_BOTTLE),
                Lists.newArrayList(0),
                Lists.newArrayList(1,2),
                Lists.newArrayList(3,4),
                buildPerksChain(xpSpeedPerkBuilder,
                        Lists.newArrayList(0,5000),
                        Lists.newArrayList(1,2)
                )
        );


        // gadgets levels
        Inventory gagdetsLevelShop = new Inventory(Constants.SHOP_RAGE_GADGETS, s.rageGagdetsLevelTitle, 1);
        Perk.Builder gagdetsLevelPerkBuilder = new Perk.Builder(PerkGame.RAGE, PerkName.RAGE_GADGETS_LEVEL,
                (lobbyPlayer, value) -> lobbyPlayer.rageGagdetsLevel >= (int) value
        );

        gagdetsLevelShop.addItem(buildBalanceItem(0));
        gagdetsLevelShop.addItem(buildBackItem(8, Constants.SHOP_RAGE));
        addPerksItems(gagdetsLevelShop, s.rageGagdetsLevel, s.rageGagdetsLevelLore,
                Lists.newArrayList(Material.SUGAR),
                Lists.newArrayList(0),
                Lists.newArrayList(1,2,3),
                Lists.newArrayList(3,4,5),
                buildPerksChain(gagdetsLevelPerkBuilder,
                        Lists.newArrayList(0,3000,7000),
                        Lists.newArrayList(1,2,3)
                )
        );


        // arrows shop
        Inventory arrowsShop = new Inventory(Constants.SHOP_RAGE_ARROWS, s.rageArrowsPerSpawnTitle, 1);
        Perk.Builder arrowsPerkBuilder = new Perk.Builder(PerkGame.RAGE, PerkName.RAGE_ARROWS_PER_SPAWN,
                (lobbyPlayer, value) -> lobbyPlayer.rageArrowsPerSpawn >= (int) value
        );

        arrowsShop.addItem(buildBalanceItem(0));
        arrowsShop.addItem(buildBackItem(8, Constants.SHOP_RAGE));
        addPerksItems(arrowsShop, s.rageArrowsPerSpawn, s.rageArrowsPerSpawnLore,
                Lists.newArrayList(Material.ARROW),
                Lists.newArrayList(0),
                Lists.newArrayList(2,3,4,5,6),
                Lists.newArrayList(2,3,4,5,6),
                buildPerksChain(arrowsPerkBuilder,
                        Lists.newArrayList(0,2000,4000,6000,8000),
                        Lists.newArrayList(2,3,4,5,6)
                )
        );

        return Lists.newArrayList(xpSpeedShop, gagdetsLevelShop, arrowsShop);
	}

	public List<Inventory> buildSurvivalRushInventories() {


		// tools
		Inventory toolsShop = new Inventory(Constants.SHOP_SURVIVAL_RUSH_TOOLS, s.survivalRushToolsTitle, 1);

		toolsShop.addItem(buildBalanceItem(0));
		toolsShop.addItem(buildBackItem(8, Constants.SHOP_SURVIVAL_RUSH));
		
		Perk.Builder ironPickaxePerkBuilder = new Perk.Builder(PerkGame.SURVIVAL_RUSH, PerkName.SURVIVAL_RUSH_IRON_PICKAXE,
			(lobbyPlayer, value) -> lobbyPlayer.survivalRushHasIronPickaxe == (boolean) value
		);
		addPerksItems(toolsShop, s.survivalRushIronPickaxe, s.survivalRushIronPickaxeLore, 
				Lists.newArrayList(Material.IRON_PICKAXE), 
				Lists.newArrayList(0), 
				Lists.newArrayList(1),
				Lists.newArrayList(3),
				buildPerksChain(ironPickaxePerkBuilder,
						Lists.newArrayList(3000),
						Lists.newArrayList(true)
				)
		);
		
		Perk.Builder lootingSwordPerkBuilder = new Perk.Builder(PerkGame.SURVIVAL_RUSH, PerkName.SURVIVAL_RUSH_LOOTING_SWORD,
			(lobbyPlayer, value) -> lobbyPlayer.survivalRushSwordLooting == (boolean) value
		);
		addPerksItems(toolsShop, s.survivalRushLootingSword, s.survivalRushLootingSwordLore, 
				Lists.newArrayList(Material.WOOD_SWORD), 
				Lists.newArrayList(0), 
				Lists.newArrayList(1),
				Lists.newArrayList(5),
				buildPerksChain(lootingSwordPerkBuilder,
						Lists.newArrayList(5000),
						Lists.newArrayList(true)
				)
		);
		

		// iron loot chance
		Inventory ironLootShop = new Inventory(Constants.SHOP_SURVIVAL_RUSH_IRON_LOOT, s.survivalRushIronLootChanceTitle, 1);
		Perk.Builder ironLootPerkBuilder = new Perk.Builder(PerkGame.SURVIVAL_RUSH, PerkName.SURVIVAL_RUSH_IRON_LOOT_CHANCE,
			(lobbyPlayer, value) -> lobbyPlayer.survivalRushIronLootChance >= (int) value
		);
		
		ironLootShop.addItem(buildBalanceItem(0));
		ironLootShop.addItem(buildBackItem(8, Constants.SHOP_SURVIVAL_RUSH));
		addPerksItems(ironLootShop, s.survivalRushIronLootChance, s.survivalRushIronLootChanceLore, 
				Lists.newArrayList(Material.IRON_INGOT), 
				Lists.newArrayList(0), 
				Lists.newArrayList(5,8,11,14,17),
				Lists.newArrayList(2,3,4,5,6),
				buildPerksChain(ironLootPerkBuilder,
						Lists.newArrayList(0,1000,2000,4000,6000),
						Lists.newArrayList(5,8,11,14,17)
				)
		);
		
		// skeletons per tnt
		Inventory skeletonsShop = new Inventory(Constants.SHOP_SURVIVAL_RUSH_SKELETON_PER_TNT, s.survivalRushSkeletonsPerTntTitle, 1);
		Perk.Builder skeletonsPerkBuilder = new Perk.Builder(PerkGame.SURVIVAL_RUSH, PerkName.SURVIVAL_RUSH_SKELETONS_PER_TNT,
			(lobbyPlayer, value) -> lobbyPlayer.survivalRushSkeletonsPerTNT <= (int) value
		);
		
		skeletonsShop.addItem(buildBalanceItem(0));
		skeletonsShop.addItem(buildBackItem(8, Constants.SHOP_SURVIVAL_RUSH));
		addPerksItems(skeletonsShop, s.survivalRushSkeletonsPerTnt, s.survivalRushSkeletonsPerTntLore, 
				Lists.newArrayList(Material.TNT), 
				Lists.newArrayList(0), 
				Lists.newArrayList(50,47,44,41,38,35,32),
				Lists.newArrayList(1,2,3,4,5,6,7),
				buildPerksChain(skeletonsPerkBuilder,
						Lists.newArrayList(0,500,1000,2000,3000,4000,5000),
						Lists.newArrayList(50,47,44,41,38,35,32)
				)
		);
		
		// blocks
		Inventory blocksShop = new Inventory(Constants.SHOP_SURVIVAL_RUSH_NBR_BLOCKS_RESPAWN, s.survivalRushNbrBlocksRespawnTitle, 1);
		Perk.Builder blocksPerkBuilder = new Perk.Builder(PerkGame.SURVIVAL_RUSH, PerkName.SURVIVAL_RUSH_NBR_BLOCKS_RESPAWN,
			(lobbyPlayer, value) -> lobbyPlayer.survivalRushNbrBlocksRespawn >= (int) value
		);
		
		blocksShop.addItem(buildBalanceItem(0));
		blocksShop.addItem(buildBackItem(8, Constants.SHOP_SURVIVAL_RUSH));
		addPerksItems(blocksShop, s.survivalRushNbrBlocksRespawn, s.survivalRushNbrBlocksRespawnLore, 
				Lists.newArrayList(Material.SANDSTONE), 
				Lists.newArrayList(0), 
				Lists.newArrayList(4,6,8,10,12,14,16),
				Lists.newArrayList(1,2,3,4,5,6,7),
				buildPerksChain(blocksPerkBuilder,
						Lists.newArrayList(500,1000,2000,3000,4000,5000,6000),
						Lists.newArrayList(4,6,8,10,12,14,16)
				)
		);
		
		return Lists.newArrayList(toolsShop, ironLootShop, skeletonsShop, blocksShop);
	}
	
	public Item buildBalanceItem(int position){
		return new Item.ItemBuilder(Material.EMERALD)
			.withName(s.money)
			.withLore(Lists.newArrayList(s.moneyLore))
			.withPosition(0)
			.build();
	}
	
	public void addPerksItems(Inventory inventory, String name, String lore, List<Material> materials, List<Integer> damages, List<Integer> amounts, List<Integer> positions, List<Perk> perks){
		
		for(int i=0 ; i<perks.size() ; i++){
			
			Material material = materials.size() > i ? materials.get(i) : materials.get(materials.size()-1);
			short damage = damages.size() > i ? (short) (int) damages.get(i) : (short) (int) damages.get(damages.size()-1);
			int amount = amounts.size() > i ? amounts.get(i) : amounts.get(amounts.size()-1);
			int position = positions.size() > i ? positions.get(i) : positions.get(positions.size()-1);
			Perk perk = perks.size() > i ? perks.get(i) : perks.get(perks.size()-1);
			
			String itemName = name.replace("%value%", String.valueOf(perk.getRoundedValue()));
			
			perk.setName(ChatColor.stripColor(itemName));
			
			List<String> buyableItemLore = new ArrayList<String>();
			buyableItemLore.add(lore.replace("%value%", String.valueOf(perk.getRoundedValue())));
			buyableItemLore.add(perk.getPrice() == 0d ? s.freeLore.replace("%price%", String.valueOf(perk.getRoundedPrice())) : s.priceLore.replace("%price%", String.valueOf(perk.getRoundedPrice())));
			
			List<String> boughtItemLore = new ArrayList<String>();
			boughtItemLore.add(lore.replace("%value%", String.valueOf(perk.getRoundedValue())));
			boughtItemLore.add(perk.getPrice() == 0d ? s.freeLore.replace("%price%", String.valueOf(perk.getRoundedPrice())) : s.priceLoreBought.replace("%price%", String.valueOf(perk.getRoundedPrice())));
			boughtItemLore.add(s.unlockedLore);
			
			List<String> unbuyableItemLore = new ArrayList<String>();
			unbuyableItemLore.add(lore.replace("%value%", String.valueOf(perk.getRoundedValue())));
			unbuyableItemLore.add(perk.getPrice() == 0d ? s.freeLore.replace("%price%", String.valueOf(perk.getRoundedPrice())) : s.priceLore.replace("%price%", String.valueOf(perk.getRoundedPrice())));
			if(perk.getRequiredPerk() != null){
				unbuyableItemLore.add(s.requirePreviousPerkLore.replace("%perk%", perk.getRequiredPerk().getName()));
			}
			
			Item buyableItem = new Item.ItemBuilder(material)
					.withDamage(damage)
					.withName(itemName)
					.withLore(buyableItemLore)
					.withAmount(amount)
					.withPosition(position)
					.withHideFlags(true)
					.build();
			
			Item boughtItem = new Item.ItemBuilder(Material.STAINED_GLASS_PANE)
					.withDamage((short) 5)
					.withName(itemName)
					.withLore(boughtItemLore)
					.withAmount(amount)
					.withPosition(position)
					.withHideFlags(true)
					.build();
			
			Item unbuyableItem = new Item.ItemBuilder(material)
					.withDamage(damage)
					.withName(itemName)
					.withLore(unbuyableItemLore)
					.withAmount(amount)
					.withPosition(position)
					.withHideFlags(true)
					.build();
			
			buyableItem.setActions(Lists.newArrayList(new BuyPerkAction(api, buyableItem, unbuyableItem, boughtItem, perk)));
			
			inventory.addItem(buyableItem);
		}
	}
	
	public boolean updatePlayerPerk(Perk perk, LobbyPlayer lobbyPlayer){
		HCMySQLAPI sql = api.getMySQLAPI();
		if(sql.isEnabled()){
			
			try{
				sql.execute(sql.prepareStatement(updatePlayerPerkSQL
						.replace("%table%", perk.getGame().getTable())
						.replace("%column%", perk.getPerkName().getColumn())
						.replace("%value%", String.valueOf(perk.getValue()))
					, String.valueOf(lobbyPlayer.getId()))
				);
				reloadPlayerPerks(lobbyPlayer);
				return true;
			}catch(Exception e){
                e.printStackTrace();
				return false;
			}
			
		}else{
			return false;
		}
	}
	
	public Item buildBackItem(int position, String backToInvName){
		Item back = new Item.ItemBuilder(Material.RAW_FISH)
				.withDamage((short) 3)
				.withName(s.back)
				.withPosition(position)
				.build();
		back.setActions(Lists.newArrayList(new OpenInventoryAction(backToInvName)));
		return back;
	}

	public void reloadPlayerPerks(LobbyPlayer lobbyPlayer){
		
		try{

			HCMySQLAPI sql = api.getMySQLAPI();
			
			CachedRowSet perks = sql.query(sql.prepareStatement(selectPlayerPerksSQL, String.valueOf(lobbyPlayer.getId())));
			
			if(!perks.first()){
				Log.warn("No perks found for player "+lobbyPlayer.getName());
				return; // nothing found, wierd
				
			}
			
			if(perks.first()){
				lobbyPlayer.killSkillExperienceAmount = perks.getInt("killSkillExperienceAmount");
				lobbyPlayer.killSkillLapisChance = perks.getInt("killSkillLapisChance");
				lobbyPlayer.killSkillPotionChance = perks.getInt("killSkillPotionChance");
				lobbyPlayer.killSkillIronChance = perks.getInt("killSkillIronChance");
				lobbyPlayer.killSkillDiamondChance = perks.getInt("killSkillDiamondChance");
				lobbyPlayer.killSkillBlazeChance = perks.getInt("killSkillBlazeChance");
				lobbyPlayer.killSkillSupplyAmount = perks.getInt("killSkillSupplyAmount");
				lobbyPlayer.killSkillSupplyLevel = perks.getInt("killSkillSupplyLevel");
				lobbyPlayer.killSkillEnderchestRows = perks.getInt("killSkillEnderchestRows");

				lobbyPlayer.evoDoubleStarsChance = perks.getInt("evoDoubleStarsChance");
				
				lobbyPlayer.ctfTntPower = perks.getDouble("ctfTntPower");
				lobbyPlayer.ctfTntProtection = perks.getDouble("ctfTntProtection");
				lobbyPlayer.ctfTntAmount = perks.getInt("ctfTntAmount");
				
				lobbyPlayer.gladiatorGladiatorHalfHearts = perks.getInt("gladiatorGladiatorHalfHearts");
				lobbyPlayer.gladiatorSlaveBowChance = perks.getInt("gladiatorSlaveBowChance");
				lobbyPlayer.gladiatorSlaveExtraChance = perks.getInt("gladiatorSlaveExtraChance");
				lobbyPlayer.gladiatorSlavePotionChance = perks.getInt("gladiatorSlavePotionChance");
				lobbyPlayer.gladiatorEnchantChance = perks.getInt("gladiatorEnchantChance");
				
				lobbyPlayer.survivalRushHasIronPickaxe = perks.getBoolean("survivalRushHasIronPickaxe");
				lobbyPlayer.survivalRushSwordLooting = perks.getBoolean("survivalRushSwordLooting");
				lobbyPlayer.survivalRushIronLootChance = perks.getInt("survivalRushIronLootChance");
				lobbyPlayer.survivalRushSkeletonsPerTNT = perks.getInt("survivalRushSkeletonsPerTNT");
				lobbyPlayer.survivalRushNbrBlocksRespawn = perks.getInt("survivalRushNbrBlocksRespawn");

                lobbyPlayer.rageArrowsPerSpawn = perks.getInt("rageArrowsPerSpawn");
                lobbyPlayer.rageGagdetsLevel = perks.getInt("rageGagdetsLevel");
                lobbyPlayer.rageXpRegainSpeed = perks.getInt("rageXpRegainSpeed");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			Log.severe("Couldn't load perks for player "+lobbyPlayer.getName()+" Read error above.");
		}
		
	}
}
