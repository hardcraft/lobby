package com.gmail.val59000mc.lobby.perks;

public enum PerkGame {
	KILL_SKILL("killskill_player_perks"),
	EVOLUTION("evo_player_perks"),
	CTF("ctf_player_perks"),
	GLADIATOR("gladiator_player_perks"),
	SURVIVAL_RUSH("survivalrush_player_perks"),
	RAGE("rage_player_perks");
	
	private String table;
	
	private PerkGame(String table){
		this.table = table;
	}

	public String getTable() {
		return table;
	}
	
}
