package com.gmail.val59000mc.lobby.perks;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;

public class PerksStrings {

	private HCGameAPI api;
	
	// common
	public String requirePreviousPerkLore;
	public String priceLore;
	public String priceLoreBought;
	public String back;
	public String money;
	public String moneyLore;
	public String freeLore;
	public String unlockedLore;
	
	// kill skill
	public String killSkillExperience;
	public String killSkillExperienceLore;
	public String killSkillExperienceTitle;
	public String killSkillLapis;
	public String killSkillLapisLore;
	public String killSkillLapisTitle;
	public String killSkillIron;
	public String killSkillIronLore;
	public String killSkillIronTitle;
	public String killSkillDiamond;
	public String killSkillDiamondLore;
	public String killSkillDiamondTitle;
	public String killSkillPotion;
	public String killSkillPotionLore;
	public String killSkillPotionTitle;
	public String killSkillBlaze;
	public String killSkillBlazeLore;
	public String killSkillBlazeTitle;
	public String killSkillSupplyAmount;
	public String killSkillSupplyAmountLore;
	public String killSkillSupplyAmountTitle;
	public String killSkillSupplyLevel;
	public String killSkillSupplyLevelLore;
	public String killSkillSupplyLevelTitle;
	public String killSkillEnderchest;
	public String killSkillEnderchestLore;
	public String killSkillEnderchestTitle;

	// evolution
	public String evolutionStar;
	public String evolutionStarLore;
	public String evolutionStarTitle;
	
	// ctf
	public String ctfTntAmount;
	public String ctfTntAmountLore;
	public String ctfTntAmountTitle;
	public String ctfTntPower;
	public String ctfTntPowerLore;
	public String ctfTntPowerTitle;
	public String ctfTntProtection;
	public String ctfTntProtectionLore;
	public String ctfTntProtectionTitle;
	
	// gladiator
	public String gladiatorGladiatorHalfHearts;
	public String gladiatorGladiatorHalfHeartsLore;
	public String gladiatorGladiatorHalfHeartsTitle;
	public String gladiatorSlaveBowChance;
	public String gladiatorSlaveBowChanceLore;
	public String gladiatorSlaveBowChanceTitle;
	public String gladiatorSlavePotionChance;
	public String gladiatorSlavePotionChanceLore;
	public String gladiatorSlavePotionChanceTitle;
	public String gladiatorSlaveExtraChance;
	public String gladiatorSlaveExtraChanceLore;
	public String gladiatorSlaveExtraChanceTitle;
	public String gladiatorEnchantChance;
	public String gladiatorEnchantChanceLore;
	public String gladiatorEnchantChanceTitle;
	
	// survival rush
	public String survivalRushToolsTitle;
	public String survivalRushIronPickaxe;
	public String survivalRushIronPickaxeLore;
	public String survivalRushLootingSword;
	public String survivalRushLootingSwordLore;
	public String survivalRushIronLootChance;
	public String survivalRushIronLootChanceLore;
	public String survivalRushIronLootChanceTitle;
	public String survivalRushSkeletonsPerTnt;
	public String survivalRushSkeletonsPerTntLore;
	public String survivalRushSkeletonsPerTntTitle;
	public String survivalRushNbrBlocksRespawn;
	public String survivalRushNbrBlocksRespawnLore;
	public String survivalRushNbrBlocksRespawnTitle;

	// rage
    public String rageXpRegainSpeed;
    public String rageXpRegainSpeedLore;
    public String rageXpRegainSpeedTitle;
    public String rageGagdetsLevel;
    public String rageGagdetsLevelLore;
    public String rageGagdetsLevelTitle;
    public String rageArrowsPerSpawn;
    public String rageArrowsPerSpawnLore;
    public String rageArrowsPerSpawnTitle;

	public PerksStrings(HCGameAPI api){
		this.api = api;
	}

	public HCGameAPI getApi() {
		return api;
	}
	
	public void setup(){
		HCStringsAPI s = api.getStringsAPI();
		
		String b;
		
		// common
		b = "lobby.perks.common."; // base common
		requirePreviousPerkLore = s.get(b+"require-previous-lore").toString();
		priceLore = s.get(b+"price-lore").toString();
		priceLoreBought = s.get(b+"price-lore-bought").toString();
		back = s.get(b+"back").toString();
		money = s.get(b+"money.name").toString();
		moneyLore = s.get(b+"money.lore").toString();
		freeLore = s.get(b+"free-lore").toString();
		unlockedLore = s.get(b+"unlocked-lore").toString();
		
		// killskill
		b = "lobby.perks.kill-skill."; // base kill-skill
		killSkillExperience = s.get(b+"experience.name").toString();
		killSkillExperienceLore = s.get(b+"experience.lore").toString();
		killSkillExperienceTitle = s.get(b+"experience.title").toString();
		killSkillLapis = s.get(b+"lapis.name").toString();
		killSkillLapisLore = s.get(b+"lapis.lore").toString();
		killSkillLapisTitle = s.get(b+"lapis.title").toString();
		killSkillIron = s.get(b+"iron.name").toString();
		killSkillIronLore = s.get(b+"iron.lore").toString();
		killSkillIronTitle = s.get(b+"iron.title").toString();
		killSkillDiamond = s.get(b+"diamond.name").toString();
		killSkillDiamondLore = s.get(b+"diamond.lore").toString();
		killSkillDiamondTitle = s.get(b+"diamond.title").toString();
		killSkillPotion = s.get(b+"potion.name").toString();
		killSkillPotionLore = s.get(b+"potion.lore").toString();
		killSkillPotionTitle = s.get(b+"potion.title").toString();
		killSkillBlaze = s.get(b+"blaze.name").toString();
		killSkillBlazeLore = s.get(b+"blaze.lore").toString();
		killSkillBlazeTitle = s.get(b+"blaze.title").toString();
		killSkillSupplyAmount = s.get(b+"supply-amount.name").toString();
		killSkillSupplyAmountLore = s.get(b+"supply-amount.lore").toString();
		killSkillSupplyAmountTitle = s.get(b+"supply-amount.title").toString();
		killSkillSupplyLevel = s.get(b+"supply-level.name").toString();
		killSkillSupplyLevelLore = s.get(b+"supply-level.lore").toString();
		killSkillSupplyLevelTitle = s.get(b+"supply-level.title").toString();
        killSkillEnderchest = s.get(b+"enderchest.name").toString();
        killSkillEnderchestLore = s.get(b+"enderchest.lore").toString();
        killSkillEnderchestTitle = s.get(b+"enderchest.title").toString();
		
		// evolution
		b = "lobby.perks.evolution."; // base evolution
		evolutionStar = s.get(b+"star.name").toString();
		evolutionStarLore = s.get(b+"star.lore").toString();
		evolutionStarTitle = s.get(b+"star.title").toString();
		
		// ctf
		b = "lobby.perks.ctf."; // base ctf
		ctfTntAmount = s.get(b+"tnt-amount.name").toString();
		ctfTntAmountLore = s.get(b+"tnt-amount.lore").toString();
		ctfTntAmountTitle = s.get(b+"tnt-amount.title").toString();
		ctfTntPower = s.get(b+"tnt-power.name").toString();
		ctfTntPowerLore = s.get(b+"tnt-power.lore").toString();
		ctfTntPowerTitle = s.get(b+"tnt-power.title").toString();
		ctfTntProtection = s.get(b+"tnt-protection.name").toString();
		ctfTntProtectionLore = s.get(b+"tnt-protection.lore").toString();
		ctfTntProtectionTitle = s.get(b+"tnt-protection.title").toString();
		
		// gladiator
		b = "lobby.perks.gladiator."; // base gladiator
		gladiatorGladiatorHalfHearts = s.get(b+"gladiator-half-hearts.name").toString();
		gladiatorGladiatorHalfHeartsLore = s.get(b+"gladiator-half-hearts.lore").toString();
		gladiatorGladiatorHalfHeartsTitle = s.get(b+"gladiator-half-hearts.title").toString();
		gladiatorEnchantChance = s.get(b+"enchant-chance.name").toString();
		gladiatorEnchantChanceLore = s.get(b+"enchant-chance.lore").toString();
		gladiatorEnchantChanceTitle = s.get(b+"enchant-chance.title").toString();
		gladiatorSlavePotionChance = s.get(b+"slave-potion-chance.name").toString();
		gladiatorSlavePotionChanceLore = s.get(b+"slave-potion-chance.lore").toString();
		gladiatorSlavePotionChanceTitle = s.get(b+"slave-potion-chance.title").toString();
		gladiatorSlaveExtraChance = s.get(b+"slave-extra-chance.name").toString();
		gladiatorSlaveExtraChanceLore = s.get(b+"slave-extra-chance.lore").toString();
		gladiatorSlaveExtraChanceTitle = s.get(b+"slave-extra-chance.title").toString();
		gladiatorSlaveBowChance = s.get(b+"slave-bow-chance.name").toString();
		gladiatorSlaveBowChanceLore = s.get(b+"slave-bow-chance.lore").toString();
		gladiatorSlaveBowChanceTitle = s.get(b+"slave-bow-chance.title").toString();
		
		// survival rush
		b = "lobby.perks.survival-rush."; // base survival-rush
		survivalRushToolsTitle = s.get(b+"tools.title").toString();
		survivalRushIronPickaxe = s.get(b+"iron-pickaxe.name").toString();
		survivalRushIronPickaxeLore = s.get(b+"iron-pickaxe.lore").toString();
		survivalRushLootingSword = s.get(b+"looting-sword.name").toString();
		survivalRushLootingSwordLore = s.get(b+"looting-sword.lore").toString();
		survivalRushIronLootChance = s.get(b+"iron-loot-chance.name").toString();
		survivalRushIronLootChanceLore = s.get(b+"iron-loot-chance.lore").toString();
		survivalRushIronLootChanceTitle = s.get(b+"iron-loot-chance.title").toString();
		survivalRushSkeletonsPerTnt = s.get(b+"skeletons-per-tnt.name").toString();
		survivalRushSkeletonsPerTntLore = s.get(b+"skeletons-per-tnt.lore").toString();
		survivalRushSkeletonsPerTntTitle = s.get(b+"skeletons-per-tnt.title").toString();
		survivalRushNbrBlocksRespawn = s.get(b+"nbr-blocks-respawn.name").toString();
		survivalRushNbrBlocksRespawnLore = s.get(b+"nbr-blocks-respawn.lore").toString();
		survivalRushNbrBlocksRespawnTitle = s.get(b+"nbr-blocks-respawn.title").toString();

        // rage
        b = "lobby.perks.rage."; // base rage
        rageXpRegainSpeedTitle = s.get(b+"xp-regain-speed.title").toString();
        rageXpRegainSpeed = s.get(b+"xp-regain-speed.name").toString();
        rageXpRegainSpeedLore = s.get(b+"xp-regain-speed.lore").toString();
        rageGagdetsLevelTitle = s.get(b+"gagdets-level.title").toString();
        rageGagdetsLevel = s.get(b+"gagdets-level.name").toString();
        rageGagdetsLevelLore = s.get(b+"gagdets-level.lore").toString();
        rageArrowsPerSpawnTitle = s.get(b+"arrows-per-spawn.title").toString();
        rageArrowsPerSpawn = s.get(b+"arrows-per-spawn.name").toString();
        rageArrowsPerSpawnLore = s.get(b+"arrows-per-spawn.lore").toString();
		
	}
	
	
}
