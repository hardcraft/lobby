package com.gmail.val59000mc.lobby.perks;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lobby.callbacks.LobbyCallbacks;
import com.gmail.val59000mc.lobby.players.LobbyPlayer;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemModifier;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class BuyPerkAction extends Action implements ItemModifier{

	private HCGameAPI api;
	private PerksManager perksManager;
	private Perk perk;
	private Item buyableItem; // has required perks to buy
	private Item unbuyableItem; // no required perk to buy
	private Item boughtItem; // perk already bought

	public BuyPerkAction(HCGameAPI api, Item buyableItem, Item unbuyableItem, Item boughtItem, Perk perk) {
		this.api = api;
		this.perksManager = ((LobbyCallbacks) api.getCallbacksApi()).getPerksManager();
		this.buyableItem = buyableItem;
		this.unbuyableItem = unbuyableItem;
		this.boughtItem = boughtItem;
		this.perk = perk;

		if(!api.getDependencies().isVaultEnabled()){
			throw new IllegalStateException("Cannot register a buy action without Vault being installed !");
		}
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		LobbyPlayer lobbyPlayer = (LobbyPlayer) api.getPlayersManagerAPI().getHCPlayer(player);	
		
		if(lobbyPlayer == null){
			executeNextAction(player, sigPlayer, false);
		}else{
			
			OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
			
			if(perk.hasUnlockedPerk(lobbyPlayer)){
				api.getStringsAPI()
					.get("lobby.perks.action.already-bought")
					.replace("%perk%", perk.getName())
					.sendChatP(lobbyPlayer);
				executeNextAction(player, sigPlayer, false);
			}else if(!perk.hasRequiredPerk(lobbyPlayer)){
				api.getStringsAPI()
					.get("lobby.perks.action.need-required-perk")
					.replace("%perk%", perk.getRequiredPerk().getName())
					.sendChatP(lobbyPlayer);
				executeNextAction(player, sigPlayer, false);
			}else{
				
				Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), () ->{
					if(api.getDependencies().hasEnoughMoney(offlinePlayer, perk.getPrice())){
						boolean isPerkUpdated = perksManager.updatePlayerPerk(perk, lobbyPlayer);
						if(isPerkUpdated){
							api.getDependencies().removeMoney(offlinePlayer, perk.getPrice());
							Bukkit.getScheduler().runTask(api.getPlugin(), () ->{
								api.getStringsAPI()
									.get("lobby.perks.action.bought")
									.replace("%price%", String.valueOf(perk.getRoundedPrice()))
									.replace("%perk%", perk.getName())
									.sendChatP(lobbyPlayer);
								api.getSoundAPI().play(lobbyPlayer,Sound.NOTE_PLING, 1, 2);
								executeNextAction(player, sigPlayer, true);
							});
						}else{
							Bukkit.getScheduler().runTask(api.getPlugin(), () ->{
								api.getStringsAPI().get("lobby.perks.action.error").sendChatP(lobbyPlayer);
								executeNextAction(player, sigPlayer, false);
							});
						}
					}else{
						Bukkit.getScheduler().runTask(api.getPlugin(), () ->{
							api.getStringsAPI()
								.get("lobby.perks.action.not-enough-money")
								.replace("%price%", String.valueOf(perk.getRoundedPrice()))
								.replace("%perk%", perk.getName())
								.sendChatP(lobbyPlayer);
							executeNextAction(player, sigPlayer, false);
						});
					}
				});
			}
		}
	}

	@Override
	public boolean isModifier() {
		return true;
	}
	
	@Override
	public Item getAlternateVersion(Player player, SigPlayer sigPlayer) {

		LobbyPlayer lobbyPlayer = (LobbyPlayer) api.getPlayersManagerAPI().getHCPlayer(player);	
		if(lobbyPlayer == null){
			return buyableItem;
		}
		
		if(perk.hasUnlockedPerk(lobbyPlayer)){
			return boughtItem;
		}else if(perk.hasRequiredPerk(lobbyPlayer)){
			return buyableItem;
		}else{
			return unbuyableItem;
		}
	}

}
