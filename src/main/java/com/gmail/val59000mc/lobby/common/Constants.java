package com.gmail.val59000mc.lobby.common;

import com.gmail.val59000mc.lobby.flowdispatcher.FlowPair;
import com.google.common.collect.Lists;

import java.util.List;

public class Constants {
	public final static double VIP_MONTH_PRICE = 30000;
	public final static double VIP_PLUS_MONTH_PRICE = 50000;
	public final static List<String> SCOREBOARD_LOADING_PAGE = Lists.newArrayList(
		"§7Chargement de tes",
		"§7statistiques en cours",
		"§7..."
	);
	
	public static final int SCOREBOARD_ROTATION_SECONDS = 10;

	// perks
	
	public static String SHOP_GAMES = "shop-games";
	
	public static String SHOP_KILL_SKILL = "shop-killskill";
	public static String SHOP_KILL_SKILL_EXPERIENCE = "shop-killskill-exp";
	public static String SHOP_KILL_SKILL_LAPIS = "shop-killskill-lapis";
	public static String SHOP_KILL_SKILL_POTION = "shop-killskill-potion";
	public static String SHOP_KILL_SKILL_IRON = "shop-killskill-iron";
	public static String SHOP_KILL_SKILL_DIAMOND = "shop-killskill-diamond";
	public static String SHOP_KILL_SKILL_BLAZE = "shop-killskill-ingredient";
	public static String SHOP_KILL_SKILL_SUPPLY_AMOUNT = "shop-killskill-supply-amount";
	public static String SHOP_KILL_SKILL_SUPPLY_LEVEL = "shop-killskill-supply-level";
	public static String SHOP_KILL_SKILL_ENDERCHEST = "shop-killskill-enderchest";

	public static String SHOP_EVOLUTION = "shop-evolution";
	public static String SHOP_EVOLUTION_STAR = "shop-evolution-star";
	
	public static String SHOP_CTF = "shop-ctf";
	public static String SHOP_CTF_TNT_AMOUNT = "shop-ctf-tnt";
	public static String SHOP_CTF_TNT_POWER = "shop-ctf-tntpower";
	public static String SHOP_CTF_TNT_PROTECTION = "shop-ctf-tntprotection";
	
	public static String SHOP_GLADIATOR= "shop-gladiator";
	public static String SHOP_GLADIATOR_GLADIATOR_HALF_HEART= "shop-gladiator-heart";
	public static String SHOP_GLADIATOR_ENCHANT_CHANCE= "shop-gladiator-enchant";
	public static String SHOP_GLADIATOR_SLAVE_BOW_CHANCE= "shop-gladiator-bow";
	public static String SHOP_GLADIATOR_SLAVE_EXTRA_CHANCE= "shop-gladiator-extra";
	public static String SHOP_GLADIATOR_SLAVE_POTION_CHANCE= "shop-gladiator-potion";
	
	public static String SHOP_SURVIVAL_RUSH= "shop-survivalrush";
	public static String SHOP_SURVIVAL_RUSH_TOOLS= "shop-survivalrush-tools";
	public static String SHOP_SURVIVAL_RUSH_IRON_LOOT= "shop-survivalrush-ironloot";
	public static String SHOP_SURVIVAL_RUSH_SKELETON_PER_TNT= "shop-survivalrush-skeletonspertnt";
	public static String SHOP_SURVIVAL_RUSH_NBR_BLOCKS_RESPAWN= "shop-survivalrush-nbblocksrespawn";

	public static String SHOP_RAGE= "shop-rage";
	public static String SHOP_RAGE_XP_SPEED= "shop-rage-xpspeed";
	public static String SHOP_RAGE_ARROWS= "shop-rage-arrows";
	public static String SHOP_RAGE_GADGETS= "shop-rage-gadgets";

	// statistics inventores
	public static final String STATISTICS_MAIN_INVENTORY = "statistics-main-inventory";
	public static final long STATISTICS_CACHE_EXPIRATION_MILLIS = 300000L;
	public static final long STATISTICS_ACCESS_EXPIRATION_MILLIS = 60000L;
	
	public static final String LEFT_ARROW = "http://textures.minecraft.net/texture/737648ae7a564a5287792b05fac79c6b6bd47f616a559ce8b543e6947235bce";
	public static final String LEFT_GRAYED_ARROW = "http://textures.minecraft.net/texture/bb0f6e8af46ac6faf88914191ab66f261d6726a7999c637cf2e4159fe1fc477";
	public static final String RIGHT_ARROW = "http://textures.minecraft.net/texture/1a4f68c8fb279e50ab786f9fa54c88ca4ecfe1eb5fd5f0c38c54c9b1c7203d7a";
	public static final String RIGHT_GRAYED_ARROW = "http://textures.minecraft.net/texture/f2f3a2dfce0c3dab7ee10db385e5229f1a39534a8ba2646178e37c4fa93b";
	public static final String DEAD_SKULL = "http://textures.minecraft.net/texture/1ae3855f952cd4a03c148a946e3f812a5955ad35cbcb52627ea4acd47d3081";
	public static final String CLOCK = "http://textures.minecraft.net/texture/6fe8cff75f7d433260af1ecb2f773b4bc381d951de4e2eb661423779a590e72b";
	public static final String CHECKBOARD = "http://textures.minecraft.net/texture/f870a65992c2dd8ad738b98d6d3596e45a2dd14efbace4b21122aeaff777f5";
	public static final String TARGET = "http://textures.minecraft.net/texture/86fcaefa19669d8be02cf5ba9a7f2cf6d27e636410496ffcfa62b03dceb9d378";
	
	public static final int KILLS_FILLTER_POSITION = 46;
	public static final int DEATHS_FILLTER_POSITION = 47;
	public static final int RATIO_FILLTER_POSITION = 48;
	public static final int COINS_FILLTER_POSITION = 49;
	public static final int PLAY_TIME_FILLTER_POSITION = 50;
	public static final int WINS_FILLTER_POSITION = 51;
	
	public static short GREEN_PANE = (short) 5;
	public static short WHITE_PANE = (short) 0;
	
	// sponsors
	public static final int SPONSORS_CONFIRMATION_DELAY_SECONDS = 432000;
	public static final int SPONSORS_CONFIRMATION_KILLS = 100;
	
	// kill skill dispatcher
	public static final String GLOBAL_CONFIG_KILL_SKILL_SERVERS_KEY = "kill-skill-dispatcher.servers";
	public static final String GLOBAL_CONFIG_KILL_SKILL_DEFAULT_SERVERS = "KillSkill1,KillSkill2";
	public static final String GLOBAL_CONFIG_KILL_SKILL_CURRENT_SERVER_KEY = "kill-skill-dispatcher.current-server";
	
	// flow dispatcher
	public static final String GLOBAL_CONFIG_FLOW_MAP_KEY(String serverName){ return "flow-dispatcher."+serverName; }
	public static final String GLOBAL_CONFIG_FLOW_PAIR_KEY(FlowPair flowPair){ 
		return "flow-dispatcher."
				+flowPair.getFirst().getServer()
				+flowPair.getSecond().getServer();
	}
	public static final String GLOBAL_CONFIG_FLOW_UNKNOWN_GAME = "Jeu inconnu";
	public static final long DEFAULT_FLOW_TIMEOUT_SECONDS = 2700;
	public static final boolean DEFAULT_FLOW_ENABLED = false;
	
	
}
