package com.gmail.val59000mc.lobby.players;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import org.bukkit.entity.Player;

public class LobbyPlayer extends HCPlayer{

	///////////////
	// PERKS
	////////////////
	
	// Kill Skill
	public int killSkillExperienceAmount;
	public int killSkillLapisChance;
	public int killSkillPotionChance;
	public int killSkillIronChance;
	public int killSkillDiamondChance;
	public int killSkillBlazeChance;
	public int killSkillSupplyAmount;
	public int killSkillSupplyLevel;
    public int killSkillEnderchestRows;
	
	// Evolution
	
	public int evoDoubleStarsChance;
	
	// CTF

	public double ctfTntPower;
	public double ctfTntProtection;
	public int ctfTntAmount;
	
	// Gladiator

	public int gladiatorGladiatorHalfHearts;
	public int gladiatorSlaveBowChance;
	public int gladiatorSlaveExtraChance;
	public int gladiatorSlavePotionChance;
	public int gladiatorEnchantChance;
	
	// Survival Rush

	public boolean survivalRushHasIronPickaxe;
	public boolean survivalRushSwordLooting;
	public int survivalRushIronLootChance;
	public int survivalRushSkeletonsPerTNT;
	public int survivalRushNbrBlocksRespawn;

	// Rage
    public int rageArrowsPerSpawn;
    public int rageXpRegainSpeed;
    public int rageGagdetsLevel;

	
	//////////////
	// STATS
	//////////////
	
	// stats all
	public int globalKills;
	public int globalDeaths;
	public int globalPlayTime;
	public double globalCoinsEarned;
	public int globalWins;
	
	public int monthKills;
	public int monthDeaths;
	public int monthPlayTime;
	public double monthCoinsEarned;
	public int monthWins;
	
	// lobby

	public int globalLobbyPlayTime;
	
	public int monthLobbyPlayTime;
	
	// stats kill skill
	public int globalKillSkillKills;
	public int globalKillSkillDeaths;
	public int globalKillSkillPlayTime;
	public double globalKillSkillCoinsEarned;
	public int globalKillSkillExperienceLooted;
	public int globalKillSkillLapisLooted;
	public int globalKillSkillPotionLooted;
	public int globalKillSkillIronLooted;
	public int globalKillSkillDiamondLooted;
	public int globalKillSkillBlazeLooted;
	public int globalKillSkillHeadLooted;
	public int globalKillSkillGrenadeLooted;
	public int globalKillSkillTntLooted;
	public int globalKillSkillCannonLooted;
	public int globalKillSkillSupplyOpened;

	public int monthKillSkillKills;
	public int monthKillSkillDeaths;
	public int monthKillSkillPlayTime;
	public double monthKillSkillCoinsEarned;
	public int monthKillSkillExperienceLooted;
	public int monthKillSkillLapisLooted;
	public int monthKillSkillPotionLooted;
	public int monthKillSkillIronLooted;
	public int monthKillSkillDiamondLooted;
	public int monthKillSkillBlazeLooted;
	public int monthKillSkillHeadLooted;
	public int monthKillSkillGrenadeLooted;
	public int monthKillSkillTntLooted;
	public int monthKillSkillCannonLooted;
	public int monthKillSkillSupplyOpened;
	
	// stats survival rush
	public int globalSurvivalRushKills;
	public int globalSurvivalRushDeaths;
	public int globalSurvivalRushPlayTime;
	public double globalSurvivalRushCoinsEarned;
	public int globalSurvivalRushWins;
	public int globalSurvivalRushSkeletonsKilled;
	public int globalSurvivalRushBlocksPlaced;
	public int globalSurvivalRushIronMined;
	public int globalSurvivalRushDiamondMined;
	public int globalSurvivalRushGoldMined;
	public int globalSurvivalRushGravelMined;
	
	public int monthSurvivalRushKills;
	public int monthSurvivalRushDeaths;
	public int monthSurvivalRushPlayTime;
	public double monthSurvivalRushCoinsEarned;
	public int monthSurvivalRushWins;
	public int monthSurvivalRushSkeletonsKilled;
	public int monthSurvivalRushBlocksPlaced;
	public int monthSurvivalRushIronMined;
	public int monthSurvivalRushDiamondMined;
	public int monthSurvivalRushGoldMined;
	public int monthSurvivalRushGravelMined;
	
	// stats evolution
	public int globalEvolutionKills;
	public int globalEvolutionDeaths;
	public int globalEvolutionPlayTime;
	public double globalEvolutionCoinsEarned;
	public int globalEvolutionWins;
	public int globalEvolutionStarsEarned;
	
	public int monthEvolutionKills;
	public int monthEvolutionDeaths;
	public int monthEvolutionPlayTime;
	public double monthEvolutionCoinsEarned;
	public int monthEvolutionWins;
	public int monthEvolutionStarsEarned;
	
	// stats ctf
	public int globalCtfKills;
	public int globalCtfDeaths;
	public int globalCtfPlayTime;
	public double globalCtfCoinsEarned;
	public int globalCtfWins;
	public int globalCtfFlagsTaken;
	public int globalCtfFlagsSaved;
	
	public int monthCtfKills;
	public int monthCtfDeaths;
	public int monthCtfPlayTime;
	public double monthCtfCoinsEarned;
	public int monthCtfWins;
	public int monthCtfFlagsTaken;
	public int monthCtfFlagsSaved;
	
	// stats gladiator
	public int globalGladiatorKills;
	public int globalGladiatorDeaths;
	public int globalGladiatorPlayTime;
	public double globalGladiatorCoinsEarned;
	public int globalGladiatorGladiatorsKilled;
	public int globalGladiatorSlavesKilled;
	public int globalGladiatorGladiatorWins;
	public int globalGladiatorSlaveWins;
	
	public int monthGladiatorKills;
	public int monthGladiatorDeaths;
	public int monthGladiatorPlayTime;
	public double monthGladiatorCoinsEarned;
	public int monthGladiatorGladiatorsKilled;
	public int monthGladiatorSlavesKilled;
	public int monthGladiatorGladiatorWins;
	public int monthGladiatorSlaveWins;

    // stats rage
    public int globalRageKills;
    public int globalRageDeaths;
    public int globalRagePlayTime;
    public double globalRageCoinsEarned;
    public int globalRageWins;
    public int globalRageBestKillStreak;
    public int globalRageTier1Gadgets;
    public int globalRageTier2Gadgets;
    public int globalRageTier3Gadgets;
    public int globalRageDoubleJumps;

    public int monthRageKills;
    public int monthRageDeaths;
    public int monthRagePlayTime;
    public double monthRageCoinsEarned;
    public int monthRageWins;
    public int monthRageBestKillStreak;
    public int monthRageTier1Gadgets;
    public int monthRageTier2Gadgets;
    public int monthRageTier3Gadgets;
    public int monthRageDoubleJumps;

    // stats tdm
    public int globalTdmKills;
    public int globalTdmDeaths;
    public int globalTdmPlayTime;
    public double globalTdmCoinsEarned;
    public int globalTdmWins;

    public int monthTdmKills;
    public int monthTdmDeaths;
    public int monthTdmPlayTime;
    public double monthTdmCoinsEarned;
    public int monthTdmWins;
		
	
	public LobbyPlayer(Player player) {
		super(player);
		init();	
	}
	
	private void init(){
		
		///////////////
		// perks
		//////////////
		
		// Kill Skill
		this.killSkillExperienceAmount = 1;
		this.killSkillLapisChance = 5;
		this.killSkillPotionChance = 20;
		this.killSkillIronChance = 5;
		this.killSkillDiamondChance = 5;
		this.killSkillBlazeChance = 6;
		this.killSkillSupplyAmount = 2;
		this.killSkillSupplyLevel = 1;
        this.killSkillEnderchestRows = 6;
		
		// Evolution
		
		this.evoDoubleStarsChance = 0;
		
		// CTF

		this.ctfTntPower = 20;
		this.ctfTntProtection = 0;
		this.ctfTntAmount = 1;
		
		// Gladiator

		this.gladiatorGladiatorHalfHearts = 40;
		this.gladiatorSlaveBowChance = 20;
		this.gladiatorSlaveExtraChance = 20;
		this.gladiatorSlavePotionChance = 20;
		this.gladiatorEnchantChance = 20;
		
		// Survival Rush

		this.survivalRushHasIronPickaxe = false;
		this.survivalRushSwordLooting = false;
		this.survivalRushIronLootChance = 5;
		this.survivalRushSkeletonsPerTNT = 50;
		this.survivalRushNbrBlocksRespawn = 0;

        // Rage
        this.rageArrowsPerSpawn = 2;
        this.rageXpRegainSpeed = 1;
        this.rageGagdetsLevel = 1;
		
		//////////////
		// stats
		//////////////
		
		// stats all
		this.globalKills = 0;
		this.globalDeaths = 0;
		this.globalPlayTime = 0;
		this.globalCoinsEarned = 0;
		this.globalWins = 0;
		
		this.monthKills = 0;
		this.monthDeaths = 0;
		this.monthPlayTime = 0;
		this.monthCoinsEarned = 0;
		this.monthWins = 0;
		
		//lobby
		this.globalLobbyPlayTime = 0;
		
		this.monthLobbyPlayTime = 0;
		
		// stats kill skill
		this.globalKillSkillKills = 0;
		this.globalKillSkillDeaths = 0;
		this.globalKillSkillPlayTime = 0;
		this.globalKillSkillCoinsEarned = 0;
		this.globalKillSkillExperienceLooted = 0;
		this.globalKillSkillLapisLooted = 0;
		this.globalKillSkillPotionLooted = 0;
		this.globalKillSkillIronLooted = 0;
		this.globalKillSkillDiamondLooted = 0;
		this.globalKillSkillBlazeLooted = 0;
		this.globalKillSkillHeadLooted = 0;
		this.globalKillSkillGrenadeLooted = 0;
		this.globalKillSkillTntLooted = 0;
		this.globalKillSkillCannonLooted = 0;
		this.globalKillSkillSupplyOpened = 0;

		this.monthKillSkillKills = 0;
		this.monthKillSkillDeaths = 0;
		this.monthKillSkillPlayTime = 0;
		this.monthKillSkillCoinsEarned = 0;
		this.monthKillSkillExperienceLooted = 0;
		this.monthKillSkillLapisLooted = 0;
		this.monthKillSkillPotionLooted = 0;
		this.monthKillSkillIronLooted = 0;
		this.monthKillSkillDiamondLooted = 0;
		this.monthKillSkillBlazeLooted = 0;
		this.monthKillSkillHeadLooted = 0;
		this.monthKillSkillGrenadeLooted = 0;
		this.monthKillSkillTntLooted = 0;
		this.monthKillSkillCannonLooted = 0;
		this.monthKillSkillSupplyOpened = 0;
		
		// stats survival rush
		this.globalSurvivalRushKills = 0;
		this.globalSurvivalRushDeaths = 0;
		this.globalSurvivalRushPlayTime = 0;
		this.globalSurvivalRushCoinsEarned = 0;
		this.globalSurvivalRushWins = 0;
		this.globalSurvivalRushSkeletonsKilled = 0;
		this.globalSurvivalRushBlocksPlaced = 0;
		this.globalSurvivalRushIronMined = 0;
		this.globalSurvivalRushDiamondMined = 0;
		this.globalSurvivalRushGoldMined = 0;
		this.globalSurvivalRushGravelMined = 0;
		
		this.monthSurvivalRushKills = 0;
		this.monthSurvivalRushDeaths = 0;
		this.monthSurvivalRushPlayTime = 0;
		this.monthSurvivalRushCoinsEarned = 0;
		this.monthSurvivalRushWins = 0;
		this.monthSurvivalRushSkeletonsKilled = 0;
		this.monthSurvivalRushBlocksPlaced = 0;
		this.monthSurvivalRushIronMined = 0;
		this.monthSurvivalRushDiamondMined = 0;
		this.monthSurvivalRushGoldMined = 0;
		this.monthSurvivalRushGravelMined = 0;
		
		// stats evolution
		this.globalEvolutionKills = 0;
		this.globalEvolutionDeaths = 0;
		this.globalEvolutionPlayTime = 0;
		this.globalEvolutionCoinsEarned = 0;
		this.globalEvolutionWins = 0;
		this.globalEvolutionStarsEarned = 0;
		
		this.monthEvolutionKills = 0;
		this.monthEvolutionDeaths = 0;
		this.monthEvolutionPlayTime = 0;
		this.monthEvolutionCoinsEarned = 0;
		this.monthEvolutionWins = 0;
		this.monthEvolutionStarsEarned = 0;
		
		// stats ctf
		this.globalCtfKills = 0;
		this.globalCtfDeaths = 0;
		this.globalCtfPlayTime = 0;
		this.globalCtfCoinsEarned = 0;
		this.globalCtfWins = 0;
		this.globalCtfFlagsTaken = 0;
		this.globalCtfFlagsSaved = 0;
		
		this.monthCtfKills = 0;
		this.monthCtfDeaths = 0;
		this.monthCtfPlayTime = 0;
		this.monthCtfCoinsEarned = 0;
		this.monthCtfWins = 0;
		this.monthCtfFlagsTaken = 0;
		this.monthCtfFlagsSaved = 0;
		
		// stats gladiator
		this.globalGladiatorKills = 0;
		this.globalGladiatorDeaths = 0;
		this.globalGladiatorPlayTime = 0;
		this.globalGladiatorCoinsEarned = 0;
		this.globalGladiatorGladiatorsKilled = 0;
		this.globalGladiatorSlavesKilled = 0;
		this.globalGladiatorGladiatorWins = 0;
		this.globalGladiatorSlaveWins = 0;
		
		this.monthGladiatorKills = 0;
		this.monthGladiatorDeaths = 0;
		this.monthGladiatorPlayTime = 0;
		this.monthGladiatorCoinsEarned = 0;
		this.monthGladiatorGladiatorsKilled = 0;
		this.monthGladiatorSlavesKilled = 0;
		this.monthGladiatorGladiatorWins = 0;
		this.monthGladiatorSlaveWins = 0;

        // stats rage
        this.globalRageKills = 0;
        this.globalRageDeaths = 0;
        this.globalRagePlayTime = 0;
        this.globalRageCoinsEarned = 0;
        this.globalRageWins = 0;
        this.globalRageBestKillStreak = 0;
        this.globalRageTier1Gadgets = 0;
        this.globalRageTier2Gadgets = 0;
        this.globalRageTier3Gadgets = 0;
        this.globalRageDoubleJumps = 0;

        this.monthRageKills = 0;
        this.monthRageDeaths = 0;
        this.monthRagePlayTime = 0;
        this.monthRageCoinsEarned = 0;
        this.monthRageWins = 0;
        this.monthRageBestKillStreak = 0;
        this.monthRageTier1Gadgets = 0;
        this.monthRageTier2Gadgets = 0;
        this.monthRageTier3Gadgets = 0;
        this.monthRageDoubleJumps = 0;

        // stats tdm
        this.globalTdmKills = 0;
        this.globalTdmDeaths = 0;
        this.globalTdmPlayTime = 0;
        this.globalTdmCoinsEarned = 0;
        this.globalTdmWins = 0;

        this.monthTdmKills = 0;
        this.monthTdmDeaths = 0;
        this.monthTdmPlayTime = 0;
        this.monthTdmCoinsEarned = 0;
        this.monthTdmWins = 0;
	}
	
	public LobbyPlayer(HCPlayer hcPlayer) {
		super(hcPlayer);
	}
	
	public void subtractBy(HCPlayer lastUpdatedSession) {
		super.subtractBy(lastUpdatedSession);
	}
	
	// 
	/**
	 * Light constructor forinventory stats
	 * 
	 *  HOULD NOT BE USED IN PLAYERS MANAGER !!!!
	 *  
	 * @param name
	 */
	public LobbyPlayer(String name){
		super();
		this.name = name;
		init();
	}
	
	
}
