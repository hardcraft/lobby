package com.gmail.val59000mc.lobby;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.*;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.listeners.*;
import com.gmail.val59000mc.lobby.callbacks.LobbyCallbacks;
import com.gmail.val59000mc.lobby.commands.*;
import com.gmail.val59000mc.lobby.listeners.*;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.google.common.collect.Sets;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lobby extends JavaPlugin{

	public void onEnable(){

		this.getDataFolder().mkdirs();
		File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(),"config.yml"));
		File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(),"lang.yml"));
		
		HCCommand vanishCmd = new LobbyVanishCommand(this);

		HCGameAPI game = new HCGame.Builder("Lobby", this, config, lang)
				.withPluginCallbacks(new LobbyCallbacks())
				.withListeners(Sets.newHashSet(
					new HCChatListener(),
					new HCMySQLListener(),
					new LobbyMySQLListener(),
					new LobbyBlocksListener(),
					new LobbyDamageAndInteractListener(),
					new HCPingListener(),
					new HCPlayerConnectionListener(),
					new HCPlayerDeathListener(),
					new HCPlayerDamageListener(),
					new LobbyCommandOverrideListener(vanishCmd),
					new LobbySigListener(),
					new LobbyStartContinuousListener(),
					new HCBoosterListener(),
					new HCAFKListener()
				))
				.withCommands(Sets.newHashSet(
					new LobbyBuildCommand(this),
					new LobbyFlyCommand(this),
					new LobbyRankCommand(this),
					new LobbySpawnCommand(this),
					new HCWhitelistCommand(HCGamesLib.getPlugin()),
					new LobbySponsorCommand(this),
					new LobbyKillSkillDispatcherCommand(this),
					new LobbyFlowDispatcherCommand(this),
					new HCKickAllCommand(HCGamesLib.getPlugin()),
					new HCSayCommand(HCGamesLib.getPlugin()),
					new HCBoosterCommand(HCGamesLib.getPlugin()),
					new HCDonationCommand(HCGamesLib.getPlugin()),
					new LobbyShopCommand(this)
				))
				.build();
		game.loadGame();
	}
	public void onDisable(){
	}
	
}
