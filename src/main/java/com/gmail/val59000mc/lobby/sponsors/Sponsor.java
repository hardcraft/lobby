package com.gmail.val59000mc.lobby.sponsors;

import java.time.LocalDateTime;
import java.util.UUID;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class Sponsor {
	
	private int id;
	private UUID uuid;
	private String name;
	private LocalDateTime expireAt;
	public Sponsor(HCPlayer hcPlayer, LocalDateTime expireAt) {
		super();
		this.id = hcPlayer.getId();
		this.uuid = hcPlayer.getUuid();
		this.name = hcPlayer.getName();
		this.expireAt = expireAt;
	}
	public int getId() {
		return id;
	}
	public UUID getUuid() {
		return uuid;
	}
	public LocalDateTime getExpireAt() {
		return expireAt;
	}
	public String getName() {
		return name;
	}
	
	
}
