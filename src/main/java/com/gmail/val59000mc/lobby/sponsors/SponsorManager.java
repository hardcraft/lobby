package com.gmail.val59000mc.lobby.sponsors;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.rowset.CachedRowSet;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Time;

public class SponsorManager extends HCListener{

	private Map<String, Sponsor> sponsored;
	private HCGameAPI api;
	
	private String insertPlayerSponsor;
	private String selectConfirmedSponsoredPlayersBySponsor;
	private String selectSponsoredPlayersBySponsorAndDates;
	private String updateConfirmedSponsoredPlayer;
	private String updateUnconfirmedSponsoredPlayers;
	private String updateInvalidSponsoredPlayers;
	
	public SponsorManager(HCGameAPI api){
		this.api = api;
		this.sponsored = new ConcurrentHashMap<String, Sponsor>(20,0.75f,2);  
		if(api.getMySQLAPI().isEnabled()){
			HCMySQLAPI sql = api.getMySQLAPI();
			insertPlayerSponsor = sql.readQueryFromResource(api.getPlugin(), "sql/sponsor/insert_player_sponsor.sql");
			selectSponsoredPlayersBySponsorAndDates = sql.readQueryFromResource(api.getPlugin(), "sql/sponsor/select_sponsored_players_by_sponsor_and_dates.sql");
			selectConfirmedSponsoredPlayersBySponsor = sql.readQueryFromResource(api.getPlugin(), "sql/sponsor/select_confirmed_sponsored_players_by_sponsor.sql");
			updateConfirmedSponsoredPlayer = sql.readQueryFromResource(api.getPlugin(), "sql/sponsor/update_confirmed_sponsored_player.sql");
			updateUnconfirmedSponsoredPlayers = sql.readQueryFromResource(api.getPlugin(), "sql/sponsor/update_unconfirmed_sponsored_players.sql");
			updateInvalidSponsoredPlayers = sql.readQueryFromResource(api.getPlugin(), "sql/sponsor/update_invalid_sponsored_players.sql");
			startSchedulers();
		}
	}
	
	@EventHandler
	public void afterPlayerInserted(HCPlayerDBInsertedEvent e){
		
		// check if player has been sponsored
		checkCreateUnconfirmedSponsorships(e.getHcPlayer());
		
		// check if player has CONFIRMED sponsorships
		checkHasConfirmedSponsorships(e.getHcPlayer());
	}
	
	public void printWeekSponsoredByPlayer(HCPlayer hcPlayer){
		if(hcPlayer.getId() == 0){
			return; // deny sponsor usage when id is not set
		}
		
		Map<SponsoringState,String> outputStates = new HashMap<SponsoringState,String>();
		outputStates.put(SponsoringState.UNCONFIRMED, api.getStringsAPI().get("lobby.sponsors.state."+SponsoringState.UNCONFIRMED.name()).toString());
		outputStates.put(SponsoringState.CONFIRMED, api.getStringsAPI().get("lobby.sponsors.state."+SponsoringState.CONFIRMED.name()).toString());
		outputStates.put(SponsoringState.VALIDATED, api.getStringsAPI().get("lobby.sponsors.state."+SponsoringState.VALIDATED.name()).toString());
		outputStates.put(SponsoringState.INVALID, api.getStringsAPI().get("lobby.sponsors.state."+SponsoringState.INVALID.name()).toString());
		
		api.async(()->{
			
			LocalDateTime now = LocalDateTime.now();
			
			LocalDateTime startOfMonth = getFirstDayOfMonth();
			
			HCMySQLAPI sql = api.getMySQLAPI();
			CachedRowSet res;
			try{
				res = sql.query(sql.prepareStatement(selectSponsoredPlayersBySponsorAndDates,
					String.valueOf(hcPlayer.getId()),
					startOfMonth.toString(),
					now.toString()
				));
			}catch(SQLException e){
				Logger.severe("Could not load sponsored players by sponsor and dates, sponsor="+hcPlayer.getName()+" uuid="+hcPlayer.getUuid()+" start="+startOfMonth+" end="+now);
				e.printStackTrace();
				res = null;
			}
			
			boolean next = false;
			try {
				next = res.first();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if(!next){
				api.sync(()->{
					api.getStringsAPI()
						.get("lobby.sponsors.sponsored-this-month-empty")
						.sendChatP(hcPlayer);
				});
			}else{
				
				List<String> output = new ArrayList<String>();
				
				try {
					while(next){
						SponsoringState state = SponsoringState.valueOf(res.getString("state"));
						
						output.add(outputStates.get(state)
							.replace("%player%", res.getString("name"))
							.replace("%kills%", String.valueOf(res.getInt("kills"))
						));
						
						next = res.next();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				api.sync(()->{
					HCStringsAPI s = api.getStringsAPI();
					s.get("lobby.sponsors.sponsored-this-month")
						.sendChatP(hcPlayer);
					for(String out : output){
						s.getRaw(out).sendChat(hcPlayer);
					}
				});
			}
			
		});
		
	}
	
	public void checkHasConfirmedSponsorships(HCPlayer hcPlayer){
		// dont validate sponsoring if sponsor is not online
		if(!hcPlayer.isOnline())
			return;
			
		api.async(()->{
		
			HCMySQLAPI sql = api.getMySQLAPI();
					
			CachedRowSet res;
			
			try{
				res = sql.query(sql.prepareStatement(selectConfirmedSponsoredPlayersBySponsor,
					String.valueOf(hcPlayer.getId())	
				));
			}catch(SQLException e){
				Logger.severe("Could not load confirmed sponsored players by sponsor="+hcPlayer.getName()+" uuid="+hcPlayer.getUuid());
				e.printStackTrace();
				return;
			}
			
			List<String> confirmedSponsoredPlayerNames = new ArrayList<String>();

			try{
				boolean next = res.first();
				if(!next) return;
				
				
				while(next){
					confirmedSponsoredPlayerNames.add(res.getString("name"));
					
					// validate sponsored player
					sql.execute(sql.prepareStatement(updateConfirmedSponsoredPlayer, String.valueOf(res.getInt("sponsored_player_id"))));
					next = res.next();
				}
			}catch (Exception e) {
				Logger.severe("Could not load update confirmed sponsored player");
				e.printStackTrace();
				return;
			}
			
			api.sync(()->{
			
			FileConfiguration cfg = api.getConfig();
			double validatedReward = cfg.getDouble("sponsors.rewards.validated",0);
			int kills = cfg.getInt("sponsors.confirmation-kills",Constants.SPONSORS_CONFIRMATION_KILLS);
			
			
				// reward money to sponsor and display message for each new validated sponsored player
				for(String name : confirmedSponsoredPlayerNames){
			
					api.getDependencies().addMoney(Bukkit.getOfflinePlayer(hcPlayer.getUuid()), validatedReward);
					api.getStringsAPI()
						.get("lobby.sponsors.now-validated")
						.replace("%player%", name)
						.replace("%kills%", String.valueOf(kills))
						.replace("%reward%", String.valueOf(validatedReward))
						.sendChatP(hcPlayer);
				}
				
				api.getSoundAPI().play(hcPlayer, Sound.LEVEL_UP,1,2);
			});
			
		});
	}	
	
	public void checkCreateUnconfirmedSponsorships(HCPlayer hcPlayer){
								
		if(sponsored.containsKey(hcPlayer.getName())){
			Sponsor sponsor = sponsored.get(hcPlayer.getName());
			LocalDateTime now = LocalDateTime.now();
			if(now.isAfter(sponsor.getExpireAt())){
				msgJoinHasExpired(sponsor.getUuid(), hcPlayer.getName());
				sponsored.remove(hcPlayer.getName());
			}else{
				api.async(()->{
				
					HCMySQLAPI sql = api.getMySQLAPI();

					try {
						CachedRowSet res = sql.selectPlayerByUUID(hcPlayer.getUuid().toString());
						Logger.debug("selectPlayerByUUID uuid="+hcPlayer.getUuid().toString());
						if(res.first()){
							Logger.debug("found");
							int playTime = res.getInt("play_time");
							if(playTime == 0){
								// sponsored player is legit and has not changed name
								sql.execute(sql.prepareStatement(insertPlayerSponsor, String.valueOf(sponsor.getId()), String.valueOf(hcPlayer.getId())));
								
								api.sync(()->{
									msgAndRewardUnconfirmedSponsorship(sponsor, hcPlayer);
									sponsored.remove(hcPlayer.getName());
								});
								
							}else{
								// sponsored player tried to cheat and has changed name
								api.sync(()->{
									msgChangedNameToGetSponsorship(sponsor, hcPlayer);
									sponsored.remove(hcPlayer.getName());
								});
							}
						}else{
							Logger.debug("not found");
						}
					} catch (Exception e) {
						Logger.severe("Could not check create unconfirmed sponsorships");
						e.printStackTrace();
					}
					
				});
			}
		}
	}

	public void addWaitingSponsor(HCPlayer hcPlayer, String playerName){
		if(hcPlayer.getId() == 0){
			return; // deny sponsor usage when id is not set
		}
		if(sponsored.containsKey(playerName)){
			msgAlreadyWaitingJoin(hcPlayer, playerName);
		}else{
			long count = sponsored.entrySet().
				stream()
				.filter((entry) -> entry.getValue().getUuid().equals(hcPlayer.getUuid()))
				.count();
			if(count == 3){
				msgMaxAllowedWaitingSponsors(hcPlayer);
			}else{
				Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), () ->{
					
					try{
						CachedRowSet res = api.getMySQLAPI().selectPlayerByName(playerName);
						Bukkit.getScheduler().runTask(api.getPlugin(), () ->{
							
							
							boolean found = true;
							try {
								found = res.first();
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							if(sponsored.containsKey(playerName)){
								// playerName already waiting for connection
								msgAlreadyWaitingJoin(hcPlayer, playerName);
							}else if(!found){
								// playerName can be sponsored
								sponsored.put(playerName, new Sponsor(hcPlayer, LocalDateTime.now().plusMinutes(10)));
								msgNowWaitingForJoin(hcPlayer, playerName);
							}else{
								// playerName already Known
								msgCannotSponsorKnownPlayer(hcPlayer,playerName);
							}
							
						});
					}catch(SQLException e){
						Logger.severe("Could not add waiting sponsor for hcPlayer="+hcPlayer.getName()+" uuid="+hcPlayer.getUuid());
						api.getStringsAPI()
							.getRaw("&cUne erreur est survenue lors de l'ajout du sponsor, merci de contacter un administrateur.")
							.sendChatP(hcPlayer);
						e.printStackTrace();
					}
					
				});
			}
		}
	}



	public void schedulerCleanExpiredSponsorships() {
		
		LocalDateTime now = LocalDateTime.now();
		
		Iterator<Entry<String,Sponsor>> it = sponsored.entrySet().iterator();
		while(it.hasNext()){
			Entry<String, Sponsor> entry = it.next();
			if(now.isAfter(entry.getValue().getExpireAt())){
				msgJoinHasExpired(entry.getValue().getUuid(), entry.getKey());
				it.remove();
			}
		}
	}
	
	

		public void schedulerhandleConfirmedAndInvalidSponsorships() {
			
			int goal = api.getConfig().getInt("sponsors.confirmation-kills",Constants.SPONSORS_CONFIRMATION_KILLS);
			long confirmationDelaySeconds = api.getConfig().getLong("sponsors.confirmation-delay-seconds",Constants.SPONSORS_CONFIRMATION_DELAY_SECONDS);
			
			api.async(()->{
				HCMySQLAPI sql = api.getMySQLAPI();
				
				try{
					
					// set UNCONFIRMED sponsored players to CONFIRMED if they have reached the kills goal
					sql.execute(sql.prepareStatement(
						updateUnconfirmedSponsoredPlayers, 
						String.valueOf(goal), 
						LocalDateTime.now().minusSeconds(confirmationDelaySeconds).toString(), 
						LocalDateTime.now().toString()
					));
					
					// set INVALID sponsored players to UNCONFIRMES if they are still unconfirmed after the confirmation delay
					sql.execute(sql.prepareStatement(
						updateInvalidSponsoredPlayers, 
						LocalDateTime.now().minusSeconds(confirmationDelaySeconds).toString()
					));
					
					// check if CONFIRMED sponsorships for online players
					api.sync(()->{
						for(HCPlayer hcPlayer : api.getPlayersManagerAPI().getPlayers(true, null)){
							checkHasConfirmedSponsorships(hcPlayer);
						}
					});
				
				}catch(SQLException e){
					Logger.severe("Could not process scheduled confirmed and invalid sponsorships");
					e.printStackTrace();
				}
			});
		}
					
			
	public void startSchedulers(){
	
		api.buildTask("clean expired sponsorships", new HCTask() {
			
			@Override
			public void run() {
				schedulerCleanExpiredSponsorships();
			}
		})
		.withDelay(3000) // 2.5 min
		.withInterval(3000)
		.build()
		.start();
		
		
		api.buildTask("confirm player having reach kills goal", new HCTask() {
			
			@Override
			public void run() {
				schedulerhandleConfirmedAndInvalidSponsorships();
			}
		})
		.withDelay(24000) // 20 min
		.withInterval(24000) // 20 min
		.build()
		.start();
	}

	// rewards and messages
	
	private void msgAndRewardUnconfirmedSponsorship(Sponsor sponsor, HCPlayer sponsored) {		
		
		// message sponsored player
		api.getStringsAPI()
			.get("lobby.sponsors.sponsored-by")
			.replace("%sponsor%", sponsor.getName())
			.sendChatP(sponsored);
		api.getSoundAPI().play(sponsored, Sound.LEVEL_UP,1,2);
		
		// message and reward sponsor if online
		HCPlayer hcSponsor = api.getPlayersManagerAPI().getHCPlayer(sponsor.getUuid());
		if(api.getDependencies().isVaultEnabled()){
		
			FileConfiguration cfg = api.getConfig();
			double unconfirmedReward = cfg.getDouble("sponsors.rewards.unconfirmed",0);
			double validatedReward = cfg.getDouble("sponsors.rewards.validated",0);
			String time = Time.getFormattedTime(cfg.getLong("sponsors.confirmation-delay-seconds",Constants.SPONSORS_CONFIRMATION_DELAY_SECONDS));
			int kills = cfg.getInt("sponsors.confirmation-kills",Constants.SPONSORS_CONFIRMATION_KILLS);
			
			api.getDependencies().addMoney(Bukkit.getOfflinePlayer(sponsor.getUuid()), unconfirmedReward);
			
			if(hcSponsor != null){
				api.getStringsAPI()
					.get("lobby.sponsors.now-unconfirmed")
					.replace("%player%", sponsored.getName())
					.replace("%reward%", String.valueOf(unconfirmedReward))
					.sendChatP(hcSponsor);
				api.getStringsAPI()
					.get("lobby.sponsors.help-to-validate")
					.replace("%player%", sponsored.getName())
					.replace("%reward%", String.valueOf(validatedReward))
					.replace("%time%", time)
					.replace("%kills%", String.valueOf(kills))
					.sendChatP(hcSponsor);
				api.getSoundAPI().play(hcSponsor, Sound.LEVEL_UP,1,2);
			}
		}
		
	}
	
	
	private void msgChangedNameToGetSponsorship(Sponsor sponsor, HCPlayer sponsored) {
		HCPlayer hcSponsor = api.getPlayersManagerAPI().getHCPlayer(sponsor.getUuid());
		if(hcSponsor != null){
			api.getStringsAPI()
				.get("lobby.sponsors.tried-change-name")
				.replace("%player%", sponsored.getName())
				.sendChatP(hcSponsor);
		}	
	}
	
	private void msgMaxAllowedWaitingSponsors(HCPlayer hcPlayer) {
		api.getStringsAPI()
			.get("lobby.sponsors.max-allowed-waiting-sponsors")
			.sendChatP(hcPlayer);
	}
	
	private void msgNowWaitingForJoin(HCPlayer hcPlayer, String name){
		api.getStringsAPI()
		.get("lobby.sponsors.now-waiting-for-join")
		.replace("%name%", name)
		.sendChatP(hcPlayer);
	}
	
	private void msgCannotSponsorKnownPlayer(HCPlayer hcPlayer, String name){
		api.getStringsAPI()
			.get("lobby.sponsors.cannot-sponsor-known-player")
			.replace("%name%", name)
			.sendChatP(hcPlayer);
	}
	
	private void msgAlreadyWaitingJoin(HCPlayer hcPlayer, String name){
		api.getStringsAPI()
			.get("lobby.sponsors.already-waiting-join")
			.replace("%name%", name)
			.sendChatP(hcPlayer);
	}
	
	private void msgJoinHasExpired(UUID uuid, String name){
		HCPlayer hcPlayer = api.getPlayersManagerAPI().getHCPlayer(uuid);
		if(hcPlayer != null){
			api.getStringsAPI()
				.get("lobby.sponsors.join-has-expired")
				.replace("%name%", name)
				.sendChatP(hcPlayer);
		}
	}
	
	private LocalDateTime getFirstDayOfMonth(){
		return LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0);
	}
}
