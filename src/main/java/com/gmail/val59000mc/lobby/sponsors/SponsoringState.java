package com.gmail.val59000mc.lobby.sponsors;

public enum SponsoringState {
	UNCONFIRMED, // sponsored player has joined but not yet reached the sponsorship reward goal
	CONFIRMED, // sponsored player has reached the sponsorship reward goal but the reward has not been claimed by the sponsor yet
	VALIDATED, // sponsoring reward has been claimed by the sponsor
	INVALID; // goal not reached
}
