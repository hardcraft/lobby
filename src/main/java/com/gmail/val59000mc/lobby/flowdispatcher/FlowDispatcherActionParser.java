package com.gmail.val59000mc.lobby.flowdispatcher;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class FlowDispatcherActionParser extends ActionParser{

	private FlowDispatcher flowDispatcher;
	
	public FlowDispatcherActionParser(FlowDispatcher flowDispatcher) {
		this.flowDispatcher = flowDispatcher;
	}

	@Override
	public String getType() {
		return "flow-dispatcher";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String name = action.getString("flow");
		if(name == null){
			throw new ActionParseException("#The flow-dispatcher action '"+action.getName()+"' must provide a flow attribute");		
		}
		return new FlowDispatcherAction(flowDispatcher, name);
	}

	

}
