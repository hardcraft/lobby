package com.gmail.val59000mc.lobby.flowdispatcher;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class FlowDispatcherAction extends Action{

	private FlowDispatcher dispatcher;
	private String flowPairName;
	
	public FlowDispatcherAction(FlowDispatcher dispatcher, String flowPairName) {
		this.dispatcher = dispatcher;
		this.flowPairName =flowPairName;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		
		FlowPair flowPair = dispatcher.getFlowPairByPairName(flowPairName);
		dispatcher.dispatchPlayer(flowPair, player);
		
		executeNextAction(player, sigPlayer, true);
	}

}
