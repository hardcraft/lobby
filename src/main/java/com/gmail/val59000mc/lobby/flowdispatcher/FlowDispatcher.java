package com.gmail.val59000mc.lobby.flowdispatcher;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.spigotutils.Bungee;
import com.gmail.val59000mc.spigotutils.Parser;

public class FlowDispatcher {

	protected HCGameAPI api;
	private Map<String,FlowPair> flows;
	
	public FlowDispatcher(HCGameAPI api){
		this.api = api;
		this.flows = new HashMap<>();
	}
	
	private boolean isEnabled(){
		return api.getConfig().getBoolean("flow-dispatcher.enable", Constants.DEFAULT_FLOW_ENABLED);
	}
	public void load() {
		Log.info("Loading Flow dispatcher");
		
		if(!isEnabled()){
			Log.info("Flow dispatcher is disabled. Aborting");
			return;
		}
		
		api.sync(()->{
			
			World world = api.getWorldConfig().getWorld();
			FileConfiguration cfg = api.getConfig();
			
			// load server local flows config
			ConfigurationSection flowsCfgSection = cfg.getConfigurationSection("flow-dispatcher.flows");
			if(flowsCfgSection != null){
				Log.info("Reading flow-dispatcher.flows section");
				for(String key : flowsCfgSection.getKeys(false)){
					ConfigurationSection flowSection = flowsCfgSection.getConfigurationSection(key);
					if(flowSection != null){
						Log.info("Reading flow section "+key);
						String firstServer = flowSection.getString("first");
						String secondServer = flowSection.getString("second");
						String sign = flowSection.getString("sign");
						if(firstServer != null && secondServer != null && sign != null){
							FlowPair pair = new FlowPair(new Flow(firstServer), new Flow(secondServer), Parser.parseLocation(world, sign));
							flows.put(pair.getName(), pair);
							Log.info("registering flow pair "+pair.getName());
						}
					}
				}
			}
			
			updateFlowPairs();
		
		}, 5);
		
	}

	public FlowPair getFlowPairByPairName(String pairKey){
		for(String flowKey : flows.keySet()){
			if(flowKey.equals(pairKey)){
				return flows.get(flowKey);
			}
		}
		return null;
	}
	
	public FlowPair getFlowPairByServerName(String serverName){
		for(FlowPair flowPair : flows.values()){
			if(flowPair.getFirst().getServer().equals(serverName)
				|| flowPair.getSecond().getServer().equals(serverName)){
				return flowPair;
			}
		}
		return null;
	}
	
	public Flow getFlowByActiveName(FlowPair pair, String activeName){
		if(activeName == null)
			return null;
		else if(pair.getFirst().getServer().equals(activeName))
			return pair.getFirst();
		else if(pair.getSecond().getServer().equals(activeName))
			return pair.getSecond();
		else
			return null;
	}

	private void updateFlowPairsFromGlobalConfig(HCMySQLAPI sql) {
		
		synchronized (flows) {
			
			Log.info("update flow pairs from global config");
			
			try{
				
				for(FlowPair flowPair : flows.values()){
					
					String firstMap = sql.selectGlobalConfig(Constants.GLOBAL_CONFIG_FLOW_MAP_KEY(flowPair.getFirst().getServer()));
					String secondMap = sql.selectGlobalConfig(Constants.GLOBAL_CONFIG_FLOW_MAP_KEY(flowPair.getSecond().getServer()));
					String activeServerName = sql.selectGlobalConfig(Constants.GLOBAL_CONFIG_FLOW_PAIR_KEY(flowPair));
					Flow active = getFlowByActiveName(flowPair, activeServerName);
					
					if(firstMap == null || secondMap == null || activeServerName == null || active == null){
						flowPair.setActive(null);
						Log.info("Could not load flow pair "+flowPair.getName()+". Missing information in global config. Disabling it");
					}else{
						flowPair.getFirst().setMap(firstMap);
						flowPair.getSecond().setMap(secondMap);
						flowPair.setActive(active);
						Log.info("Flow pair "+flowPair.getName()+" loaded with first map ="+firstMap+" second map="+secondMap+" active="+flowPair.getActive().getServer());
					}
	
				}
			
			}catch(SQLException e){
				Log.severe("Could not update flow dispatcher from global config. See error below.");
				e.printStackTrace();
				return;
			}
			
		}
		
	}

	/**
	 * Update the remore global config
	 * Doesn't change the internal in-memory config
	 * Must be run asynchronously to avoid lag spike
	 * @return
	 */
	public void nextMap(String flowServer, String flowMap, String nextFlowServer) {
		
		Log.info("Setting next flow map flowServer="+flowServer+" flowMap="+flowMap+" nextFlowServer="+nextFlowServer);
		
		api.async(()->{
			FlowPair pair;
			
			synchronized (flows) {
				pair = getFlowPairByServerName(flowServer);
				FlowPair pairCheck = getFlowPairByServerName(nextFlowServer);
				// ensure both flows are linked together before update global config
				if(pair == null || pairCheck == null || !pair.equals(pairCheck)){
					Log.info("The 2 flows servers are not linked in a flow pair. Aborting.");
					return;
				}
			}
			
			HCMySQLAPI sql = api.getMySQLAPI();
			if(sql.isEnabled()){
				try{
					Log.info("Writing flows in global config");
					sql.writeGlobalConfig(Constants.GLOBAL_CONFIG_FLOW_MAP_KEY(flowServer), flowMap);
					sql.writeGlobalConfig(Constants.GLOBAL_CONFIG_FLOW_PAIR_KEY(pair), nextFlowServer);
				}catch(SQLException e){
					Log.severe("Could not move flow dispatcher server="+flowServer+" to next map="+flowMap);
					e.printStackTrace();
				}
			}
			
		});
	}

	public void updateFlowPairs() {

		api.async(()->{
			HCMySQLAPI sql = api.getMySQLAPI();
			if(sql.isEnabled()){
				updateFlowPairsFromGlobalConfig(sql);
				api.sync(()->{
					synchronized (flows) {
						for(FlowPair flowPair : flows.values()){
							updateSign(flowPair);
						}
					}
				});
			}
		}, 20);
		
	}
	
	private void updateSign(FlowPair flowPair) {
		
		String textOnSign = "Inactif";
		
		if(flowPair.isActive()){
			textOnSign = flowPair.getActive().getMap();
		}
		
		Block block = flowPair.getSignLocation().getBlock();
		if(block.getType().equals(Material.WALL_SIGN)){
			Sign sign = (Sign) block.getState();
			sign.setLine(2, textOnSign);
			sign.update();
			Log.info(
				"Flow Pair sign'"+
				flowPair.getFirst().getServer()+
				"/"+
				flowPair.getSecond().getServer()+
				" is now filled with sign content '"+
				textOnSign+
				"' (active="+flowPair.isActive()+
				")"
			);
		}else{
			Log.info("No sign to update for flow pair "+flowPair.getName());
		}
	}

	public void dispatchPlayer(FlowPair flowPair, Player player){
		if(flowPair == null || !flowPair.isActive()){
			HCPlayer hcPlayer = api.getPlayersManagerAPI().getHCPlayer(player);
			if(hcPlayer != null){
				api.getStringsAPI().get("lobby.flow-dispatcher.could-not-dispatch")
				.sendChatP(hcPlayer);
			}
		}else{
			Bungee.sendPlayerToServer(api.getPlugin(), player, flowPair.getActive().getServer());
		}
	}
	
	
}
