package com.gmail.val59000mc.lobby.flowdispatcher;

public class Flow {
	private String server;
	private String map;
	
	public Flow(String server) {
		this.server = server;
		this.map = null;
	}

	public String getServer() {
		return server;
	}

	public String getMap() {
		return map;
	}
	
	public void setMap(String map) {
		this.map = map;
	}
	
}
