package com.gmail.val59000mc.lobby.flowdispatcher;

import org.bukkit.event.Cancellable;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class FlowStartTpPlayerEvent extends HCEvent implements Cancellable{

	private HCPlayer hcPlayer;
	private boolean cancelled;

	public FlowStartTpPlayerEvent(HCGameAPI api, HCPlayer hcPlayer) {
		super(api);
		this.hcPlayer = hcPlayer;
		this.cancelled = false;
	}
	
	public HCPlayer getHCPlayer(){
		return hcPlayer;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

}
