package com.gmail.val59000mc.lobby.flowdispatcher;

import org.bukkit.Location;

public class FlowPair {
	private Flow first;
	private Flow second;
	private Flow active;
	private Location signLocation;
	
	public FlowPair(Flow first, Flow second, Location signLocation) {
		this.first = first;
		this.second = second;
		this.signLocation = signLocation;
		this.active = null;
	}
	
	public boolean isActive(){
		return active != null;
	}

	public Flow getActive() {
		return active;
	}

	public void setActive(Flow active) {
		this.active = active;
	}

	public Flow getFirst() {
		return first;
	}

	public Flow getSecond() {
		return second;
	}
	
	public Location getSignLocation() {
		return signLocation;
	}
	
	public String getName(){
		return first.getServer()+second.getServer();
	}
	
}
