package com.gmail.val59000mc.lobby.killskilldispatcher;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.lobby.common.Constants;
import com.gmail.val59000mc.spigotutils.Bungee;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.google.common.collect.Lists;

public class KillSkillDispatcher {

	protected HCGameAPI api;
	private Location signLocation;
	private Map<String,String> maps;
	
	public KillSkillDispatcher(HCGameAPI api){
		this.api = api;
		this.servers = new ArrayList<>();
		this.currentServer = -1;
	}

	private List<String> servers;
	private int currentServer;
	
	public void load() {
		servers = Lists.newArrayList();
		currentServer = 0;
		
		Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), ()->{
			HCMySQLAPI sql = api.getMySQLAPI();
			if(sql.isEnabled()){
				updateDispatcherFromGlobalConfig(sql);
				Bukkit.getScheduler().runTaskLater(api.getPlugin(), ()->{

					this.signLocation = Parser.parseLocation(api.getWorldConfig().getWorld(), api.getConfig().getString("kill-skill-dispatcher.sign"));
					Log.debug("Parsed kill skill sign location at "+Locations.printLocation(signLocation));
					
					ConfigurationSection section = api.getConfig().getConfigurationSection("kill-skill-dispatcher.maps");
					this.maps = new HashMap<String,String>();
					for(String key : section.getKeys(false)){
						Log.debug("Found Kill Skill map name : "+key+"="+section.getString(key));
						this.maps.put(key, section.getString(key)); 
					}
					
					updateSign();
				},5);
			}
		});
	}

	private void updateDispatcherFromGlobalConfig(HCMySQLAPI sql) {
		try{
			
			// loading kill skill servers from database
			String serversValue = sql.selectGlobalConfig(Constants.GLOBAL_CONFIG_KILL_SKILL_SERVERS_KEY);
			if(serversValue == null){
				sql.writeGlobalConfig(Constants.GLOBAL_CONFIG_KILL_SKILL_SERVERS_KEY, Constants.GLOBAL_CONFIG_KILL_SKILL_DEFAULT_SERVERS);
				serversValue = sql.selectGlobalConfig(Constants.GLOBAL_CONFIG_KILL_SKILL_SERVERS_KEY);
			}
			
			// loading current kill skill servers from database
			String currentServerValue = sql.selectGlobalConfig(Constants.GLOBAL_CONFIG_KILL_SKILL_CURRENT_SERVER_KEY);
			if(currentServerValue == null){
				sql.writeGlobalConfig(Constants.GLOBAL_CONFIG_KILL_SKILL_CURRENT_SERVER_KEY, "0");
				currentServerValue = sql.selectGlobalConfig(Constants.GLOBAL_CONFIG_KILL_SKILL_CURRENT_SERVER_KEY);
			}
			
			synchronized (servers) {
				servers = Lists.newArrayList(serversValue.split(Pattern.quote(",")));
			}
			currentServer = Integer.parseInt(currentServerValue);
			
			Log.info("Loaded KillSkill dispatcher servers list : "+serversValue);
			Log.info("Current server list index : "+currentServerValue);
			
		}catch(SQLException e){
			Logger.severe("Couldnt update Kill Skill dispatcher from global config");
			e.printStackTrace();
		}
	}

	public void dispatchPlayer(Player player){
		if(servers.size() == 0){
			Logger.warn("No kill skill server in dispatcher list !");
		}else if(currentServer >= servers.size()){
			Logger.warn("Invalid kill skill dispatcher currentServer index (greater than server list size)");
		}else{
			String serverName = servers.get(currentServer);
			Logger.debug("Dispatching player "+player.getName()+" to kill skill server "+serverName);
			Bungee.sendPlayerToServer(api.getPlugin(), player, serverName);
		}
	}

	/**
	 * Update the remore global config
	 * Doesn't change the internal in-memory config
	 * @return
	 */
	public int nextServer() {
		int current = currentServer;
		current++;
		if(current >= servers.size())
			current = 0;
		
		final int newCurrentServer = current;

		// save to database
		Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), ()->{
			HCMySQLAPI sql = api.getMySQLAPI();
			if(sql.isEnabled()){
				try{
					sql.writeGlobalConfig(Constants.GLOBAL_CONFIG_KILL_SKILL_CURRENT_SERVER_KEY, String.valueOf(newCurrentServer));
				}catch(SQLException e){
					Logger.severe("Couldnt write Kill Skill dispatcher next server to global config, newServer="+newCurrentServer);
					e.printStackTrace();
				}
			}
		});
		
		return current;		
	}

	public void updateCurrent() {

		Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), ()->{
			HCMySQLAPI sql = api.getMySQLAPI();
			if(sql.isEnabled()){
				updateDispatcherFromGlobalConfig(sql);
				Bukkit.getScheduler().runTask(api.getPlugin(), ()->{
					updateSign();
				});
			}
		});
		
	}

	private void updateSign() {
		if(signLocation == null || maps == null){
			return;
		}
		
		String textOnSign;
		if(servers.size() == 0 || currentServer >= servers.size()){
			textOnSign = "KillSkill";
		}else{
			textOnSign = servers.get(currentServer);
			if(maps.containsKey(textOnSign)){
				textOnSign = maps.get(textOnSign);
			}
		}
		
		Block block = signLocation.getBlock();
		if(block.getType().equals(Material.WALL_SIGN)){
			Sign sign = (Sign) block.getState();
			sign.setLine(2, textOnSign);
			sign.update();
			Log.debug("Kill Skill sign updated with content="+textOnSign);
		}
	}
	
}
