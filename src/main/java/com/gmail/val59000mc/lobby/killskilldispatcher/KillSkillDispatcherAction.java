package com.gmail.val59000mc.lobby.killskilldispatcher;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class KillSkillDispatcherAction extends Action{

	private KillSkillDispatcher dispatcher;
	
	public KillSkillDispatcherAction(KillSkillDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		
		dispatcher.dispatchPlayer(player);
		
		executeNextAction(player, sigPlayer, true);
	}

}
