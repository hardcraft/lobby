package com.gmail.val59000mc.lobby.killskilldispatcher;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class KillSkillDispatcherActionParser extends ActionParser{

	private KillSkillDispatcher killSkillDispatcher;
	
	public KillSkillDispatcherActionParser(KillSkillDispatcher killSkillDispatcher) {
		this.killSkillDispatcher = killSkillDispatcher;
	}

	@Override
	public String getType() {
		return "kill-skill-dispatcher";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		return new KillSkillDispatcherAction(killSkillDispatcher);
	}

	

}
