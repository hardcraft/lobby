SELECT 
    IFNULL(p.kills,0) AS globalKills,
    IFNULL(p.deaths,0) AS globalDeaths,
    IFNULL(p.play_time,0) AS globalPlayTime,
    IFNULL(p.coins_earned,0) AS globalCoinsEarned,
    IFNULL(p.wins,0) AS globalWins,
    
    IFNULL(lobby.play_time,0) AS globalLobbyPlayTime,
    
    IFNULL(ks.kills,0) AS globalKillSkillKills,
    IFNULL(ks.deaths,0) AS globalKillSkillDeaths,
    IFNULL(ks.experience_looted,0) AS globalKillSkillExperienceLooted,
    IFNULL(ks.lapis_looted,0) AS globalKillSkillLapisLooted,
    IFNULL(ks.potion_looted,0) AS globalKillSkillPotionLooted,
    IFNULL(ks.iron_looted,0) AS globalKillSkillIronLooted,
    IFNULL(ks.diamond_looted,0) AS globalKillSkillDiamondLooted,
    IFNULL(ks.blaze_looted,0) AS globalKillSkillBlazeLooted,
    IFNULL(ks.head_looted,0) AS globalKillSkillHeadLooted,
    IFNULL(ks.grenade_looted,0) AS globalKillSkillGrenadeLooted,
    IFNULL(ks.tnt_looted,0) AS globalKillSkillTntLooted,
    IFNULL(ks.cannon_looted,0) AS globalKillSkillCannonLooted,
    IFNULL(ks.supply_opened,0) AS globalKillSkillSupplyOpened,
    IFNULL(ks.coins_earned,0) AS globalKillSkillCoinsEarned,
    IFNULL(ks.play_time,0) AS globalKillSkillPlayTime,
    
    IFNULL(rush.kills,0) AS globalSurvivalRushKills,
    IFNULL(rush.deaths,0) AS globalSurvivalRushDeaths,
    IFNULL(rush.skeletons_killed,0) AS globalSurvivalRushSkeletonsKilled,
    IFNULL(rush.blocks_placed,0) AS globalSurvivalRushBlocksPlaced,
    IFNULL(rush.iron_mined,0) AS globalSurvivalRushIronMined,
    IFNULL(rush.diamond_mined,0) AS globalSurvivalRushDiamondMined,
    IFNULL(rush.gold_mined,0) AS globalSurvivalRushGoldMined,
    IFNULL(rush.gravel_mined,0) AS globalSurvivalRushGravelMined,
    IFNULL(rush.coins_earned,0) AS globalSurvivalRushCoinsEarned,
    IFNULL(rush.play_time,0) AS globalSurvivalRushPlayTime,
    IFNULL(rush.wins,0) AS globalSurvivalRushWins,
    
    IFNULL(evo.kills,0) AS globalEvolutionKills,
    IFNULL(evo.deaths,0) AS globalEvolutionDeaths,
    IFNULL(evo.play_time,0) AS globalEvolutionPlayTime,
    IFNULL(evo.stars_earned,0) AS globalEvolutionStarsEarned,
    IFNULL(evo.coins_earned,0) AS globalEvolutionCoinsEarned,
    IFNULL(evo.wins,0) AS globalEvolutionWins,
    
    IFNULL(ctf.kills,0) AS globalCtfKills,
    IFNULL(ctf.deaths,0) AS globalCtfDeaths,
    IFNULL(ctf.flags_taken,0) AS globalCtfFlagsTaken,
    IFNULL(ctf.flags_saved,0) AS globalCtfFlagsSaved,
    IFNULL(ctf.coins_earned,0) AS globalCtfCoinsEarned,
    IFNULL(ctf.play_time,0) AS globalCtfPlayTime,
    IFNULL(ctf.wins,0) AS globalCtfWins,
     
    IFNULL(gla.kills,0) AS globalGladiatorKills,
    IFNULL(gla.deaths,0) AS globalGladiatorDeaths,
    IFNULL(gla.gladiators_killed,0) AS globalGladiatorGladiatorsKilled,
    IFNULL(gla.slaves_killed,0) AS globalGladiatorSlavesKilled,
    IFNULL(gla.gladiator_wins,0) AS globalGladiatorGladiatorWins,
    IFNULL(gla.slave_wins,0) AS globalGladiatorSlaveWins,
    IFNULL(gla.coins_earned,0) AS globalGladiatorCoinsEarned,
    IFNULL(gla.play_time,0) AS globalGladiatorPlayTime,

    IFNULL(rage.kills,0) AS globalRageKills,
    IFNULL(rage.deaths,0) AS globalRageDeaths,
    IFNULL(rage.best_kill_streak,0) AS globalRageBestKillStreak,
    IFNULL(rage.tier_1_gadgets,0) AS globalRageTier1Gadgets,
    IFNULL(rage.tier_2_gadgets,0) AS globalRageTier2Gadgets,
    IFNULL(rage.tier_3_gadgets,0) AS globalRageTier3Gadgets,
    IFNULL(rage.double_jumps,0) AS globalRageDoubleJumps,
    IFNULL(rage.coins_earned,0) AS globalRageCoinsEarned,
    IFNULL(rage.play_time,0) AS globalRagePlayTime,
    IFNULL(rage.wins,0) AS globalRageWins,

    IFNULL(tdm.kills,0) AS globalTdmKills,
    IFNULL(tdm.deaths,0) AS globalTdmDeaths,
    IFNULL(tdm.coins_earned,0) AS globalTdmCoinsEarned,
    IFNULL(tdm.play_time,0) AS globalTdmPlayTime,
    IFNULL(tdm.wins,0) AS globalTdmWins
    
FROM 
	player p

LEFT JOIN lobby_player lobby ON ( lobby.player_id = p.id)

LEFT JOIN killskill_player ks ON ( ks.player_id = p.id)

LEFT JOIN survivalrush_player rush ON ( rush.player_id = p.id)

LEFT JOIN evo_player evo ON ( evo.player_id = p.id)

LEFT JOIN ctf_player ctf ON ( ctf.player_id = p.id)

LEFT JOIN gladiator_player gla ON ( gla.player_id = p.id)

LEFT JOIN rage_player rage ON ( rage.player_id = p.id)

LEFT JOIN tdm_player tdm ON (tdm.player_id = p.id)

WHERE
	p.id = ?;
    