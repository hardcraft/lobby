CREATE TABLE IF NOT EXISTS `lobby_player` (
  `player_id` INT(11) NOT NULL,
  `play_time` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_lobby_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;