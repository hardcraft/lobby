
SELECT 
     IFNULL(ks.experience_amount,1) AS killSkillExperienceAmount, 
     IFNULL(ks.lapis_chance,5) AS killSkillLapisChance, 
     IFNULL(ks.potion_chance,20) AS killSkillPotionChance, 
     IFNULL(ks.iron_chance,5) AS killSkillIronChance, 
     IFNULL(ks.diamond_chance,5) AS killSkillDiamondChance, 
     IFNULL(ks.blaze_chance,6) AS killSkillBlazeChance,
     IFNULL(ks.supply_amount,2) AS killSkillSupplyAmount,
     IFNULL(ks.supply_level,1) AS killSkillSupplyLevel,
     IFNULL(ks.enderchest_rows,6) AS killSkillEnderchestRows,

     IFNULL(evo.star,0) AS evoDoubleStarsChance,
     
     IFNULL(ctf.tnt_power,20) AS ctfTntPower,
     IFNULL(ctf.tnt_protection,0) AS ctfTntProtection,
     IFNULL(ctf.tnt_amount,1) AS ctfTntAmount,
     
     IFNULL(gla.gladiator_half_hearts,40) AS gladiatorGladiatorHalfHearts,
     IFNULL(gla.slave_bow_chance,20) AS gladiatorSlaveBowChance,
     IFNULL(gla.slave_extra_chance,20) AS gladiatorSlaveExtraChance,
     IFNULL(gla.slave_potion_chance,20) AS gladiatorSlavePotionChance,
     IFNULL(gla.enchant_chance,20) AS gladiatorEnchantChance,
     
     IFNULL(rush.iron_pickaxe,0) AS survivalRushHasIronPickaxe,
     IFNULL(rush.looting_sword,0) AS survivalRushSwordLooting,
     IFNULL(rush.iron_loot_chance,5) AS survivalRushIronLootChance,
     IFNULL(rush.skeletons_per_tnt,50) AS survivalRushSkeletonsPerTNT,
     IFNULL(rush.nbr_blocks_respawn,0) AS survivalRushNbrBlocksRespawn,

     IFNULL(rage.arrows_per_spawn,2) AS rageArrowsPerSpawn,
     IFNULL(rage.xp_regain_speed,1) AS rageXpRegainSpeed,
     IFNULL(rage.gagdets_level,1) AS rageGagdetsLevel
    
FROM 
	 player p
	 
LEFT JOIN killskill_player_perks ks ON (p.id = ks.player_id)
LEFT JOIN evo_player_perks evo ON (p.id = evo.player_id)
LEFT JOIN ctf_player_perks ctf ON (p.id = ctf.player_id)
LEFT JOIN gladiator_player_perks gla ON (p.id = gla.player_id)
LEFT JOIN survivalrush_player_perks rush ON (p.id = rush.player_id)
LEFT JOIN rage_player_perks rage ON (p.id = rage.player_id)

WHERE
     p.id = ?;