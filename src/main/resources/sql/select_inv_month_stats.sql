SELECT 
    IFNULL(player.kills,0) AS monthKills,
    IFNULL(player.deaths,0) AS monthDeaths,
    IFNULL(player.play_time,0) AS monthPlayTime,
    IFNULL(player.coins_earned,0) AS monthCoinsEarned,
    IFNULL(player.wins,0) AS monthWins,
    
    IFNULL(lobby.play_time,0) AS monthLobbyPlayTime,
    
    IFNULL(ks.kills,0) AS monthKillSkillKills,
    IFNULL(ks.deaths,0) AS monthKillSkillDeaths,
    IFNULL(ks.experience_looted,0) AS monthKillSkillExperienceLooted,
    IFNULL(ks.lapis_looted,0) AS monthKillSkillLapisLooted,
    IFNULL(ks.potion_looted,0) AS monthKillSkillPotionLooted,
    IFNULL(ks.iron_looted,0) AS monthKillSkillIronLooted,
    IFNULL(ks.diamond_looted,0) AS monthKillSkillDiamondLooted,
    IFNULL(ks.blaze_looted,0) AS monthKillSkillBlazeLooted,
    IFNULL(ks.head_looted,0) AS monthKillSkillHeadLooted,
    IFNULL(ks.grenade_looted,0) AS monthKillSkillGrenadeLooted,
    IFNULL(ks.tnt_looted,0) AS monthKillSkillTntLooted,
    IFNULL(ks.cannon_looted,0) AS monthKillSkillCannonLooted,
    IFNULL(ks.supply_opened,0) AS monthKillSkillSupplyOpened,
    IFNULL(ks.coins_earned,0) AS monthKillSkillCoinsEarned,
    IFNULL(ks.play_time,0) AS monthKillSkillPlayTime,
    
    IFNULL(rush.kills,0) AS monthSurvivalRushKills,
    IFNULL(rush.deaths,0) AS monthSurvivalRushDeaths,
    IFNULL(rush.skeletons_killed,0) AS monthSurvivalRushSkeletonsKilled,
    IFNULL(rush.blocks_placed,0) AS monthSurvivalRushBlocksPlaced,
    IFNULL(rush.iron_mined,0) AS monthSurvivalRushIronMined,
    IFNULL(rush.diamond_mined,0) AS monthSurvivalRushDiamondMined,
    IFNULL(rush.gold_mined,0) AS monthSurvivalRushGoldMined,
    IFNULL(rush.gravel_mined,0) AS monthSurvivalRushGravelMined,
    IFNULL(rush.coins_earned,0) AS monthSurvivalRushCoinsEarned,
    IFNULL(rush.play_time,0) AS monthSurvivalRushPlayTime,
    IFNULL(rush.wins,0) AS monthSurvivalRushWins,
    
    IFNULL(evo.kills,0) AS monthEvolutionKills,
    IFNULL(evo.deaths,0) AS monthEvolutionDeaths,
    IFNULL(evo.play_time,0) AS monthEvolutionPlayTime,
    IFNULL(evo.stars_earned,0) AS monthEvolutionStarsEarned,
    IFNULL(evo.coins_earned,0) AS monthEvolutionCoinsEarned,
    IFNULL(evo.wins,0) AS monthEvolutionWins,
    
    IFNULL(ctf.kills,0) AS monthCtfKills,
    IFNULL(ctf.deaths,0) AS monthCtfDeaths,
    IFNULL(ctf.flags_taken,0) AS monthCtfFlagsTaken,
    IFNULL(ctf.flags_saved,0) AS monthCtfFlagsSaved,
    IFNULL(ctf.coins_earned,0) AS monthCtfCoinsEarned,
    IFNULL(ctf.play_time,0) AS monthCtfPlayTime,
    IFNULL(ctf.wins,0) AS monthCtfWins,
     
    IFNULL(gla.kills,0) AS monthGladiatorKills,
    IFNULL(gla.deaths,0) AS monthGladiatorDeaths,
    IFNULL(gla.gladiators_killed,0) AS monthGladiatorGladiatorsKilled,
    IFNULL(gla.slaves_killed,0) AS monthGladiatorSlavesKilled,
    IFNULL(gla.gladiator_wins,0) AS monthGladiatorGladiatorWins,
    IFNULL(gla.slave_wins,0) AS monthGladiatorSlaveWins,
    IFNULL(gla.coins_earned,0) AS monthGladiatorCoinsEarned,
    IFNULL(gla.play_time,0) AS monthGladiatorPlayTime,

    IFNULL(rage.kills,0) AS monthRageKills,
    IFNULL(rage.deaths,0) AS monthRageDeaths,
    IFNULL(rage.best_kill_streak,0) AS monthRageBestKillStreak,
    IFNULL(rage.tier_1_gadgets,0) AS monthRageTier1Gadgets,
    IFNULL(rage.tier_2_gadgets,0) AS monthRageTier2Gadgets,
    IFNULL(rage.tier_3_gadgets,0) AS monthRageTier3Gadgets,
    IFNULL(rage.double_jumps,0) AS monthRageDoubleJumps,
    IFNULL(rage.coins_earned,0) AS monthRageCoinsEarned,
    IFNULL(rage.play_time,0) AS monthRagePlayTime,
    IFNULL(rage.wins,0) AS monthRageWins,

    IFNULL(tdm.kills,0) AS monthTdmKills,
    IFNULL(tdm.deaths,0) AS monthTdmDeaths,
    IFNULL(tdm.coins_earned,0) AS monthTdmCoinsEarned,
    IFNULL(tdm.play_time,0) AS monthTdmPlayTime,
    IFNULL(tdm.wins,0) AS monthTdmWins
    
FROM 
	player p
	
LEFT JOIN player_stats player ON ( player.player_id = p.id AND player.month = %month% AND player.year = %year%)

LEFT JOIN lobby_player_stats lobby ON ( lobby.player_id = p.id AND lobby.month = %month% AND lobby.year = %year%)

LEFT JOIN killskill_player_stats ks ON ( ks.player_id = p.id AND ks.month = %month% AND ks.year = %year%)

LEFT JOIN survivalrush_player_stats rush ON ( rush.player_id = p.id AND rush.month = %month% AND rush.year = %year%)

LEFT JOIN evo_player_stats evo ON ( evo.player_id = p.id AND evo.month = %month% AND evo.year = %year%)

LEFT JOIN ctf_player_stats ctf ON ( ctf.player_id = p.id AND ctf.month = %month% AND ctf.year = %year%)

LEFT JOIN gladiator_player_stats gla ON ( gla.player_id = p.id AND gla.month = %month% AND gla.year = %year%)

LEFT JOIN rage_player_stats rage ON ( rage.player_id = p.id AND rage.month = %month% AND rage.year = %year%)

LEFT JOIN tdm_player_stats tdm ON ( tdm.player_id = p.id AND tdm.month = %month% AND tdm.year = %year%)

WHERE
	p.id = ?;
    