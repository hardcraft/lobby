SELECT player.id, player.name FROM player 
LEFT JOIN %table% AS game ON (player.id = game.player_id AND game.month = %month% AND game.year = %year%)
WHERE %where%
ORDER BY game.%sortBy% DESC LIMIT 5 OFFSET %offset%;