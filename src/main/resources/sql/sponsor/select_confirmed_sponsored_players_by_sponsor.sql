SELECT 
	player.id,
	player.uuid,
	player.name,
	player.kills,
	player_sponsorship.sponsored_player_id,
	player_sponsorship.date_confirmed,
	player_sponsorship.state
FROM
	player_sponsorship
LEFT JOIN player ON (player.id = player_sponsorship.sponsored_player_id)
WHERE
	player_sponsorship.sponsor_player_id = ? 
	AND player_sponsorship.state = 'CONFIRMED'
ORDER BY player_sponsorship.date_confirmed DESC;