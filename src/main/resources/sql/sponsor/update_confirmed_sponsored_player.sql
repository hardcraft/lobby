UPDATE 
	player_sponsorship 
SET 
	state='VALIDATED' 
WHERE 
	sponsored_player_id = ?
	AND state='CONFIRMED';
