UPDATE player_sponsorship 

LEFT JOIN player ON (player.id = sponsored_player_id)

SET 
	state='CONFIRMED',
	date_confirmed=NOW() 

WHERE 
	state='UNCONFIRMED'
	AND player.kills >= ?
	AND date_unconfirmed >= ?
	AND date_unconfirmed <= ?;
