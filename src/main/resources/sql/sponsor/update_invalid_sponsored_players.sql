UPDATE 
	player_sponsorship 
SET 
	state='INVALID' 
WHERE 
	date_unconfirmed <= ?
	AND state='UNCONFIRMED';
