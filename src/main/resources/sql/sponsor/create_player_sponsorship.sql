CREATE TABLE IF NOT EXISTS `player_sponsorship` (
  `sponsor_player_id` INT(11) NOT NULL,
  `sponsored_player_id` INT(11) NOT NULL,
  `date_unconfirmed` DATETIME NOT NULL,
  `date_confirmed` DATETIME DEFAULT NULL,
  `state` VARCHAR(11) NOT NULL DEFAULT 'UNCONFIRMED',  
  PRIMARY KEY (`sponsor_player_id`, `sponsored_player_id`),
  UNIQUE INDEX `sponsored_player_id_UNIQUE` (`sponsored_player_id` ASC),
  CONSTRAINT `fk_player_sponsorship_sponsor_player_id`
    FOREIGN KEY (`sponsor_player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_player_sponsorship_sponsored_player_id`
    FOREIGN KEY (`sponsored_player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;