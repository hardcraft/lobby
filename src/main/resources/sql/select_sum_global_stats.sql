SELECT 

COALESCE(SUM(p.kills),0) AS globalKills, 
COALESCE(SUM(p.deaths),0) AS globalDeaths, 
COALESCE(AVG(p.play_time),0) AS globalPlayTime,
COALESCE(SUM(p.coins_earned),0) AS globalCoinsEarned, 
COALESCE(SUM(p.wins),0) AS globalWins, 

COALESCE(AVG(lb.play_time),0) AS globalLobbyPlayTime,

COALESCE(SUM(ks.kills),0) AS globalKillSkillKills, 
COALESCE(SUM(ks.deaths),0) AS globalKillSkillDeaths,
COALESCE(SUM(ks.experience_looted),0) AS globalKillSkillExperienceLooted,
COALESCE(SUM(ks.lapis_looted),0) AS globalKillSkillLapisLooted,
COALESCE(SUM(ks.potion_looted),0) AS globalKillSkillPotionLooted,
COALESCE(SUM(ks.iron_looted),0) AS globalKillSkillIronLooted,
COALESCE(SUM(ks.diamond_looted),0) AS globalKillSkillDiamondLooted,
COALESCE(SUM(ks.blaze_looted),0) AS globalKillSkillBlazeLooted,
COALESCE(SUM(ks.head_looted),0) AS globalKillSkillHeadLooted,
COALESCE(SUM(ks.grenade_looted),0) AS globalKillSkillGrenadeLooted,
COALESCE(SUM(ks.tnt_looted),0) AS globalKillSkillTntLooted,
COALESCE(SUM(ks.cannon_looted),0) AS globalKillSkillCannonLooted,
COALESCE(SUM(ks.supply_opened),0) AS globalKillSkillSupplyOpened,
COALESCE(AVG(ks.play_time),0) AS globalKillSkillPlayTime,
COALESCE(SUM(ks.coins_earned),0) AS globalKillSkillCoinsEarned,

COALESCE(SUM(rush.kills),0) AS globalSurvivalRushKills,
COALESCE(SUM(rush.deaths),0) AS globalSurvivalRushDeaths,
COALESCE(SUM(rush.skeletons_killed),0) AS globalSurvivalRushSkeletonsKilled,
COALESCE(SUM(rush.blocks_placed),0) AS globalSurvivalRushBlocksPlaced,
COALESCE(SUM(rush.iron_mined),0) AS globalSurvivalRushIronMined,
COALESCE(SUM(rush.diamond_mined),0) AS globalSurvivalRushDiamondMined,
COALESCE(SUM(rush.gold_mined),0) AS globalSurvivalRushGoldMined,
COALESCE(SUM(rush.gravel_mined),0) AS globalSurvivalRushGravelMined,
COALESCE(SUM(rush.coins_earned),0) AS globalSurvivalRushCoinsEarned,
COALESCE(AVG(rush.play_time),0) AS globalSurvivalRushPlayTime,
COALESCE(SUM(rush.wins),0) AS globalSurvivalRushWins,

COALESCE(SUM(evo.kills),0) AS globalEvolutionKills,
COALESCE(SUM(evo.deaths),0) AS globalEvolutionDeaths,
COALESCE(AVG(evo.play_time),0) AS globalEvolutionPlayTime,
COALESCE(SUM(evo.stars_earned),0) AS globalEvolutionStarsEarned,
COALESCE(SUM(evo.coins_earned),0) AS globalEvolutionCoinsEarned,
COALESCE(SUM(evo.wins),0) AS globalEvolutionWins,

COALESCE(SUM(ctf.kills),0) AS globalCtfKills,
COALESCE(SUM(ctf.deaths),0) AS globalCtfDeaths,
COALESCE(SUM(ctf.flags_taken),0) AS globalCtfFlagsTaken,
COALESCE(SUM(ctf.flags_saved),0) AS globalCtfFlagsSaved,
COALESCE(SUM(ctf.coins_earned),0) AS globalCtfCoinsEarned,
COALESCE(AVG(ctf.play_time),0) AS globalCtfPlayTime,
COALESCE(SUM(ctf.wins),0) AS globalCtfWins,
 
COALESCE(SUM(gla.kills),0) AS globalGladiatorKills,
COALESCE(SUM(gla.deaths),0) AS globalGladiatorDeaths,
COALESCE(SUM(gla.gladiators_killed),0) AS globalGladiatorGladiatorsKilled,
COALESCE(SUM(gla.slaves_killed),0) AS globalGladiatorSlavesKilled,
COALESCE(SUM(gla.gladiator_wins),0) AS globalGladiatorGladiatorWins,
COALESCE(SUM(gla.slave_wins),0) AS globalGladiatorSlaveWins,
COALESCE(SUM(gla.coins_earned),0) AS globalGladiatorCoinsEarned,
COALESCE(AVG(gla.play_time),0) AS globalGladiatorPlayTime,

COALESCE(SUM(rage.kills),0) AS globalRageKills,
COALESCE(SUM(rage.deaths),0) AS globalRageDeaths,
COALESCE(MAX(rage.best_kill_streak),0) AS globalRageBestKillStreak,
COALESCE(SUM(rage.tier_1_gadgets),0) AS globalRageTier1Gadgets,
COALESCE(SUM(rage.tier_2_gadgets),0) AS globalRageTier2Gadgets,
COALESCE(SUM(rage.tier_3_gadgets),0) AS globalRageTier3Gadgets,
COALESCE(SUM(rage.double_jumps),0) AS globalRageDoubleJumps,
COALESCE(SUM(rage.coins_earned),0) AS globalRageCoinsEarned,
COALESCE(AVG(rage.play_time),0) AS globalRagePlayTime,
COALESCE(SUM(rage.wins),0) AS globalRageWins,

COALESCE(SUM(tdm.kills),0) AS globalTdmKills,
COALESCE(SUM(tdm.deaths),0) AS globalTdmDeaths,
COALESCE(SUM(tdm.coins_earned),0) AS globalTdmCoinsEarned,
COALESCE(AVG(tdm.play_time),0) AS globalTdmPlayTime,
COALESCE(SUM(tdm.wins),0) AS globalTdmWins
    
FROM player p
LEFT JOIN lobby_player lb ON (lb.player_id = p.id)
LEFT JOIN killskill_player ks ON (ks.player_id = p.id)
LEFT JOIN survivalrush_player rush ON ( rush.player_id = p.id)
LEFT JOIN evo_player evo ON ( evo.player_id = p.id)
LEFT JOIN ctf_player ctf ON ( ctf.player_id = p.id)
LEFT JOIN gladiator_player gla ON ( gla.player_id = p.id)
LEFT JOIN rage_player rage ON ( rage.player_id = p.id)
LEFT JOIN tdm_player tdm ON ( tdm.player_id = p.id);