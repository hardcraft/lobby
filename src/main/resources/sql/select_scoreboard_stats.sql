SELECT 
    IFNULL(p.kills,0) AS globalKills,
    IFNULL(p.deaths,0) AS globalDeaths,
    IFNULL(p.play_time,0) AS globalPlayTime,
    IFNULL(p.coins_earned,0) AS globalCoinsEarned,
    IFNULL(p.wins,0) AS globalWins,
    
    IFNULL(p_stats.kills,0) AS monthKills,
    IFNULL(p_stats.deaths,0) AS monthDeaths,
    IFNULL(p_stats.play_time,0) AS monthPlayTime,
    IFNULL(p_stats.coins_earned,0) AS monthCoinsEarned,
    IFNULL(p_stats.wins,0) AS monthWins,
    
    
    IFNULL(lobby.play_time,0) AS globalLobbyPlayTime,
    
    IFNULL(lobby_stats.play_time,0) AS monthLobbyPlayTime,
    
    
    IFNULL(ks.kills,0) AS globalKillSkillKills,
    IFNULL(ks.deaths,0) AS globalKillSkillDeaths,
    IFNULL(ks.play_time,0) AS globalKillSkillPlayTime,
    IFNULL(ks.coins_earned,0) AS globalKillSkillCoinsEarned,
    
    IFNULL(ks_stats.kills,0) AS monthKillSkillKills,
    IFNULL(ks_stats.deaths,0) AS monthKillSkillDeaths,
    IFNULL(ks_stats.play_time,0) AS monthKillSkillPlayTime,
    IFNULL(ks_stats.coins_earned,0) AS monthKillSkillCoinsEarned,
    
    
    IFNULL(rush.kills,0) AS globalSurvivalRushKills,
    IFNULL(rush.deaths,0) AS globalSurvivalRushDeaths,
    IFNULL(rush.play_time,0) AS globalSurvivalRushPlayTime,
    IFNULL(rush.coins_earned,0) AS globalSurvivalRushCoinsEarned,
    IFNULL(rush.wins,0) AS globalSurvivalRushWins,
    
    IFNULL(rush_stats.kills,0) AS monthSurvivalRushKills,
    IFNULL(rush_stats.deaths,0) AS monthSurvivalRushDeaths,
    IFNULL(rush_stats.play_time,0) AS monthSurvivalRushPlayTime,
    IFNULL(rush_stats.coins_earned,0) AS monthSurvivalRushCoinsEarned,
    IFNULL(rush_stats.wins,0) AS monthSurvivalRushWins,
    
    
    IFNULL(evo.kills,0) AS globalEvolutionKills,
    IFNULL(evo.deaths,0) AS globalEvolutionDeaths,
    IFNULL(evo.play_time,0) AS globalEvolutionPlayTime,
    IFNULL(evo.coins_earned,0) AS globalEvolutionCoinsEarned,
    IFNULL(evo.wins,0) AS globalEvolutionWins,
    
    IFNULL(evo_stats.kills,0) AS monthEvolutionKills,
    IFNULL(evo_stats.deaths,0) AS monthEvolutionDeaths,
    IFNULL(evo_stats.play_time,0) AS monthEvolutionPlayTime,
    IFNULL(evo_stats.coins_earned,0) AS monthEvolutionCoinsEarned,
    IFNULL(evo_stats.wins,0) AS monthEvolutionWins,
    
    
    IFNULL(ctf.kills,0) AS globalCtfKills,
    IFNULL(ctf.deaths,0) AS globalCtfDeaths,
    IFNULL(ctf.play_time,0) AS globalCtfPlayTime,
    IFNULL(ctf.coins_earned,0) AS globalCtfCoinsEarned,
    IFNULL(ctf.wins,0) AS globalCtfWins,
    
    IFNULL(ctf_stats.kills,0) AS monthCtfKills,
    IFNULL(ctf_stats.deaths,0) AS monthCtfDeaths,
    IFNULL(ctf_stats.play_time,0) AS monthCtfPlayTime,
    IFNULL(ctf_stats.coins_earned,0) AS monthCtfCoinsEarned,
    IFNULL(ctf_stats.wins,0) AS monthCtfWins,

     
    IFNULL(gla.kills,0) AS globalGladiatorKills,
    IFNULL(gla.deaths,0) AS globalGladiatorDeaths,
    IFNULL(gla.play_time,0) AS globalGladiatorPlayTime,
    IFNULL(gla.coins_earned,0) AS globalGladiatorCoinsEarned,
    
    IFNULL(gla_stats.kills,0) AS monthGladiatorKills,
    IFNULL(gla_stats.deaths,0) AS monthGladiatorDeaths,
    IFNULL(gla_stats.play_time,0) AS monthGladiatorPlayTime,
    IFNULL(gla_stats.coins_earned,0) AS monthGladiatorCoinsEarned,


    IFNULL(rage.kills,0) AS globalRageKills,
    IFNULL(rage.deaths,0) AS globalRageDeaths,
    IFNULL(rage.play_time,0) AS globalRagePlayTime,
    IFNULL(rage.coins_earned,0) AS globalRageCoinsEarned,
    IFNULL(rage.wins,0) AS globalRageWins,

    IFNULL(rage_stats.kills,0) AS monthRageKills,
    IFNULL(rage_stats.deaths,0) AS monthRageDeaths,
    IFNULL(rage_stats.play_time,0) AS monthRagePlayTime,
    IFNULL(rage_stats.coins_earned,0) AS monthRageCoinsEarned,
    IFNULL(rage_stats.wins,0) AS monthRageWins,


    IFNULL(tdm.kills,0) AS globalTdmKills,
    IFNULL(tdm.deaths,0) AS globalTdmDeaths,
    IFNULL(tdm.play_time,0) AS globalTdmPlayTime,
    IFNULL(tdm.coins_earned,0) AS globalTdmCoinsEarned,
    IFNULL(tdm.wins,0) AS globalTdmWins,

    IFNULL(tdm_stats.kills,0) AS monthTdmKills,
    IFNULL(tdm_stats.deaths,0) AS monthTdmDeaths,
    IFNULL(tdm_stats.play_time,0) AS monthTdmPlayTime,
    IFNULL(tdm_stats.coins_earned,0) AS monthTdmCoinsEarned,
    IFNULL(tdm_stats.wins,0) AS monthTdmWins
    
FROM 
	player p
     
LEFT JOIN player_stats p_stats ON ( p_stats.player_id = p.id AND p_stats.month = %month% AND p_stats.year = %year%)

LEFT JOIN lobby_player lobby ON ( lobby.player_id = p.id)
LEFT JOIN lobby_player_stats lobby_stats ON ( lobby_stats.player_id = p.id AND lobby_stats.month = %month% AND lobby_stats.year = %year%)

LEFT JOIN killskill_player ks ON ( ks.player_id = p.id)
LEFT JOIN killskill_player_stats ks_stats ON ( ks_stats.player_id = p.id AND ks_stats.month = %month% AND ks_stats.year = %year%)

LEFT JOIN survivalrush_player rush ON ( rush.player_id = p.id)
LEFT JOIN survivalrush_player_stats rush_stats ON ( rush_stats.player_id = p.id AND rush_stats.month = %month% AND rush_stats.year = %year%)

LEFT JOIN evo_player evo ON ( evo.player_id = p.id)
LEFT JOIN evo_player_stats evo_stats ON ( evo_stats.player_id = p.id AND evo_stats.month = %month% AND evo_stats.year = %year%)

LEFT JOIN ctf_player ctf ON ( ctf.player_id = p.id)
LEFT JOIN ctf_player_stats ctf_stats ON ( ctf_stats.player_id = p.id AND ctf_stats.month = %month% AND ctf_stats.year = %year%)

LEFT JOIN gladiator_player gla ON ( gla.player_id = p.id)
LEFT JOIN gladiator_player_stats gla_stats ON ( gla_stats.player_id = p.id AND gla_stats.month = %month% AND gla_stats.year = %year%)

LEFT JOIN rage_player rage ON ( rage.player_id = p.id)
LEFT JOIN rage_player_stats rage_stats ON ( rage_stats.player_id = p.id AND rage_stats.month = %month% AND rage_stats.year = %year%)

LEFT JOIN tdm_player tdm ON ( tdm.player_id = p.id)
LEFT JOIN tdm_player_stats tdm_stats ON ( tdm_stats.player_id = p.id AND tdm_stats.month = %month% AND tdm_stats.year = %year%)

WHERE
	p.id = ?;
    