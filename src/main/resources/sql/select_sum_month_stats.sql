SELECT 

COALESCE(SUM(player.kills),0) AS monthKills, 
COALESCE(SUM(player.deaths),0) AS monthDeaths, 
COALESCE(AVG(player.play_time),0) AS monthPlayTime,
COALESCE(SUM(player.coins_earned),0) AS monthCoinsEarned, 
COALESCE(SUM(player.wins),0) AS monthWins, 

COALESCE(AVG(lb.play_time),0) AS monthLobbyPlayTime,

COALESCE(SUM(ks.kills),0) AS monthKillSkillKills, 
COALESCE(SUM(ks.deaths),0) AS monthKillSkillDeaths,
COALESCE(SUM(ks.experience_looted),0) AS monthKillSkillExperienceLooted,
COALESCE(SUM(ks.lapis_looted),0) AS monthKillSkillLapisLooted,
COALESCE(SUM(ks.potion_looted),0) AS monthKillSkillPotionLooted,
COALESCE(SUM(ks.iron_looted),0) AS monthKillSkillIronLooted,
COALESCE(SUM(ks.diamond_looted),0) AS monthKillSkillDiamondLooted,
COALESCE(SUM(ks.blaze_looted),0) AS monthKillSkillBlazeLooted,
COALESCE(SUM(ks.head_looted),0) AS monthKillSkillHeadLooted,
COALESCE(SUM(ks.grenade_looted),0) AS monthKillSkillGrenadeLooted,
COALESCE(SUM(ks.tnt_looted),0) AS monthKillSkillTntLooted,
COALESCE(SUM(ks.cannon_looted),0) AS monthKillSkillCannonLooted,
COALESCE(SUM(ks.supply_opened),0) AS monthKillSkillSupplyOpened,
COALESCE(AVG(ks.play_time),0) AS monthKillSkillPlayTime,
COALESCE(SUM(ks.coins_earned),0) AS monthKillSkillCoinsEarned,

COALESCE(SUM(rush.kills),0) AS monthSurvivalRushKills,
COALESCE(SUM(rush.deaths),0) AS monthSurvivalRushDeaths,
COALESCE(SUM(rush.skeletons_killed),0) AS monthSurvivalRushSkeletonsKilled,
COALESCE(SUM(rush.blocks_placed),0) AS monthSurvivalRushBlocksPlaced,
COALESCE(SUM(rush.iron_mined),0) AS monthSurvivalRushIronMined,
COALESCE(SUM(rush.diamond_mined),0) AS monthSurvivalRushDiamondMined,
COALESCE(SUM(rush.gold_mined),0) AS monthSurvivalRushGoldMined,
COALESCE(SUM(rush.gravel_mined),0) AS monthSurvivalRushGravelMined,
COALESCE(SUM(rush.coins_earned),0) AS monthSurvivalRushCoinsEarned,
COALESCE(AVG(rush.play_time),0) AS monthSurvivalRushPlayTime,
COALESCE(SUM(rush.wins),0) AS monthSurvivalRushWins,

COALESCE(SUM(evo.kills),0) AS monthEvolutionKills,
COALESCE(SUM(evo.deaths),0) AS monthEvolutionDeaths,
COALESCE(AVG(evo.play_time),0) AS monthEvolutionPlayTime,
COALESCE(SUM(evo.stars_earned),0) AS monthEvolutionStarsEarned,
COALESCE(SUM(evo.coins_earned),0) AS monthEvolutionCoinsEarned,
COALESCE(SUM(evo.wins),0) AS monthEvolutionWins,

COALESCE(SUM(ctf.kills),0) AS monthCtfKills,
COALESCE(SUM(ctf.deaths),0) AS monthCtfDeaths,
COALESCE(SUM(ctf.flags_taken),0) AS monthCtfFlagsTaken,
COALESCE(SUM(ctf.flags_saved),0) AS monthCtfFlagsSaved,
COALESCE(SUM(ctf.coins_earned),0) AS monthCtfCoinsEarned,
COALESCE(AVG(ctf.play_time),0) AS monthCtfPlayTime,
COALESCE(SUM(ctf.wins),0) AS monthCtfWins,
 
COALESCE(SUM(gla.kills),0) AS monthGladiatorKills,
COALESCE(SUM(gla.deaths),0) AS monthGladiatorDeaths,
COALESCE(SUM(gla.gladiators_killed),0) AS monthGladiatorGladiatorsKilled,
COALESCE(SUM(gla.slaves_killed),0) AS monthGladiatorSlavesKilled,
COALESCE(SUM(gla.gladiator_wins),0) AS monthGladiatorGladiatorWins,
COALESCE(SUM(gla.slave_wins),0) AS monthGladiatorSlaveWins,
COALESCE(SUM(gla.coins_earned),0) AS monthGladiatorCoinsEarned,
COALESCE(AVG(gla.play_time),0) AS monthGladiatorPlayTime,

COALESCE(SUM(rage.kills),0) AS monthRageKills,
COALESCE(SUM(rage.deaths),0) AS monthRageDeaths,
COALESCE(MAX(rage.best_kill_streak),0) AS monthRageBestKillStreak,
COALESCE(SUM(rage.tier_1_gadgets),0) AS monthRageTier1Gadgets,
COALESCE(SUM(rage.tier_2_gadgets),0) AS monthRageTier2Gadgets,
COALESCE(SUM(rage.tier_3_gadgets),0) AS monthRageTier3Gadgets,
COALESCE(SUM(rage.double_jumps),0) AS monthRageDoubleJumps,
COALESCE(SUM(rage.coins_earned),0) AS monthRageCoinsEarned,
COALESCE(AVG(rage.play_time),0) AS monthRagePlayTime,
COALESCE(SUM(rage.wins),0) AS monthRageWins,

COALESCE(SUM(tdm.kills),0) AS monthTdmKills,
COALESCE(SUM(tdm.deaths),0) AS monthTdmDeaths,
COALESCE(SUM(tdm.coins_earned),0) AS monthTdmCoinsEarned,
COALESCE(AVG(tdm.play_time),0) AS monthTdmPlayTime,
COALESCE(SUM(tdm.wins),0) AS monthTdmWins
    
FROM player p
LEFT JOIN player_stats player ON (player.player_id = p.id AND player.month = %month% AND player.year = %year%)
LEFT JOIN lobby_player_stats lb ON (lb.player_id = p.id AND lb.month = %month% AND lb.year = %year%)
LEFT JOIN killskill_player_stats ks ON (ks.player_id = p.id AND ks.month = %month% AND ks.year = %year%)
LEFT JOIN survivalrush_player_stats rush ON ( rush.player_id = p.id AND rush.month = %month% AND rush.year = %year%)
LEFT JOIN evo_player_stats evo ON ( evo.player_id = p.id AND evo.month = %month% AND evo.year = %year%)
LEFT JOIN ctf_player_stats ctf ON ( ctf.player_id = p.id AND ctf.month = %month% AND ctf.year = %year%)
LEFT JOIN gladiator_player_stats gla ON ( gla.player_id = p.id AND gla.month = %month% AND gla.year = %year%)
LEFT JOIN rage_player_stats rage ON ( rage.player_id = p.id AND rage.month = %month% AND rage.year = %year%)
LEFT JOIN tdm_player_stats tdm ON ( tdm.player_id = p.id AND tdm.month = %month% AND tdm.year = %year%);